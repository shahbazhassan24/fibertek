@extends('layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <strong class="inner-heading">Hi {{ $emailData['data']['name'] }},</strong>
            <div class="information">
                <div class="row">
                    You have been successfully registered with the following information:
                </div> 
                <div class="row">
                    <ul class="box background">
                        <li><span>User Type:</span><span>{{ $emailData['data']['user_role'] }}</span></li>
                        <li><span>Email Address (User ID):</span><span>{{ $emailData['data']['email'] }}</span></li>
                        <li><span>Company Name:</span><span>{{ $emailData['data']['company_name'] }}</span></li>
                        <li><span>Company Address:</span><span>{{ $emailData['data']['company_address'] }}</span></li>
                        <li><span>Job Destination/ Appointment:</span><span>{{ $emailData['data']['job_designation'] }}</span></li>
                        <li><span>Office Number:</span><span>{{ $emailData['data']['office_number'] }}</span></li>
                        <li><span>Mobile Number:</span><span>{{ $emailData['data']['mobile_number'] }}</span></li>
                        <li><span>ACRA:</span><span>{{ $emailData['data']['acra'] }}</span></li>
                    </ul>
                </div>    
            </div>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <div class="row">
                    Please refer to the links below for more information
                </div> 
                <div class="row">
                    <ul class="links">
                        <li><a href="{{ url('/page/1') }}">FAQs</a></li>
                        <li><a href="{{ url('/page/2') }}">How it Works</a></li>
                        <li><a href="{{ url('/page/3') }}">Downloadable Guides/Forms</a></li>
                    </ul>
                </div>    
            </div>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
           <a href="<?php echo url('/login'); ?>" class="btn_primary">Get Started</a>
        </td>
    </tr>
</table>
@endsection
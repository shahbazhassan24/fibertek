@extends('layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Hi {{ $emailData['data']['name'] }},</strong>
                <div class="row">
                    There are PTW Applications that you need to review:
                </div>    
            </div>
        </td>
    </tr>
     <tr>
        <td class="main-heading">
            Pending PTW Applications ({{ $emailData['data']['count'] }})
        </td>
    </tr>
    <?php
    if(!empty($emailData['data']['applications'])){
        foreach ($emailData['data']['applications'] as $applicationKey => $applicationValue) { ?>
    <tr>
        <td align="left" valign="top">
            <ul class="pending-applications">
                <li>
                    <a href="{{ url('/approver/application/view/'.$applicationValue['application_id']) }}" class="underline">{{ $applicationValue['application_number'] }}</a>
                </li>
                <li>
                    <div class="information">
                        <strong>{{ $applicationValue['work_location'] }} ({{ $applicationValue['common_area'] }})</strong>
                        <div class="row small-margin">Application #: <span>{{ $applicationValue['application_number'] }}</span></div>
                        <div class="row small-margin">{{ $applicationValue['work_nature'] }}</div>
                        <div class="row small-margin">{{ $applicationValue['work_duration'] }}</div>
                    </div>
                </li>
                <li>
                    <div class="information">
                        <div class="row small-margin">{{ $applicationValue['submitted_at'] }}</div>
                    </div>
                </li>
            </ul>
        </td>
    </tr>
        <?php } ?>
    <?php } ?>
    <tr>
        <td>
            <div class="information">
                <div class="row">* The completed PTW with necessary supporting documents must be submitted at least three (3) working days prior to the actual work for processing.</div>
            </div>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
           <a href="<?php echo url('/login') ?>" class="btn_primary">Login Now</a>
        </td>
    </tr>
</table>
@endsection
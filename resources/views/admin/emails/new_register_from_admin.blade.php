@extends('layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <strong class="inner-heading">Hi {{ $emailData['data']['name'] }},</strong>
            <div class="information">
                <div class="row">
                    You have been registered for access to APM PTW as an {{ $emailData['data']['user_role'] }}:
                </div> 
                <div class="row">
                    <ul class="box background">
                        <li><span>User ID:</span><span>{{ $emailData['data']['email'] }}</span></li>
                        <li><span>Password (temporary):</span><span>{{ $emailData['data']['password'] }}</span></li>
                        <?php
                        if(isset($emailData['data']['company_name']) && ($emailData['data']['company_name'] != '')){ ?>
                            <li><span>Company Name:</span><span>{{ $emailData['data']['company_name'] }}</span></li>
                        <?php } ?>
                        <?php
                        if(isset($emailData['data']['company_address']) && ($emailData['data']['company_address'] != '')){ ?>
                            <li><span>Company Address:</span><span>{{ $emailData['data']['company_address'] }}</span></li>
                        <?php } ?>
                        <?php
                        if(isset($emailData['data']['job_designation']) && ($emailData['data']['job_designation'] != '')){ ?>
                            <li><span>Job Destination/ Appointment:</span><span>{{ $emailData['data']['job_designation'] }}</span></li>
                        <?php } ?>
                        <?php
                        if(isset($emailData['data']['office_number']) && ($emailData['data']['office_number'] != '')){ ?>
                            <li><span>Office Number:</span><span>{{ $emailData['data']['office_number'] }}</span></li>
                        <?php } ?>
                        <?php
                        if(isset($emailData['data']['mobile_number']) && ($emailData['data']['mobile_number'] != '')){ ?>
                            <li><span>Mobile Number:</span><span>{{ $emailData['data']['mobile_number'] }}</span></li>
                        <?php } ?>
                        <?php
                        if(isset($emailData['data']['acra']) && ($emailData['data']['acra'] != '')){ ?>
                            <li><span>ACRA:</span><span>{{ $emailData['data']['acra'] }}</span></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="row">
                    For security compliance purpose, you will be prompted to change your password upon initial login.
                </div>   
            </div>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <div class="row">
                    Please refer to the links below for more information
                </div> 
                <div class="row">
                    <ul class="links">
                        <li><a href="{{ url('/page/1') }}">FAQs</a></li>
                        <li><a href="{{ url('/page/2') }}">How it Works</a></li>
                        <li><a href="{{ url('/page/3') }}">Downloadable Guides/Forms</a></li>
                    </ul>
                </div>    
            </div>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
           <a href="<?php echo url('/login'); ?>" class="btn_primary">Login Now</a>
        </td>
    </tr>
</table>
@endsection
@extends('layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Hi Admin,</strong>
                <div class="row">
                    You have recceived new query, Details are listed below
                </div> 
                <div class="row">
                    <ul class="box background">
                        <li><span>Name:</span><span>{{ $emailData['data']['name'] }}</span></li>
                        <li><span>Email:</span><span>{{ $emailData['data']['email'] }}</span></li>
                        <li><span>Phone Number:</span><span>{{ $emailData['data']['phone'] }}</span></li>
                        <li><span>Company:</span><span>{{ $emailData['data']['company'] }}</span></li>
                        <li><span>Message:</span><span>{{ $emailData['data']['message'] }}</span></li>
                    </ul>
                </div>    
            </div>
        </td>
    </tr>
</table>
@endsection
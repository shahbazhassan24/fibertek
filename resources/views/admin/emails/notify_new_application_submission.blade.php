@extends('layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Dear {{ $emailData['data']['tenant_name'] }},</strong>
                <div class="row">{{ $emailData['data']['contractor_name'] }}, from {{ $emailData['data']['contractor_company'] }} company have submitted a PTW application on your behalf on the following works ({{ $emailData['data']['work_location'] }}, {{ $emailData['data']['work_nature'] }})</div>
                <div class="row">If this works(permit no. {{ $emailData['data']['application_number'] }}) is not permitted by you, kindly email us at <a href="mailto:workpermit@apmasia.com.sg">workpermit@apmasia.com.sg</a> or contact us at (65) 6909 2699.</div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <strong class="section-title"><span>Details of Work</span></strong>
            <div class="information">
                <div class="row">
                    <ul class="box">
                        <li><span>Location of Work:</span><span>{{ $emailData['data']['work_location'] }}</span></li>
                        <li><span>Common/Tenanted:</span><span>{{ $emailData['data']['common_area'] }}</span></li>
                        <li><span>Nature of Work:</span><span>{{ $emailData['data']['work_nature'] }}</span></li>
                        <li><span>Unit/Colour Zone/Near Pillar:</span><span>{{ $emailData['data']['unit_number'] }}</span></li>
                        <li><span>Details of Work:</span><span>{{ $emailData['data']['work_details'] }}</span></li>
                        <li><span>Duration:</span><span>{{ $emailData['data']['work_duration'] }}</span></li>
                        <li><span>Day/night Works:</span><span>{{ $emailData['data']['day_night'] }}</span></li>
                        <li><span>Special Work:</span><span>{{ $emailData['data']['special_work'] }}</span></li>
                    </ul>
                </div>   
            </div>
        </td>
    </tr>
    <?php /*<tr>
        <td align="center" valign="top">
           <a href="<?php echo url('/approver/application/view/'.$emailData['data']['application_id']) ?>" class="btn_primary">View Application</a>
        </td>
    </tr>*/ ?>
</table>
@endsection
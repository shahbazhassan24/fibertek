@extends('layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Hi {{ $emailData['data']['name'] }},</strong>
                <div class="row">A new PTW application has been submitted. Please review details and documents provided by the applicant.</div> 
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <strong class="section-title"><span>Submission Details</span></strong>
            <div class="information">
                <div class="row">
                    <ul class="box">
                        <li><span>Application #:</span><span>{{ $emailData['data']['application_number'] }}</span></li>
                        <li><span>Company Name:</span><span>{{ $emailData['data']['application_company_name'] }}</span></li>
                        <li><span>Name:</span><span>{{ $emailData['data']['application_name'] }}</span></li>
                        <li><span>Office Number:</span><span>{{ $emailData['data']['application_office_number'] }}</span></li>
                        <li><span>Mobile Number:</span><span>{{ $emailData['data']['application_mobile_number'] }}</span></li>
                        <li><span>Email:</span><span>{{ $emailData['data']['application_email'] }}</span></li>
                    </ul>
                </div>   
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <strong class="section-title"><span>Details of Work</span></strong>
            <div class="information">
                <div class="row">
                    <ul class="box">
                        <li><span>Location of Work:</span><span>{{ $emailData['data']['work_location'] }}</span></li>
                        <li><span>Common/Tenanted:</span><span>{{ $emailData['data']['common_area'] }}</span></li>
                        <li><span>Nature of Work:</span><span>{{ $emailData['data']['work_nature'] }}</span></li>
                        <li><span>Unit/Colour Zone/Near Pillar:</span><span>{{ $emailData['data']['unit_number'] }}</span></li>
                        <li><span>Details of Work:</span><span>{{ $emailData['data']['work_details'] }}</span></li>
                        <li><span>Duration:</span><span>{{ $emailData['data']['work_duration'] }}</span></li>
                        <li><span>Day/night Works:</span><span>{{ $emailData['data']['day_night'] }}</span></li>
                        <li><span>Special Work:</span><span>{{ $emailData['data']['special_work'] }}</span></li>
                    </ul>
                </div>   
            </div>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
           <a href="<?php echo url('/approver/application/view/'.$emailData['data']['application_id']) ?>" class="btn_primary">View Application</a>
        </td>
    </tr>
</table>
@endsection
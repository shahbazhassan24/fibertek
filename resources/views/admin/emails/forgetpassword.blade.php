@extends('layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <strong class="inner-heading">Hi {{ $emailData['data']['name'] }},</strong>
            <div class="information">
                <div class="row">We received a request to reset your password for your account: <a href="mailto:{{ $emailData['data']['email'] }}">{{ $emailData['data']['email'] }}</a></div>
                <div class="row">Simply click on the button to set a new password.</div> 
            </div>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
           <a href="<?php echo asset('reset-password?token='.$emailData['data']['token']) ?>" class="btn_primary">Set a New Password</a>
        </td>
    </tr>
</table>
@endsection
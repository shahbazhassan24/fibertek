@extends('layouts.email')
@section('content')
<table border="0" width="100%">
     <tr>
        <td class="main-heading">
            Your PTW Application has been {{ $emailData['data']['status'] }}!
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong>{{ $emailData['data']['work_location'] }} ({{ $emailData['data']['common_area'] }})</strong>
                <div class="row small-margin">Application #: <span>{{ $emailData['data']['application_number'] }}</span></div>
                <div class="row small-margin">{{ $emailData['data']['work_nature'] }}</div>
                <div class="row small-margin">{{ $emailData['data']['work_duration'] }}</div>
                <div class="row small-margin">Status: <span class="status-{{ $emailData['data']['status_class'] }}">{{ $emailData['data']['status'] }}</span></div> 
            </div>
        </td>
    </tr>
    <?php if(isset($emailData['data']['remarks']) && ($emailData['data']['remarks'] != '')){ ?>
    <tr>
        <td>
            <strong class="section-title"><span>Remarks</span></strong>
            <div class="information">
                <div class="row">{{ $emailData['data']['remarks'] }}</div>   
            </div>
        </td>
    </tr>
    <?php } ?>
    <tr>
        <td align="center" valign="top">
           <a href="<?php echo url('/application/view/'.$emailData['data']['application_id']) ?>" class="btn_primary">View Application</a>
        </td>
    </tr>
</table>
@endsection
<?php if ($errors->any()) { ?>
<div class="alert alert-danger icons-alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled"></i>
    </button>
    <p>
    <?php foreach ($errors->all() as $error) { ?>
    	<?=$error ?><br/>
	<?php } ?>
	</p>
</div>
<?php
}
if (Session::has('error')) { ?>
<div class="alert alert-danger icons-alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled"></i>
    </button>
    <p><?php echo Session::get('error') ?></p>
</div>
<?php } 
if (Session::has('success')) { ?>
<div class="alert alert-primary icons-alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled"></i>
    </button>
    <p><?php echo Session::get('success') ?></p>
</div>
<?php } ?>
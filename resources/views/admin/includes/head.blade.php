<head>
	<title>@yield('title') | {{ Helper::getConfigurationByKey('web_title') }}</title>
	<!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	  <![endif]-->
	  <!-- Meta -->
	  <meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	  <!-- CSRF Token -->
	  <meta name="csrf-token" content="{{ csrf_token() }}">

	  <meta name="keywords" content="{{ Helper::getConfigurationByKey('meta_name_keywords') }}">
	  <meta name="description" content="{{ Helper::getConfigurationByKey('meta_name_description') }}">
	  <meta name="author" content="Pixart">
	  <!-- Favicon icon -->
	  <link rel="icon" href="{{ asset('/images/').'/'.Helper::getConfigurationByKey('web_ico') }}" type="image/x-icon">
	  <!-- Google font-->
	  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	  <!-- Required Fremwork -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
	  <!-- jquery file upload Frame work -->
	  <link href="{{ asset('admin/bower_components/jquery.filer/css/jquery.filer.css') }}" type="text/css" rel="stylesheet" />
	  <link href="{{ asset('admin/bower_components/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css') }}" type="text/css" rel="stylesheet" />
	  <!-- Font Awesome -->
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/icon/font-awesome/css/font-awesome.min.css') }}">
	  <!-- themify icon -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/icon/themify-icons/themify-icons.css') }}">
	  <!-- ico font -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/icon/icofont/css/icofont.css') }}">
	  <!-- flag icon framework css -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/pages/flag-icon/flag-icon.min.css') }}">
	  <!-- Menu-Search css -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/pages/menu-search/css/component.css') }}">
	  <!-- Data Table Css -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/pages/data-table/css/buttons.dataTables.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
	  <!-- Switch component css -->
      <link rel="stylesheet" type="text/css" href="{{ asset('admin/bower_components/switchery/dist/switchery.min.css') }}">
	  <!-- Horizontal-Timeline css -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/pages/dashboard/horizontal-timeline/css/style.css') }}">
	  <!-- amchart css -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/pages/dashboard/amchart/css/amchart.css') }}">
	  <!-- flag icon framework css -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/pages/flag-icon/flag-icon.min.css') }}">
	  <!-- Style.css -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/style.css') }}">
	  <!--color css-->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/color/color-1.css') }}" id="color" />

	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/linearicons.css') }}">
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/simple-line-icons.css') }}">
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/ionicons.css') }}">
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/jquery.mCustomScrollbar.css') }}">

	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/jquery.mCustomScrollbar.css') }}">
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/tail.select-modern.css') }}" >

	  <!-- Custom Style.css -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/custom-style.css') }}">
</head>
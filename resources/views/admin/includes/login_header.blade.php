<ul class="menu list_none">
    <li><a href="{{ url('/login/') }}">Home</a></li>
    <li class="<?=(isset($load_page) && $load_page == 'faq') ? 'active' : '';?>"><a href="{{ url('/page/faq') }}">FAQs</a></li>
    <li class="<?=(isset($load_page) && $load_page == 'guide_forms') ? 'active' : '';?>"><a href="{{ url('/page/guides-forms') }}">Guides/Forms</a></li>
    <li class="<?=(isset($load_page) && $load_page == 'ptw_process') ? 'active' : '';?>"><a href="{{ url('/page/ptw-process') }}">PTW Process</a></li>
    <?php 
    $cmsPageWhere = ['status' => '1', 'parent' => '0', 'in_navigation' => '1'];
    $cmsPages = Helper::getCmsPages($cmsPageWhere);
    foreach ($cmsPages as $cmsPageKey => $cmsPageValue) { ?>
    <li class="<?=(isset($page_id) && $page_id == $cmsPageValue->id) ? 'active' : '';?>">
        <a href="{{ url('/page/'.$cmsPageValue->id) }}">{{ $cmsPageValue->title }}</a>
        <?php if ($cmsPageValue->children->count()){ ?>
        <ul class="drop_down">
            @foreach ($cmsPageValue->children as $child)
                <li class="<?=(isset($page_id) && $page_id == $child->id) ? 'active' : '';?>"><a href="{{ url('/page/'.$child->id) }}">{{ $child->title }}</a></li>
            @endforeach
        </ul>
        <?php } ?>
    </li>
    <?php } ?>
</ul>
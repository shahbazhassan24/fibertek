<script>
	var appJSUrl = '<?=route('admin-dashboard');?>';
    var appAssetsUrl = '<?=asset('/');?>';
</script>

<!-- Older IE warning message -->
    <!--[if lt IE 9]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="{{ asset('admin/assets/images/browser/chrome.png') }}" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="{{ asset('admin/assets/images/browser/firefox.png') }}" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="{{ asset('admin/assets/images/browser/opera.png') }}" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="{{ asset('admin/assets/images/browser/safari.png') }}" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="{{ asset('admin/assets/images/browser/ie.png') }}" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jqurey -->
<script type="text/javascript" src="{{ asset('admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/bower_components/tether/dist/js/tether.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{ asset('admin/bower_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{ asset('admin/bower_components/modernizr/modernizr.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/bower_components/modernizr/feature-detects/css-scrollbars.js') }}"></script>
<!-- classie js -->
<script type="text/javascript" src="{{ asset('admin/bower_components/classie/classie.js') }}"></script>
<!-- Rickshow Chart js -->
<script src="{{ asset('admin/bower_components/d3/d3.js') }}"></script>
<script src="{{ asset('admin/bower_components/rickshaw/rickshaw.js') }}"></script>
<!-- Morris Chart js -->
<script src="{{ asset('admin/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('admin/bower_components/morris.js/morris.js') }}"></script>
<!-- Horizontal-Timeline js -->
<script type="text/javascript" src="{{ asset('admin/assets/pages/dashboard/horizontal-timeline/js/main.js') }}"></script>
<!-- amchart js -->
<script type="text/javascript" src="{{ asset('admin/assets/pages/dashboard/amchart/js/amcharts.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/pages/dashboard/amchart/js/serial.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/pages/dashboard/amchart/js/light.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/pages/dashboard/amchart/js/custom-amchart.js') }}"></script>
<!-- jquery file upload js -->
<script src="{{ asset('admin/bower_components/jquery.filer/js/jquery.filer.min.js') }}"></script>
<script src="{{ asset('admin/assets/pages/filer/custom-filer.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/pages/filer/jquery.fileuploads.init.js') }}" type="text/javascript"></script>
<!-- Tail Select js -->
<script type="text/javascript" src="{{ asset('admin/assets/pages/tail/tail.select.min.js') }}"></script>
<!-- Switch component js -->
<script type="text/javascript" src="{{ asset('admin/bower_components/switchery/dist/switchery.min.js') }}"></script>
<!-- data-table js -->
<script src="{{ asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin/assets/pages/data-table/js/jszip.min.js') }}"></script>
<script src="{{ asset('admin/assets/pages/data-table/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin/assets/pages/data-table/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
<!-- ck editor -->
<script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js') }}"></script>
<!-- i18next.min.js -->
<script type="text/javascript" src="{{ asset('admin/bower_components/i18next/i18next.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/bower_components/i18next-xhr-backend/i18nextXHRBackend.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/bower_components/i18next-browser-languagedetector/i18nextBrowserLanguageDetector.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/bower_components/jquery-i18next/jquery-i18next.min.js') }}"></script>
<!-- Custom js -->
<script type="text/javascript" src="{{ asset('admin/assets/pages/dashboard/custom-dashboard.js') }}"></script>
<script src="{{ asset('admin/assets/pages/data-table/js/data-table-custom.js') }}"></script>
<script type="text/javascript">
    var CKEditorUploadOptions = {
        filebrowserUploadUrl: "{{ route('ckeditor-upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    }
</script>
<script type="text/javascript" src="{{ asset('admin/assets/pages/ckeditor/ckeditor-custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/js/script.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/pages/advance-elements/swithces.js') }}"></script>

<!-- pcmenu js -->
<script src="{{ asset('admin/assets/js/pcoded.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/demo-12.js') }}"></script>
<script src="{{ asset('admin/assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/jquery.mousewheel.min.js') }}"></script>
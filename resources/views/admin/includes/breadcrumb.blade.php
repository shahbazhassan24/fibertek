<div class="page-header">
    <div class="page-header-title">
        <h4>@yield('title')</h4>
    </div>
    <div class="page-header-breadcrumb">
        <ul class="breadcrumb-title">
            <li class="breadcrumb-item">
                <a href="{{ route('admin-dashboard') }}">
                    <i class="icofont icofont-home"></i>
                </a>
            </li>
            @if(isset($breadcrumbs))
            @foreach($breadcrumbs as $breadcrumb)
            <li class="breadcrumb-item">
                @if(isset($breadcrumb['url']))
                <a href="{{ $breadcrumb['url'] }}">{{ $breadcrumb['title'] }}</a>
                @else
                {{ $breadcrumb['title'] }}
                @endif
            </li>
            @endforeach
            @endif
        </ul>
    </div>
</div>
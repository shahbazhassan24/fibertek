<div id="remove-item-permanently" class="popup">
    <div class="d_table">
        <div class="v_middle tab_text">
        	<div class="tab_holder">
                <h2 id="remove-item-title"></h2>
                <div class="field_row">
                    <div id='remove-item-text'></div>
                </div>
                <div class="row text-right btns_row">
                    <a href="#" class="cancel_btn btn_primary popup_closer">CANCEL</a>
                    <a href="#" class="btn_primary" id="remove-item-confirm-btn">CONFIRM</a>
                </div>
            </div>
        </div>
    </div>
</div>
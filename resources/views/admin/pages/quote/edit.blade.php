@extends('admin.layouts.app')
@section('title', 'Edit Quote')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card create-product">
            <div class="card-block">
                {{ Form::model($quote, array('route' => array('quotes.update', $quote->id), 'method' => 'PUT')) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="card-block">
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Status</label>
                                <div class="col-sm-9">
                                    <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $quote->status) == "1" ? 'checked' : '' }}/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Products</label>
                                <div class="col-sm-9">
                                @if($quote->products)
                                @php($requestQuoteProducts = json_decode($quote->products, true))
                                    @foreach($requestQuoteProducts as $quoteProduct)
                                    {!! $quoteProduct['title']. ' - ('.$quoteProduct['quantity'].')' !!}<br/>
                                    @endforeach
                                @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="{{ old('name', $quote->name) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Company</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Company" name="company" id="company" value="{{ old('company', $quote->company) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{ old('email', $quote->email) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Phone</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Phone" name="phone" id="phone" value="{{ old('phone', $quote->phone) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Message</label>
                                <div class="col-sm-9">
                                    <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Message" name="message">{!! old('message', $quote->message) !!}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
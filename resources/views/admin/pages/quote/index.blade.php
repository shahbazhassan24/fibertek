@extends('admin.layouts.app')
@section('title', 'Quotes')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mb-3 pull-right">
                        <a href="{{ route('export-quotes', 'xls') }}" class="btn btn-inverse btn-round">Export</a>
                            <!-- <a href="{{ route('quotes.create') }}" class="btn btn-inverse btn-round">Add Quote</a> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Name</th>
                                        <th>Company</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Products</th>
                                        <th>Active</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php ($i = 1)
                                    @foreach ($quotes as $quote)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $quote->created_at->format('j M Y') }}</td>
                                        <td>{{ $quote->name }}</td>
                                        <td>{{ $quote->company }}</td>
                                        <td>{{ $quote->email }}</td>
                                        <td>{{ $quote->phone }}</td>
                                        <td>{{ count(json_decode($quote->products, true)) }}</td>
                                        <td>{{ $quote->status ? 'yes' : 'no' }}</td>
                                        <td class="column-operations">
                                            <a href="{{ route('quotes.edit', $quote->id) }}" class="btn btn-inverse btn-round">Edit</a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['quotes.destroy', $quote->id] ]) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-round']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Name</th>
                                        <th>Company</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Product</th>
                                        <th>Active</th>
                                        <th>Option</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
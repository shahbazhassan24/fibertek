@extends('admin.layouts.app')
@section('title', 'Create Subscriber')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card create-product">
            <div class="card-block">
                {{ Form::open(array('route' => array('subscriber.store'))) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="card-block">
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Status</label>
                                <div class="col-sm-9">
                                    <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status') == "1" ? 'checked' : '' }}/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Company</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Company" name="company" id="company" value="{{ old('company') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Source</label>
                                <div class="col-sm-9">
                                    <select name="how" class="form-control">
                                        <option value="">Please choose</option>
                                        <option value="Referral">Referral</option>
                                        <option value="Printed Advertising">Printed Advertising</option>
                                        <option value="Search ">Search </option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">How Know</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="How Know" name="how_source" id="how_source" value="{{ old('how_source') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')
@section('title', 'Edit Certification')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                {{ Form::model($certification, array('route' => array('certifications.update', $certification->id), 'files' => true, 'method' => 'PUT')) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                                <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $certification->status) == "1" ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Title</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Title" name="title" id="title" value="{{ old('title', $certification->title) }}">
                            </div>
                        </div>
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Image <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will use for all places"><i class="fa fa-question-circle"></i></a>
                            </label>
                            <div class="col-sm-4">
                                <input type="file" name="image" id="image" class="form-control">
                                <div class="image-size-note"><span>Note:</span>Width size should be 150 Pixel.</div>
                            </div>
                            <div class="col-sm-5">
                                @if(isset($certification->image) && $certification->image)
                                <a href="{{ asset('images/certification/'.$certification->id)}}/{{ $certification->image }}" target="_blank">{{ $certification->image }}</a>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" placeholder="Alt Text" name="image_alt" id="image_alt" value="{{ ($certification->image_alt) ? $certification->image_alt : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Description</label>
                            <div class="col-sm-9">
                                <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Description" name="description">{{ old('description', $certification->description) }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Document</label>
                            <div class="col-sm-4">
                                <input type="file" name="document" id="document" class="form-control">
                            </div>
                            <div class="col-sm-5">
                                @if(isset($certification->document) && $certification->document)
                                <a href="{{ asset('images/certification/'.$certification->id)}}/{{ $certification->document }}" target="_blank">{{ $certification->document }}</a>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
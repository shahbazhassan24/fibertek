@extends('admin.layouts.app')
@section('title', 'Create Enquiry')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card create-product">
            <div class="card-block">
                {{ Form::open(array('route' => array('contact-enquiries.store'))) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="card-block">
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Status</label>
                                <div class="col-sm-9">
                                    <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status') == "1" ? 'checked' : '' }}/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Company</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Company" name="company" id="company" value="{{ old('company') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Phone</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Phone" name="phone" id="phone" value="{{ old('phone') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Source</label>
                                <div class="col-sm-9">
                                <select name="hear" class="form-control">
                                    <option value="">How did you hear about us?</option>
                                    <option value="Search Engine (Google, Yahoo etc)" {{ old('how_hear') == 'Search Engine (Google, Yahoo etc)' ? 'selected' : '' }}>Search Engine (Google, Yahoo etc)</option>
                                    <option value="Social Media ( Facebook, YouTube etc)" {{ old('how_hear') == 'Social Media ( Facebook, YouTube etc)' ? 'selected' : '' }}>Social Media ( Facebook, YouTube etc)</option>
                                    <option value="Print Media" {{ old('how_hear') == 'Print Media' ? 'selected' : '' }}>Print Media</option>
                                    <option value="Existing Customer" {{ old('how_hear') == 'Existing Customer' ? 'selected' : '' }}>Existing Customer</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Feedback or Enquiry</label>
                                <div class="col-sm-9">
                                    <textarea id="editor-enquiry" class="content-editor form-control" placeholder="Feedback or Enquiry" name="enquiry">{{ old('enquiry') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')
@section('title', 'Edit Contact Enquiry')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card create-product">
            <div class="card-block">
                {{ Form::model($contactEnquiry, array('route' => array('contact-enquiries.update', $contactEnquiry->id), 'method' => 'PUT')) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="card-block">
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Status</label>
                                <div class="col-sm-9">
                                    <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $contactEnquiry->status) == "1" ? 'checked' : '' }}/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="{{ old('name', $contactEnquiry->name) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Company</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Company" name="company" id="company" value="{{ old('company', $contactEnquiry->company) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{ old('email', $contactEnquiry->email) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Phone</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Phone" name="phone" id="phone" value="{{ old('phone', $contactEnquiry->phone) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Source</label>
                                <div class="col-sm-9">
                                <select name="hear" class="form-control">
                                    <option value="">How did you hear about us?</option>
                                    <option value="Search Engine (Google, Yahoo etc)" {{ old('hear', $contactEnquiry->hear) == 'Search Engine (Google, Yahoo etc)' ? 'selected' : '' }}>Search Engine (Google, Yahoo etc)</option>
                                    <option value="Social Media ( Facebook, YouTube etc)" {{ old('hear', $contactEnquiry->hear) == 'Social Media ( Facebook, YouTube etc)' ? 'selected' : '' }}>Social Media ( Facebook, YouTube etc)</option>
                                    <option value="Print Media" {{ old('hear', $contactEnquiry->hear) == 'Print Media' ? 'selected' : '' }}>Print Media</option>
                                    <option value="Existing Customer" {{ old('hear', $contactEnquiry->hear) == 'Existing Customer' ? 'selected' : '' }}>Existing Customer</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Feedback or Enquiry</label>
                                <div class="col-sm-9">
                                    <textarea id="editor-enquiry" class="content-editor form-control" placeholder="Feedback or Enquiry" name="enquiry">{{ old('enquiry', $contactEnquiry->enquiry) }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Added At</label>
                                <div class="col-sm-9">
                                    {{ $contactEnquiry->created_at->format('d-M-Y H:i:s') }}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
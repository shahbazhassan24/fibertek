@extends('admin.layouts.app')
@section('title', 'Edit Catalogue')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                {{ Form::model($homeExploreProduct, array('route' => array('home-exlpore-product.update', $homeExploreProduct->id), 'files' => true, 'method' => 'PUT')) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="details" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Status</label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $homeExploreProduct->status) == "1" ? 'checked' : '' }}/>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Index <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Lower index will be showed first, leave all blank if want to arranged automatically by name."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="sort" id="sort" min="0" value="{{ old('sort', $homeExploreProduct->sort) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Title" name="title" id="title" value="{{ old('title', $homeExploreProduct->title) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Url</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Title" name="url" id="url" value="{{ old('url', $homeExploreProduct->url) }}">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Image <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will use for all places"><i class="fa fa-question-circle"></i></a>
                                    </label>
                                    <div class="col-sm-3">
                                        <input type="file" name="image" id="image" class="form-control">
                                        <div class="image-size-note"><span>Note:</span>Width size should be 150 Pixel.</div>
                                    </div>
                                    <div class="col-sm-3">
                                        @if(isset($homeExploreProduct->image) && $homeExploreProduct->image)
                                        <a href="{{ asset('images/home/explore-product/'.$homeExploreProduct->id)}}/{{ $homeExploreProduct->image }}" target="_blank">{{ $homeExploreProduct->image }}</a>
                                        @endif
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="Alt Text" name="image_alt" id="image_alt" value="{{ ($homeExploreProduct->image_alt) ? $homeExploreProduct->image_alt : '' }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
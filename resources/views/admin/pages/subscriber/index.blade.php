@extends('admin.layouts.app')
@section('title', 'Subscribers')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mb-3 pull-right">
                            <a href="{{ route('export-subscribers', 'xls') }}" class="btn btn-inverse btn-round">Export</a>
                            <a href="{{ route('subscriber.create') }}" class="btn btn-inverse btn-round">Add Subscriber</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Company</th>
                                        <th>Email</th>
                                        <!-- <th>Know</th> -->
                                        <th>Added At</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php ($i = 1)
                                    @foreach ($subscribers as $subscriber)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $subscriber->name }}</td>
                                        <td>{{ $subscriber->company }}</td>
                                        <td>{{ $subscriber->email }}</td>
                                        <!-- <td>{{ $subscriber->how }} : {{ $subscriber->how_source }}</td>-->
                                        <td>{{ $subscriber->created_at->format('d-M-Y H:i:s') }}</td>
                                        <td class="column-operations">
                                            <a href="{{ route('subscriber.edit', $subscriber->id) }}" class="btn btn-inverse btn-round">Edit</a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['subscriber.destroy', $subscriber->id] ]) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-round']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Company</th>
                                        <th>Email</th>
                                        <th>Know</th>
                                        <th>Active</th>
                                        <th>Option</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
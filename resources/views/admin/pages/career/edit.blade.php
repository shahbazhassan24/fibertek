@extends('admin.layouts.app')
@section('title', 'Edit Career')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                {{ Form::model($career, array('route' => array('career.update', $career->id), 'method' => 'PUT')) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#details" role="tab">Job Details</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#categories" role="tab">Categories</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="details" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Status</label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $career->status) == "1" ? 'checked' : '' }}/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Job Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Job Title" name="title" id="title" value="{{ old('title', $career->title) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Country</label>
                                    <div class="col-sm-9">
                                        <select name="country_id" class="form-control">
                                            <option value="">Please choose</option>
                                            @foreach($countries as $country)
                                            <option value="{{ $country->id }}" {{ old('country_id', $career->country_id) ==  $country->id ? 'selected' : '' }}>{{ $country->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Job Description</label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Job Description" name="job_description">{{ old('job_description', $career->job_description) }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Requirements</label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-qualification" class="content-editor form-control" placeholder="Enter Qualifications & Requirements" name="qualification">{{ old('qualification', $career->qualification) }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="categories" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-12">
                                        @foreach($categories as $category)
                                            <ul class="categories-main">
                                                <li>
                                                    <div class="checkbox-fade fade-in-primary">
                                                        <label>
                                                            <input type="checkbox" id="checkbox{{$category->id}}"value="{{$category->id}}" name="categories[{{$category->id}}]"
                                                            {{ (old('categories.'.$category->id) == $category->id) || (isset($career) && in_array($category->id, $career->categories->pluck('id')->toArray())) ? 'checked' : '' }}>
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                            </span>
                                                            <span>{{$category->title}}</span>
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')
@section('title', 'Edit Career Category')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                {{ Form::model($lifeFibertek, array('route' => array('career-life-fibertek.update', $lifeFibertek->id), 'method' => 'PUT', 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                                <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $lifeFibertek->status) == "1" ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Title</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Title" name="title" id="title" value="{{ old('title', $lifeFibertek->title) }}">
                            </div>
                        </div>
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Image
                            </label>
                            <div class="col-sm-4">
                                <input type="file" name="image" id="image" class="form-control">
                                <div class="image-size-note"><span>Note:</span>Width size should be 540 Pixel.</div>
                            </div>
                            <div class="col-sm-5">
                                @if(isset($lifeFibertek->image) && $lifeFibertek->image)
                                <a href="{{ asset('/images/career/lifeFibertek/')}}/{{ $lifeFibertek->image }}" target="_blank">{{ $lifeFibertek->image }}</a>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Description</label>
                            <div class="col-sm-9">
                                <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Description" name="description">{{ old('description', $lifeFibertek->description) }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')
@section('title', 'Add Career Images')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                {{ Form::open(array('route' => array('career-image.store'), 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <div class="image-size-note"><span>Note:</span>Width size should be 540 Pixel.</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Career Slider Images</label>
                            <div class="col-sm-9">
                                <input type="file" name="images[]" class="filer_input" multiple="multiple" data-jfiler-extensions ="jpg,jpeg,png,gif">
                                <?php 
                                $imagesArray = [];
                                if(isset($images) && ($images->isNotEmpty())){
                                    foreach($images as $imageKey => $image){
                                        $imagesArray[$imageKey]['name'] = $image->name;
                                        $imagesArray[$imageKey]['file'] = asset('/images/career/').'/'.$image->name;
                                        //$imagesArray[$imageKey]['file'] = public_path('\images\career\\'.$image->name);
                                        //$imagesArray[$imageKey]['type'] = Helper::getMimeType($image->name);
                                        $imagesArray[$imageKey]['size'] = 145;
                                    }
                                }
                                ?>
                                <script>
                                    var uploadedFiles = <?php echo json_encode($imagesArray)?>;
                                </script>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
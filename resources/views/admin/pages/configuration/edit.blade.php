@extends('admin.layouts.app')
@section('title', 'Configurations')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block configurations">
                {{ Form::model($configurations, array('route' => array('configurations.update'), 'method' => 'PUT', 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#website-data" role="tab">Website Data</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#email-configuration" role="tab">Email Configuration</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#social-media-links" role="tab">Social Media Links</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#custom-scripts" role="tab">Custom Scripts</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#sitemap" role="tab">Sitemap</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="website-data" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Maintanance Mode <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Enable : website is on maintenance mode, the content can be updated from Content Managment System"><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-single" name="web_maintenance" id="web_maintenance" value="1"{{ ( ( isset($configurations['web_maintenance']) && $configurations['web_maintenance'] == '1' ) ? 'checked' : '') }}/>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Website Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Enter Website Title" name="web_title" id="web_title" value="{{ ( isset($configurations['web_title']) ? $configurations['web_title'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Web Logo <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Make sure your logo size fit to your theme / web design"><i class="fa fa-question-circle"></i></a>
                                    </label>
                                    <div class="col-sm-3">
                                        <input type="file" name="web_logo" id="web_logo">
                                        <div class="image-size-note"><span>Note:</span>Width size should be 220 Pixel.</div>
                                    </div>
                                    <div class="col-sm-3">
                                        @if(isset($configurations['web_logo']) && $configurations['web_logo'])
                                            <a href="{{ asset('images')}}/{{ $configurations['web_logo'] }}" target="_blank">{{ $configurations['web_logo'] }}</a>
                                        @endif
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="Logo Alt Text" name="web_logo_alt" id="web_logo_alt" value="{{ ( isset($configurations['web_logo_alt']) ? $configurations['web_logo_alt'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Web Icon <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Picture on Browser Tab 16 x 16 pixel"><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-4">
                                        <input type="file" name="web_ico" id="web_ico">
                                    </div>
                                    <div class="col-sm-5">
                                        @if(isset($configurations['web_ico']) && $configurations['web_ico'])
                                        <a href="{{ asset('images') }}/{{ $configurations['web_ico'] }}" target="_blank">{{ $configurations['web_ico'] }}</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Header Tagline</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Enter Tagline" name="tagline" id="tagline" value="{{ ( isset($configurations['tagline']) ? $configurations['tagline'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Online Shop Link</label>
                                    <div class="col-sm-9">
                                        <input type="url" class="form-control" placeholder="Online Shop Link" name="online_shop_link" id="online_shop_link" value="{{ ( isset($configurations['online_shop_link']) ? $configurations['online_shop_link'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Contact Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" placeholder="Enter Email" name="contact_email" id="contact_email" value="{{ ( isset($configurations['contact_email']) ? $configurations['contact_email'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Contact Phone</label>
                                    <div class="col-sm-9">
                                        <input type="phone" class="form-control" placeholder="Enter Phone" name="contact_phone" id="contact_phone" value="{{ ( isset($configurations['contact_phone']) ? $configurations['contact_phone'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Fax Number</label>
                                    <div class="col-sm-9">
                                        <input type="phone" class="form-control" placeholder="Enter Fax Number" name="fax_number" id="fax_number" value="{{ ( isset($configurations['fax_number']) ? $configurations['fax_number'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Website Copyright</label>
                                    <div class="col-sm-9">
                                        <textarea rows="5" cols="5" class="form-control" placeholder="Website Copyright" name="copyright" id="copyright">{{ ( isset($configurations['copyright']) ? $configurations['copyright'] : '') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">SEO Description <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This is used on Search Engine Optimization, write your website description."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <textarea rows="5" cols="5" class="form-control" placeholder="SEO Description" name="meta_name_description" id="meta_name_description">{{ ( isset($configurations['meta_name_description']) ? $configurations['meta_name_description'] : '') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">SEO Keywords <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This is used on Search Engine Optimization, write keywords that describe your website. Separate with comma (,) for each keywords."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <textarea rows="5" cols="5" class="form-control" placeholder="SEO Keywords" name="meta_name_keywords" id="meta_name_keywords">{{ ( isset($configurations['meta_name_keywords']) ? $configurations['meta_name_keywords'] : '') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="email-configuration" role="tabpanel">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Email From</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" placeholder="From Email" name="email_from" id="email_from" value="{{ ( isset($configurations['email_from']) ? $configurations['email_from'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Email Enquiry (To/CC)</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" placeholder="Enquiry Email" name="email_enquiry" id="email_enquiry" value="{{ ( isset($configurations['email_enquiry']) ? $configurations['email_enquiry'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Email Career</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" placeholder="Email Career" name="email_enquiry_career" id="email_enquiry_career" value="{{ ( isset($configurations['email_enquiry_career']) ? $configurations['email_enquiry_career'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SMTP Host</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="SMTP Host" name="smtp_host" id="smtp_host" value="{{ ( isset($configurations['smtp_host']) ? $configurations['smtp_host'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SMTP Port</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="SMTP Port" name="smtp_port" id="smtp_port" value="{{ ( isset($configurations['smtp_port']) ? $configurations['smtp_port'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SMTP User</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="SMTP User" name="smtp_user" id="smtp_user" value="{{ ( isset($configurations['smtp_user']) ? $configurations['smtp_user'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SMTP Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" placeholder="****************" name="smtp_password" id="smtp_password" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="social-media-links" role="tabpanel">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Facebook Link</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="https://www.facebook.com/" name="facebook_link" id="facebook_link" value="{{ ( isset($configurations['facebook_link']) ? $configurations['facebook_link'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Twitter Link</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="https://www.twitter.com/" name="twitter_link" id="twitter_link" value="{{ ( isset($configurations['twitter_link']) ? $configurations['twitter_link'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Youtube Link</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="https://www.youtube.com/" name="youtube_link" id="youtube_link" value="{{ ( isset($configurations['youtube_link']) ? $configurations['youtube_link'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Instagram Link</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="https://www.instagram.com/" name="instagram_link" id="instagram_link" value="{{ ( isset($configurations['instagram_link']) ? $configurations['instagram_link'] : '') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Linkedin Link</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="https://www.linkedin.com/" name="linkedin_link" id="linkedin_link" value="{{ ( isset($configurations['linkedin_link']) ? $configurations['linkedin_link'] : '') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="custom-scripts" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Custom Script <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This script will be loaded inside <head></head>. You can put any script here, for example google analytics."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <textarea rows="15" cols="5" class="form-control" placeholder="Custom Script" name="custom_script" id="custom_script">{{ ( isset($configurations['custom_script']) ? $configurations['custom_script'] : '') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="sitemap" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Sitemap XML <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="File must be sitemap.xml"><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-4">
                                        <input type="file" name="sitemap_file" id="sitemap_file">
                                    </div>
                                    <div class="col-sm-5">
                                        @if(isset($configurations['sitemap_file']) && $configurations['sitemap_file'])
                                        <a href="{{ url('/') }}/{{ $configurations['sitemap_file'] }}" target="_blank">{{ $configurations['sitemap_file'] }}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')
@section('title', $pageTitle)
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block configurations">
                {{ Form::model($configurations, array('route' => array('configurations.update'), 'method' => 'PUT', 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="website-data" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Footer First Column <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will display as first column in Footer"><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-footer-links-1" class="content-editor form-control" placeholder="First Column Links" name="footer_links_1">{{ ( isset($configurations['footer_links_1']) ? $configurations['footer_links_1'] : '') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Footer Second Column <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will display as second column in Footer"><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-footer-links-2" class="content-editor form-control" placeholder="Second Column Links" name="footer_links_2">{{ ( isset($configurations['footer_links_2']) ? $configurations['footer_links_2'] : '') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Footer Third Column <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will display as third column in Footer"><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-footer-links-3" class="content-editor form-control" placeholder="Third Column Links" name="footer_links_3">{{ ( isset($configurations['footer_links_3']) ? $configurations['footer_links_3'] : '') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Footer Fourth Column <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will display as fourth column in Footer"><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-footer-links-4" class="content-editor form-control" placeholder="Forth Column" name="footer_links_4">{{ ( isset($configurations['footer_links_4']) ? $configurations['footer_links_4'] : '') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
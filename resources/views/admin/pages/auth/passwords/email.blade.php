@extends('admin.layouts.login')
@section('content')
<div class="login">
    <div class="d_table">
        <div class="v_middle">
            <div class="login_form">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="POST" action="{{ route('admin-password.email') }}" autocomplete="off" id="forgot-password-form">
                    @csrf
                    <strong>{{ __('Reset Password') }}</strong>
                    <div class="fields">
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/user.png') }}" alt="#">
                            </div>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="johndoe@gmail.com" autofocus autocomplete="off">
                        </div>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="row no_border">
                            <input type="submit" value="Reset Password">
                        </div>
                        <div class="user_links">
                            @if (Route::has('admin-login'))
                                <a href="{{ route('admin-login') }}">
                                    {{ __('Already has account') }}
                                </a>
                            @endif
                            @if (Route::has('register'))
                                <!-- <a href="{{ route('register') }}">
                                    {{ __('Create a new account') }}
                                </a> -->
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

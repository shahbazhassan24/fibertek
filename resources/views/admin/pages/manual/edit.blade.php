@extends('admin.layouts.app')
@section('title', 'Edit Manual')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                {{ Form::model($manual, array('route' => array('manuals.update', $manual->id), 'files' => true, 'method' => 'PUT')) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#details" role="tab">Manual Details</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#categories" role="tab">Categories</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="details" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Status</label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $manual->status) == "1" ? 'checked' : '' }}/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Title" name="title" id="title" value="{{ old('title', $manual->title) }}">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Image <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will use for all places"><i class="fa fa-question-circle"></i></a>
                                    </label>
                                    <div class="col-sm-3">
                                        <input type="file" name="image" id="image" class="form-control">
                                        <div class="image-size-note"><span>Note:</span>Width size should be 150 Pixel.</div>
                                    </div>
                                    <div class="col-sm-3">
                                        @if(isset($manual->image) && $manual->image)
                                        <a href="{{ asset('images/manual/'.$manual->id)}}/{{ $manual->image }}" target="_blank">{{ $manual->image }}</a>
                                        @endif
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="Alt Text" name="image_alt" id="image_alt" value="{{ ($manual->image_alt) ? $manual->image_alt : '' }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Description</label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Description" name="description">{{ old('description', $manual->description) }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Document</label>
                                    <div class="col-sm-4">
                                        <input type="file" name="document" id="document" class="form-control">
                                    </div>
                                    <div class="col-sm-5">
                                        @if(isset($manual->document) && $manual->document)
                                        <a href="{{ asset('documents/manual/'.$manual->id)}}/{{ $manual->document }}" target="_blank">{{ $manual->document }}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="categories" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-12">
                                        @foreach($categories as $category)
                                            <ul class="categories-main">
                                                <li>
                                                    <div class="checkbox-fade fade-in-primary">
                                                        <label>
                                                            <input type="checkbox" id="checkbox{{$category->id}}"value="{{$category->id}}" name="categories[{{$category->id}}]"
                                                            {{ (old('categories.'.$category->id) == $category->id) || (isset($manual) && in_array($category->id, $manual->categories->pluck('id')->toArray())) ? 'checked' : '' }}
                                                            >
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                            </span>
                                                            <span>{{$category->title}}</span>
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
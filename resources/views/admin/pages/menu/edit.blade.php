@extends('admin.layouts.app')
@section('title', 'Edit Category')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                @php ($editUrl = $menu->id)
                @if(isset($_GET['sub']))
                @php ($editUrl .= '?sub='.$_GET['sub'])
                @endif
                {{ Form::model($menu, array('route' => ['menus.update', $editUrl], 'method' => 'PUT', 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="category-details" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Status <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Enable : it will show on website."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $menu->status) == "1" ? 'checked' : '' }} />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Parent Menu</label>
                                    <div class="col-sm-9">
                                        <select name="parent_id" class="form-control">
                                            <option value="">No Parent</option>
                                            @foreach ($allMenus as $singleMenu)
                                            <option value="{{ $singleMenu->id }}" {{ old('parent_id', $menu->parent_id) == $singleMenu->id ? 'selected' : '' }}>{{ $singleMenu->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Index <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Lower index will be showed first, leave all blank if want to arranged automatically by name."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="sort" id="sort" min="0" value="{{ old('sort', $menu->sort) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Menu Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Menu Title" name="title" id="title" value="{{ old('title', $menu->title) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Menu Link</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Menu Link" name="url" id="url" value="{{ old('title', $menu->url) }}">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Menu Icon <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will use for sub menu icon"><i class="fa fa-question-circle"></i></a>
                                    </label>
                                    <div class="col-sm-3">
                                        <input type="file" name="image" id="image" class="form-control">
                                        <div class="image-size-note"><span>Note:</span>Width size should be 50 Pixel.</div>
                                    </div>
                                    <div class="col-sm-3">
                                        @if(isset($menu->image) && $menu->image)
                                        <a href="{{ asset('images/menu/'.$menu->id)}}/{{ $menu->image }}" target="_blank">{{ $menu->image }}</a>
                                        @endif
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="Alt Text" name="image_alt" id="image_alt" value="{{ ($menu->image_alt) ? $menu->image_alt : '' }}">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">New tab <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Enable : it will open link in new Window."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-switch" name="new_tab" id="new_tab" value="1" {{ old('new_tab', $menu->new_tab) == "1" ? 'checked' : '' }}/>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Short Description <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will display on sub menu"><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-short-description" class="content-editor form-control" placeholder="Enter Short Description" name="short_description">{{ old('short_description', $menu->short_description) }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
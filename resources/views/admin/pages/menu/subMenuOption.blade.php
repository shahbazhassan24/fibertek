@foreach($subcategories as $subcategory)
    <ul class="categories-sub">
        <li>
        	<div class="checkbox-fade fade-in-primary">
                <label>
                    <input type="checkbox" id="checkbox{{$subcategory->id}}" value="{{$subcategory->id}}" name="categories[{{$subcategory->id}}]" {{ (old('categories.'.$subcategory->id) == $subcategory->id) || (isset($product) && in_array($subcategory->id, $product->categories->pluck('id')->toArray())) ? 'checked' : '' }}>
                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                    </span>
                    <span>{{$subcategory->title}}</span>
                </label>
            </div>
	    </li>
    @if(count($subcategory->subCategories))
        @include('admin.pages.category.subCategoryOption', ['subcategories' => $subcategory->subCategories])
    @endif
    </ul>
@endforeach
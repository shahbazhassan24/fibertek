@foreach($subcategories as $subcategory)
    <ul class="categories-sub">
        <li>{{$subcategory->title}}</li> 
    @if(count($subcategory->subCategories))
        @include('admin.pages.category.subcategory', ['subcategories' => $subcategory->subCategories])
    @endif
    </ul> 
@endforeach
@extends('admin.layouts.app')
@section('title', 'Main Menu')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mb-3 pull-right">
                            @php ($addUrlArray = [])
                            @if(isset($_GET['sub']))
                            @php ($addUrlArray['sub'] = $_GET['sub'])
                            @endif
                            <a href="{{ route('menus.create', $addUrlArray) }}" class="btn btn-inverse btn-round">Add Menu Item</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Active</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php ($i = 1)
                                    @foreach ($menus as $menu)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $menu->title }}</td>
                                        <td>{{ $menu->status ? 'yes' : 'no' }}</td>
                                        <td class="column-operations">
                                            @if($menu->subMenus->isNotEmpty())
                                            <a href="{{ route('menus.index', ['sub' => $menu->id]) }}" class="btn btn-inverse btn-round">Sub Menu</a>
                                            @endif
                                            @php ($editUrlArray = [$menu->id])
                                            @if(isset($_GET['sub']))
                                            @php ($editUrlArray['sub'] = $_GET['sub'])
                                            @endif
                                            <a href="{{ route('menus.edit', $editUrlArray) }}" class="btn btn-inverse btn-round">Edit</a>
                                            @php ($deleteFormParam = $menu->id)
                                            @if(isset($_GET['sub']))
                                            @php ($deleteFormParam .= '?sub='.$_GET['sub'])
                                            @endif
                                            {{ Form::model($menu, array('route' => array('menus.destroy', $deleteFormParam), 'method' => 'DELETE')) }}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-round']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Active</th>
                                        <th>Option</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')
@section('title', 'Create Menu Item')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                @php ($addUrl = '')
                @if(isset($_GET['sub']))
                @php ($addUrl = 'sub='.$_GET['sub'])
                @endif
                {{ Form::open(array('route' => array('menus.store', $addUrl), 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="category-details" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Status <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Enable : it will show on website."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status') == "1" ? 'checked' : '' }} />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Parent Menu</label>
                                    <div class="col-sm-9">
                                        <select name="parent_id" class="form-control">
                                            <option value="">No Parent</option>
                                            @foreach ($allMenus as $menu)
                                            <option value="{{ $menu->id }}" {{ old('parent_id') == $menu->id ? selected : '' }}>{{ $menu->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Index <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Lower index will be showed first, leave all blank if want to arranged automatically by name."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="sort" id="sort" min="0" value="{{ old('sort', '0') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Menu Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Menu Title" name="title" id="title" value="{{ old('title') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Menu Link</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Menu Link" name="url" id="url" value="{{ old('url') }}">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Menu icon <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will use for sub menu icon"><i class="fa fa-question-circle"></i></a>
                                    </label>
                                    <div class="col-sm-3">
                                        <input type="file" name="image" id="image" class="form-control">
                                        <div class="image-size-note"><span>Note:</span>Width size should be 50 Pixel.</div>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="Alt Text" name="image_alt" id="image_alt" value="">
                                    </div>
                                    <div class="col-sm-3">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">New Tab <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Enable : it will open link in new Window."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-switch" name="new_tab" id="new_tab" value="1" {{ old('new_tab') == "1" ? 'checked' : '' }} />
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Short Description <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will display on sub menu"><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-short-description" class="content-editor form-control" placeholder="Enter Short Description" name="short_description">{{ old('short_description') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')
@section('title', 'Edit Product')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card edit-product">
            <div class="card-block">
                {{ Form::model($product, array('route' => ['products.update', $formUrl], 'method' => 'PUT', 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#product-details" role="tab">Product Details</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#categories" role="tab">Categories</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#images" role="tab">Images</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#pdf-document" role="tab">PDF</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#related" role="tab">Related Products</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="product-details" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Status</label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $product->status) == "1" ? 'checked' : '' }}/>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Index <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Lower index will be showed first, leave all blank if want to arranged automatically by name."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="sort" id="sort" min="0" value="{{ old('sort', $product->sort) }}">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Product SKU <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Stock Keeping Unit, unique code for each product"><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Product SKU" name="sku" id="sku" value="{{ old('sku', $product->sku) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Product Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Product Title" name="title" id="title" value="{{ old('title', $product->title) }}">
                                    </div>
                                </div>
                                <!-- <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Price</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Product Price" name="price" id="price" value="{{ old('price') }}">
                                    </div>
                                </div> -->
                                <!-- <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Netweight</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Netweight" name="netweight" id="netweight" value="{{ old('netweight') }}">
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Short Description</label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-short-description" class="content-editor form-control" placeholder="Enter Short Description" name="short_description">{{ old('short_description', $product->short_description) }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Description</label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Description" name="description">{{ old('description', $product->description) }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Featured <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Enable : it will make this featured."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-switch" name="is_featured" id="is_featured" value="1" {{ old('is_featured', $product->is_featured) == "1" ? 'checked' : '' }}/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Featured Description</label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-featured-description" class="content-editor form-control" placeholder="Enter Featured Description" name="featured_description">{{ old('featured_description', $product->featured_description) }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="categories" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-12">
                                        @foreach($categories as $catKey => $category)
                                            <ul class="categories-main">
                                                <li>
                                                    <div class="checkbox-fade fade-in-primary">
                                                        <label>
                                                            <input type="checkbox" id="checkbox{{$category->id}}"value="{{$category->id}}" name="categories[{{$category->id}}]"
                                                            {{ (old('categories.'.$category->id) == $category->id) || (isset($product) && in_array($category->id, $product->categories->pluck('id')->toArray())) ? 'checked' : '' }}
                                                            >
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                            </span>
                                                            <span>{{$category->title}}</span>
                                                        </label>
                                                    </div>
                                                </li>
                                                @if(count($category->subCategories))
                                                     @include('admin.pages.category.subCategoryOption', ['subcategories' => $category->subCategories])
                                                @endif 
                                            </ul>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="images" role="tabpanel">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <div class="image-size-note"><span>Note:</span>Width size should be 600 Pixel.</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Product Images</label>
                                    <div class="col-sm-9">
                                    <?php 
                                    $imagesArray = [];
                                    if(isset($product->images) && ($product->images->isNotEmpty())){
                                        foreach($product->images as $imageKey => $image){ ?>
                                        <div class="row product-image-row mt-2">
                                            <div class="col-sm-4">
                                                <input type="file" class="form-control" name="images[file][]"  accept="jpg,jpeg,png,gif">
                                                <a href="{{ asset('/images/product/'.$product->id).'/'.$image->name }}" class="product-image-admin-url" title="{{ $image->name }}">{{ $image->name }}</a>
                                                <input type="hidden" name="images[ids][]" id="" value="{{ $image->id }}">
                                            </div>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" placeholder="Image Alt Text" name="images[alt][]" id="" value="{{ $image->alt_text }}">
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="number" class="form-control" placeholder="Index" name="images[index][]" min="0" value="{{ $image->sort }}">
                                            </div>
                                            <div class="col-sm-1">
                                                <a href="javascript:void(0)" class="remove-product-image update-remove-product-image pull-right" data-image-id="{{ $image->id }}"><i class="fa fa-minus"></i></a>
                                            </div>
                                        </div>
                                    <?php }
                                    }else{ ?>
                                        <div class="row product-image-row mt-2">
                                            <div class="col-sm-4">
                                                <input type="file" class="form-control" name="images[file][]"  accept="jpg,jpeg,png,gif">
                                                <input type="hidden" name="images[ids][]" id="" value="">
                                            </div>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" placeholder="Image Alt Text" name="images[alt][]" id="" value="">
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="number" class="form-control" placeholder="Index" name="images[index][]" min="0" value="">
                                            </div>
                                            <div class="col-sm-1">
                                                <a href="javascript:void(0)" class="remove-product-image pull-right"><i class="fa fa-minus"></i></a>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <a href="javascript:void(0)" class="add-more-product-image update-add-product-image pull-right"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="pdf-document" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">PDF Download</label>
                                    <div class="col-sm-4">
                                        <input type="file" name="pdf_download" id="pdf_download" class="form-control">
                                    </div>
                                    <div class="col-sm-5">
                                        @if(isset($product->documents) && ($product->documents->isNotEmpty()))
                                        <a href="{{ asset('documents/product/'.$product->id)}}/{{ $product->documents[0]['name'] }}" target="_blank">{{ $product->documents[0]['name'] }}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="related" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group row tooltip-link">
                                            <label class="col-sm-3 col-form-label">Related Products</label>
                                            <div class="col-sm-9">
                                            @php ($productRelatedProducts = [])
                                            @if($product->linkedProducts->isNotEmpty())
                                            @php ($productRelatedProducts = $product->linkedProducts->pluck(['related_product_id'])->toArray())
                                            @endif
                                            <select type="text" name="related_products[]" id="related_products" class="multiple-select" multiple="">
                                            @foreach($relatedProducts as $productKey => $relatedProduct)
                                            <option value="<?=$relatedProduct->id;?>" <?=(in_array($relatedProduct->id, $productRelatedProducts)) ? 'selected' : '';?>><?=$relatedProduct->title;?></option>    
                                            @endforeach
                                            </select>
                                            <div class="tail-move-container"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')
@section('title', 'Products')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <form action="{{ route('products.import') }}" method="POST" name="importform" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-9">
                                        <input type="file" name="import_products" class="form-control">
                                    </div>
                                    <div class="col-sm-3">
                                        <button class="btn btn-inverse btn-round">Import Products</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                            <div class="col-sm-6">
                                <div class="mb-3 pull-right">
                                    <a href="{{ route('products.create', $formParameters) }}" class="btn btn-inverse btn-round">Add Product</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Active</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php ($i = 1)
                                    @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $product->title }}</td>
                                        <td>{{ $product->status ? 'yes' : 'no' }}</td>
                                        <td class="column-operations">
                                            @php ($editUrlParameters = [$product->id])
                                            @if(!empty($formParameters))
                                            @php ($editUrlParameters = $editUrlParameters + $formParameters)
                                            @endif
                                            <a href="{{ route('products.edit', $editUrlParameters) }}" class="btn btn-inverse btn-round">Edit</a>
                                            @php ($deleteFormParam = $product->id)
                                            @if(!empty($formParameters))
                                            @php ($deleteFormParam .= '?'.http_build_query($formParameters))
                                            @endif
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['products.destroy', $deleteFormParam] ]) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-round']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Active</th>
                                        <th>Option</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
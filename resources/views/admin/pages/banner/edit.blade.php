@extends('admin.layouts.app')
@section('title', 'Edit Banner')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                {{ Form::model($banner, array('route' => array('banners.update', $banner->id), 'files' => true, 'method' => 'PUT')) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                                <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $banner->status) == "1" ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Index <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Lower index will be showed first, leave all blank if want to arranged automatically by name."><i class="fa fa-question-circle"></i></a></label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" name="sort" id="sort" min="0" value="{{ old('sort', $banner->sort) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Title</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Title" name="title" id="title" value="{{ old('title', $banner->title) }}">
                            </div>
                        </div>
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Image <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will use for top banner"><i class="fa fa-question-circle"></i></a>
                            </label>
                            <div class="col-sm-3">
                                <input type="file" name="image" id="image" class="form-control">
                                <div class="image-size-note"><span>Note:</span>Width size should be 1400 Pixel.</div>
                            </div>
                            <div class="col-sm-3">
                                @if(isset($banner->image) && $banner->image)
                                <a href="{{ asset('images/banner/'.$banner->type.'/'.$banner->id)}}/{{ $banner->image }}" target="_blank">{{ $banner->image }}</a>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" placeholder="Alt Text" name="image_alt" id="image_alt" value="{{ ($banner->image_alt) ? $banner->image_alt : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Type</label>
                            <div class="col-sm-9">
                                <select name="type" class="form-control">
                                    <option value="home">Home</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Content</label>
                            <div class="col-sm-9">
                                <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Content" name="description">{{ old('description', $banner->description) }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
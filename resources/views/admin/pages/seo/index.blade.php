@extends('admin.layouts.app')
@section('title', 'SEO')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mb-3 pull-right">
                            <a href="{{ route('seo.create') }}" class="btn btn-inverse btn-round">Add SEO</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>URL Matched</th>
                                        <th>Page Title</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php ($i = 1)
                                    @foreach ($seo as $seoSingle)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $seoSingle->url }}</td>
                                        <td>{{ $seoSingle->meta_title }}</td>
                                        <td class="column-operations">
                                            <a href="{{ route('seo.edit', $seoSingle->id) }}" class="btn btn-inverse btn-round">Edit</a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['seo.destroy', $seoSingle->id] ]) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-round']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>URL Matched</th>
                                        <th>Page Title</th>
                                        <th>Option</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
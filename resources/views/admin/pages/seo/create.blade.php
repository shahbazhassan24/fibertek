@extends('admin.layouts.app')
@section('title', 'Create Seo')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card create-product">
            <div class="card-block">
                {{ Form::open(array('route' => array('seo.store'))) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">URL Matched</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="URL Matched" name="url" id="url" value="{{ old('url') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Page Title</label>
                                <div class="col-sm-9">
                                    <textarea rows="5" cols="5" class="form-control" placeholder="Page Title" name="meta_title" id="meta_title">{{ old('meta_title') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Meta Name Description</label>
                                <div class="col-sm-9">
                                    <textarea rows="5" cols="5" class="form-control" placeholder="Meta Name Description" name="meta_description" id="meta_description">{{ old('meta_description') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Meta Name Keywords</label>
                                <div class="col-sm-9">
                                    <textarea rows="5" cols="5" class="form-control" placeholder="Meta Name Keywords" name="meta_keywords" id="meta_keywords">{{ old('meta_keywords') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
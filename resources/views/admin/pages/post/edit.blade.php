@extends('admin.layouts.app')
@section('title', 'Edit Post')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                {{ Form::model($post, array('route' => array('posts.update', $post->id), 'files' => true, 'method' => 'PUT')) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#details" role="tab">Post Details</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tags" role="tab">Tags</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="details" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Status</label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $post->status) == "1" ? 'checked' : '' }}/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Title" name="title" id="title" value="{{ old('title', $post->title) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Slug</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Slug" name="slug" id="slug" value="{{ old('slug', $post->slug) }}">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Image <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will use for all places"><i class="fa fa-question-circle"></i></a>
                                    </label>
                                    <div class="col-sm-3">
                                        <input type="file" name="image" id="image" class="form-control">
                                        <div class="image-size-note"><span>Note:</span>Width size should be 450 Pixel.</div>
                                    </div>
                                    <div class="col-sm-3">
                                        @if(isset($post->image) && $post->image)
                                        <a href="{{ asset('images/post/'.$post->id)}}/{{ $post->image }}" target="_blank">{{ $post->image }}</a>
                                        @endif
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="Alt Text" name="image_alt" id="image_alt" value="{{ ($post->image_alt) ? $post->image_alt : '' }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Description</label>
                                    <div class="col-sm-9">
                                        <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Description" name="description">{{ old('description', $post->description) }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tags" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-12">
                                        @foreach($tags as $tag)
                                            <ul class="categories-main">
                                                <li>
                                                    <div class="checkbox-fade fade-in-primary">
                                                        <label>
                                                            <input type="checkbox" id="checkbox{{$tag->id}}" value="{{$tag->id}}" name="tags[{{$tag->id}}]"
                                                            {{ (old('tags.'.$tag->id) == $tag->id) || (isset($post) && in_array($tag->id, $post->tags->pluck('id')->toArray())) ? 'checked' : '' }}
                                                            >
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                            </span>
                                                            <span>{{$tag->title}}</span>
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
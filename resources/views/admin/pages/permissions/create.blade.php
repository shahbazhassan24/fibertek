@extends('admin.layouts.app')
@section('title', 'Create Permission')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
            {{ Form::open(array('route' => array('permissions.store'))) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Role</label>
                            <div class="col-sm-9">
                            @if(!$roles->isEmpty())
                            @foreach ($roles as $role)
                            {{ Form::checkbox('roles[]',  $role->id ) }}
                            {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                            @endforeach
                            @endif
                            </div>
                        </div>
                        {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                    </div>
                </div>
            {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
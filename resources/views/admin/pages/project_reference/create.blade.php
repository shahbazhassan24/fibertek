@extends('admin.layouts.app')
@section('title', 'Create Project Reference')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block configurations">
                {{ Form::open(array('route' => array('project-reference.store'), 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <!-- Tab panes -->
                        <div class="card-block">
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Status <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Enable : it will show on website."><i class="fa fa-question-circle"></i></a></label>
                                <div class="col-sm-9">
                                    <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status') == "1" ? 'checked' : '' }} />
                                </div>
                            </div>
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Index <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Lower index will be showed first, leave all blank if want to arranged automatically by name."><i class="fa fa-question-circle"></i></a></label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" name="sort" id="sort" min="0" value="{{ old('sort', '0') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Title</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Title" name="title" id="title" value="{{ old('title') }}">
                                </div>
                            </div>
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Image</label>
                                <div class="col-sm-3">
                                    <input type="file" name="image" id="image" class="form-control">
                                        <div class="image-size-note"><span>Note:</span>Width size should be 370 Pixel.</div>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" placeholder="Alt Text" name="image_alt" id="image_alt" value="">
                                </div>
                                <div class="col-sm-3">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Description" name="description">{{ old('description') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Document</label>
                                <div class="col-sm-4">
                                    <input type="file" name="document" id="document" class="form-control">
                                </div>
                                <div class="col-sm-5">
                                    
                                </div>
                            </div>
                            <div class="form-group row project-refference-projects">
                                <label class="col-sm-3 col-form-label">Projects</label>
                                <div class="project-reference-wrapper col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <input type="text" name="projects[year][]" class="form-control" placeholder="Year">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="projects[name][]" class="form-control" placeholder="Project">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="projects[country][]" class="form-control" placeholder="Country">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="projects[product][]" class="form-control" placeholder="Product">
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="javascript:void(0)" class="remove-more-project pull-right"><i class="fa fa-minus"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <a href="javascript:void(0)" class="add-more-project pull-right"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
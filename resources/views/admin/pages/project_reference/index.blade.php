@extends('admin.layouts.app')
@section('title', 'Project References')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mb-3 pull-right">
                            <a href="{{ route('project-reference.create') }}" class="btn btn-inverse btn-round">Add Project Reference</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Active</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php ($i = 1)
                                    @foreach ($items as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->status ? 'yes' : 'no' }}</td>
                                        <td class="column-operations">
                                            <a href="{{ route('project-reference.edit', $item->id) }}" class="btn btn-inverse btn-round">Edit</a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['project-reference.destroy', $item->id] ]) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-round']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Active</th>
                                        <th>Option</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
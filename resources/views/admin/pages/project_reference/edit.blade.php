@extends('admin.layouts.app')
@section('title', 'Edit Project Reference')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block configurations">
                {{ Form::model($projectReference, array('route' => array('project-reference.update', $projectReference->id), 'method' => 'PUT', 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <!-- Tab panes -->
                        <div class="card-block">
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Status <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Enable : it will show on website."><i class="fa fa-question-circle"></i></a></label>
                                <div class="col-sm-9">
                                    <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $projectReference->status) == "1" ? 'checked' : '' }} />
                                </div>
                            </div>
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Index <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Lower index will be showed first, leave all blank if want to arranged automatically by name."><i class="fa fa-question-circle"></i></a></label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" name="sort" id="sort" min="0" value="{{ old('sort', $projectReference->sort) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Title</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Title" name="title" id="title" value="{{ old('title', $projectReference->title) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Slug</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="Slug" name="slug" id="slug" value="{{ old('slug', $projectReference->slug) }}">
                                </div>
                            </div>
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Image</label>
                                <div class="col-sm-3">
                                    <input type="file" name="image" id="image" class="form-control">
                                        <div class="image-size-note"><span>Note:</span>Width size should be 370 Pixel.</div>
                                </div>
                                <div class="col-sm-3">
                                    @if(isset($projectReference->image))
                                    <a href="{{ asset('images/project-reference/'.$projectReference->id)}}/{{ $projectReference->image }}" target="_blank">{{ $projectReference->image }}</a>
                                    @endif
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" placeholder="Alt Text" name="image_alt" id="image_alt" value="{{ ($projectReference->image_alt) ? $projectReference->image_alt : '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Description" name="description">{{ old('description', $projectReference->description) }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row tooltip-link">
                                <label class="col-sm-3 col-form-label">Document</label>
                                <div class="col-sm-4">
                                    <input type="file" name="document" id="document" class="form-control">
                                </div>
                                <div class="col-sm-5">
                                    @if(isset($projectReference->document))
                                    <a href="{{ asset('documents/project-reference/'.$projectReference->id)}}/{{ $projectReference->document }}" target="_blank">{{ $projectReference->document }}</a>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row project-refference-projects">
                                <label class="col-sm-3 col-form-label">Projects</label>
                                <div class="project-reference-wrapper col-sm-9">
                                    @if(isset($projectReference->projects))
                                    @php ($projects = json_decode($projectReference->projects, true))
                                    @for($projectCounter = 0; $projectCounter < $projectReference->projectsCount; $projectCounter++)
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <input type="text" name="projects[year][]" class="form-control" placeholder="Year" value="{{ $projects['year'][$projectCounter] }}">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="projects[name][]" class="form-control" placeholder="Project" value="{{ $projects['name'][$projectCounter] }}">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="projects[country][]" class="form-control" placeholder="Country" value="{{ $projects['country'][$projectCounter] }}">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="projects[product][]" class="form-control" placeholder="Product" value="{{ $projects['product'][$projectCounter] }}">
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="javascript:void(0)" class="remove-more-project pull-right"><i class="fa fa-minus"></i></a>
                                        </div>
                                    </div>
                                    @endfor
                                    @endif
                                </div>
                                <div class="col-sm-12">
                                    <a href="javascript:void(0)" class="add-more-project pull-right"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
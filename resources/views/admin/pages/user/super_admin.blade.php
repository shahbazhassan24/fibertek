@extends('admin.layouts.app')
@section('title', 'Users')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mb-3 pull-right">
                            <a href="{{ route('users.create') }}" class="btn btn-inverse btn-round">Add User</a>
                            <a href="{{ route('roles.index') }}" class="btn btn-inverse btn-round">Roles</a>
                            <a href="{{ route('permissions.index') }}" class="btn btn-inverse btn-round">Permissions</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Date/Time Added</th>
                                        <th>User Roles</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php ($i = 1)
                                    @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                                        <td>{{  $user->roles()->pluck('name')->implode(' ') }}</td>
                                        <td class="column-operations">
                                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-inverse btn-round">Edit</a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id] ]) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-round']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Date/Time Added</th>
                                        <th>User Roles</th>
                                        <th>Option</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
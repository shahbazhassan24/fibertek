@extends('admin.layouts.app')
@section('title', 'Edit User')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
            {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="{{ old('name', $user->name) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{ old('email', $user->email) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Company Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Company Name" name="company_name" id="company_name" value="{{ old('company_name', $user->company_name) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Country</label>
                            <div class="col-sm-9">
                                <select name="country_id" class="form-control @error('country_id') is-invalid @enderror" required autocomplete="country_id" autofocus>
                                    <option value="">Country</option>
                                    @foreach($countries as $country)
                                    <option value="{{ $country->id }}" {{ old('country_id', $user->country_id) ==  $country->id ? 'selected' : '' }}>{{ $country->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Role</label>
                            <div class="col-sm-9">
                            @foreach ($roles as $role)
                            {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
                            {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                            @endforeach
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" placeholder="Password" name="password" id="password" value="{{ old('password') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Confirm Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" id="password_confirmation" value="{{ old('password_confirmation') }}">
                            </div>
                        </div>
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Latest Alerts</label>
                            <div class="col-sm-9">
                                <input name="stay_alert" type="checkbox" value="1" {{ ($user->send_latest_alert) ? 'checked="checked"' : '' }}>
                                <label for="stay_alert">Stay alert on the latest developments of FiberTek</label>
                            </div>
                        </div>
                        {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                    </div>
                </div>
            {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
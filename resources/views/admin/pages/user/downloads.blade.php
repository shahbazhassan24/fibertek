@extends('admin.layouts.app')
@section('title', 'Downloads of "'.$user->name.'"')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>Sr #</th>
                                        <th>Document</th>
                                        <th>Type</th>
                                        <th>Download</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php ($i = 1)
                                @foreach ($downloads as $download)
                                    @if($download->type == 'catalogue')
                                        @php ($title = $download->catalogue->title)
                                        @php ($url = asset('/documents/catalogue/'.$download->catalogue->id.'/'.$download->catalogue->document))
                                    @elseif($download->type == 'certification')
                                        @php ($title = $download->certification->title)
                                        @php ($url = asset('/documents/certification/'.$download->certification->id.'/'.$download->certification->document))
                                    @elseif($download->type == 'manual')
                                        @php ($title = $download->manual->title)
                                        @php ($url = asset('/documents/manual/'.$download->manual->id.'/'.$download->manual->document))
                                    @elseif($download->type == 'project_reference')
                                        @php ($title = $download->project_reference->title)
                                        @php ($url = asset('/documents/project-reference/'.$download->project_reference->id.'/'.$download->project_reference->document))
                                    @elseif($download->type == 'product')
                                        @php ($title = $download->product->title)
                                        @php ($url = asset('/documents/product/'.$download->product->id.'/'.$download->product->documents[0]->name))
                                    @endif
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $title }}</td>
                                        <td>{{ ucfirst($download->type) }}</td>
                                        <td class="download-link"><a href="{{ $url }}" target="_blank">Download</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Sr #</th>
                                        <th>Document</th>
                                        <th>Type</th>
                                        <th>Download</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
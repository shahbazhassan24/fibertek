@extends('admin.layouts.app')
@section('title', 'Edit Category')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                @php ($editUrl = $category->id)
                @if(isset($_GET['sub']))
                @php ($editUrl .= '?sub='.$_GET['sub'])
                @endif
                {{ Form::model($category, array('route' => ['categories.update', $editUrl], 'method' => 'PUT', 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                         <!-- Nav tabs -->
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#category-details" role="tab">Category Details</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#description" role="tab">Description</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#images" role="tab">Images</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="category-details" role="tabpanel">
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Status <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Enable : it will show on website."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $category->status) == "1" ? 'checked' : '' }} />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Parent Category</label>
                                    <div class="col-sm-9">
                                        <select name="parent_id" class="form-control">
                                            <option value="">No Parent</option>
                                            @foreach ($allCategories as $singleCategory)
                                            <option value="{{ $singleCategory->id }}" {{ old('parent_id', $category->parent_id) == $singleCategory->id ? 'selected' : '' }}>{{ $singleCategory->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Index <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Lower index will be showed first, leave all blank if want to arranged automatically by name."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="sort" id="sort" min="0" value="{{ old('sort', $category->sort) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Category Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Category Title" name="title" id="title" value="{{ old('title', $category->title) }}">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Featured <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Enable : it will make this featured."><i class="fa fa-question-circle"></i></a></label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="js-switch" name="is_featured" id="is_featured" value="1" {{ old('is_featured', $category->is_featured) == "1" ? 'checked' : '' }}/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Video Link</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Demonstration Video Link" name="video_link" id="video_link" value="{{ old('video_link', $category->video_link) }}">
                                    </div>
                                </div>
                                <div class="form-group row tooltip-link">
                                    <label class="col-sm-3 col-form-label">Demonstration Image <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will use in place of video if no video link provided on Category detail page."><i class="fa fa-question-circle"></i></a>
                                    </label>
                                    <div class="col-sm-3">
                                        <input type="file" name="demonstration_image" id="demonstration_image" class="form-control">
                                <div class="image-size-note"><span>Note:</span>Width size should be 450 Pixel.</div>
                                    </div>
                                    <div class="col-sm-3">
                                        @if(isset($category->demonstration_image) && $category->demonstration_image)
                                        <a href="{{ asset('images/category/'.$category->id)}}/{{ $category->demonstration_image }}" target="_blank">{{ $category->demonstration_image }}</a>
                                        @endif
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="Alt Text" name="demonstration_image_alt" id="demonstration_image_alt" value="{{ (isset($category->images[0])) ? json_decode($category->images[0]->alt_text)->demonstration : '' }}">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="description" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group row tooltip-link">
                                            <label class="col-sm-3 col-form-label">Short Description <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will display on parent category page"><i class="fa fa-question-circle"></i></a></label>
                                            <div class="col-sm-9">
                                                <textarea id="editor-short-description" class="content-editor form-control" placeholder="Enter Short Description" name="short_description">{{ old('short_description', $category->short_description) }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row tooltip-link">
                                            <label class="col-sm-3 col-form-label">Description <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will display on Category Detail page"><i class="fa fa-question-circle"></i></a></label>
                                            <div class="col-sm-9">
                                                <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Description" name="description">{{ old('description', $category->description) }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row tooltip-link">
                                            <label class="col-sm-3 col-form-label">Banner Description <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will display on category banner"><i class="fa fa-question-circle"></i></a></label>
                                            <div class="col-sm-9">
                                                <textarea id="editor-banner-description" class="content-editor form-control" placeholder="Enter Banner Description" name="banner_description">{{ old('banner_description', $category->banner_description) }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="images" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group row tooltip-link">
                                            <label class="col-sm-3 col-form-label">Category Image <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will use for all places"><i class="fa fa-question-circle"></i></a>
                                            </label>
                                            <div class="col-sm-3">
                                                <input type="file" name="image" id="image" class="form-control">
                                <div class="image-size-note"><span>Note:</span>Width size should be 150 Pixel.</div>
                                            </div>
                                            <div class="col-sm-3">
                                                @if(isset($category->image) && $category->image)
                                                <a href="{{ asset('images/category/'.$category->id)}}/{{ $category->image }}" target="_blank">{{ $category->image }}</a>
                                                @endif
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" placeholder="Alt Text" name="image_alt" id="image_alt" value="{{ (isset($category->images[0])) ? json_decode($category->images[0]->alt_text)->image : '' }}">
                                            </div>
                                        </div>
                                        <div class="form-group row tooltip-link">
                                            <label class="col-sm-3 col-form-label">Category Banner <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will display on category page"><i class="fa fa-question-circle"></i></a>
                                            </label>
                                            <div class="col-sm-3">
                                                <input type="file" name="banner" id="banner" class="form-control">
                                <div class="image-size-note"><span>Note:</span>Width size should be 1920 Pixel.</div>
                                            </div>
                                            <div class="col-sm-3">
                                                @if(isset($category->banner) && $category->banner)
                                                <a href="{{ asset('images/category/'.$category->id)}}/{{ $category->banner }}" target="_blank">{{ $category->banner }}</a>
                                                @endif
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" placeholder="Alt Text" name="banner_alt" id="banner_alt" value="{{ (isset($category->images[0])) ? json_decode($category->images[0]->alt_text)->banner : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
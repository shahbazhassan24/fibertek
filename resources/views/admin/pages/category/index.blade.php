@extends('admin.layouts.app')
@section('title', 'Categories')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mb-3 pull-right">
                            @php ($addUrlArray = [])
                            @if(isset($_GET['sub']))
                            @php ($addUrlArray['sub'] = $_GET['sub'])
                            @endif
                            <a href="{{ route('categories.create', $addUrlArray) }}" class="btn btn-inverse btn-round">Add Category</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Active</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php ($i = 1)
                                    @foreach ($categories as $category)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $category->title }}</td>
                                        <td>{{ $category->status ? 'yes' : 'no' }}</td>
                                        <td class="column-operations">
                                            @if($category->products->isNotEmpty())
                                            <a href="{{ route('products.index', ['sub' => $category->id]) }}" class="btn btn-inverse btn-round">Product</a>
                                            @endif
                                            @if($category->subCategories->isNotEmpty())
                                            <a href="{{ route('categories.index', ['sub' => $category->id]) }}" class="btn btn-inverse btn-round">Sub Category</a>
                                            @endif
                                            @php ($editUrlArray = [$category->id])
                                            @if(isset($_GET['sub']))
                                            @php ($editUrlArray['sub'] = $_GET['sub'])
                                            @endif
                                            <a href="{{ route('categories.edit', $editUrlArray) }}" class="btn btn-inverse btn-round">Edit</a>
                                            @php ($deleteFormParam = $category->id)
                                            @if(isset($_GET['sub']))
                                            @php ($deleteFormParam .= '?sub='.$_GET['sub'])
                                            @endif
                                            {{ Form::model($category, array('route' => array('categories.destroy', $deleteFormParam), 'method' => 'DELETE')) }}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-round']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Active</th>
                                        <th>Option</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
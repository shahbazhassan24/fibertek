@extends('admin.layouts.app')
@section('title', $pageTitle)
@section('content')
<div class="page-body">
        <div class="row">
            <div class="col-md-12 col-xl-6">
                <!-- table card start -->
                <div class="card table-card">
                    <div class="">
                        <div class="row-table">
                            <div class="col-sm-6 card-block-big br">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-archive text-success"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $products }}</h5>
                                        <a href="{{ route('products.index') }}"><span>Products</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 card-block-big">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-archive text-danger"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $categories }}</h5>
                                        <a href="{{ route('categories.index') }}"><span>Categories</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="row-table">
                            <div class="col-sm-6 card-block-big br">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-files text-info"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $jobs }}</h5>
                                        <a href="{{ route('career.index') }}"><span>Jobs</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 card-block-big">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-files text-warning"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $posts }}</h5>
                                        <a href="{{ route('posts.index') }}"><span>Blogs</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- table card end -->
            </div>
            <div class="col-md-12 col-xl-6">
                <!-- table card start -->
                <div class="card table-card">
                    <div class="">
                        <div class="row-table">
                            <div class="col-sm-6 card-block-big br">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-eye-alt text-success"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $catalogues }}</h5>
                                        <a href="{{ route('catalogues.index') }}"><span>Catalogues</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 card-block-big">
                                <div class="row ">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-network text-primary"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $certifications }}</h5>
                                        <a href="{{ route('certifications.index') }}"><span>Certifications</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="row-table">
                            <div class="col-sm-6 card-block-big br">
                                <div class="row ">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-eye-alt text-success"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $manuals }}</h5>
                                        <a href="{{ route('manuals.index') }}"><span>Manuals</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 card-block-big">
                                <div class="row ">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-network-tower text-primary"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $faqs }}</h5>
                                        <a href="{{ route('faqs.index') }}"><span>FAQ</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- table card end -->
            </div>
            <div class="col-md-12 col-xl-6">
                <!-- table card start -->
                <div class="card table-card">
                    <div class="">
                        <div class="row-table">
                            <div class="col-sm-6 card-block-big br">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-eye-alt text-success"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $subscribers }}</h5>
                                        <a href="{{ route('subscriber.index') }}"><span>Subscribers</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 card-block-big">
                                <div class="row ">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-network text-primary"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $quotes }}</h5>
                                        <a href="{{ route('quotes.index') }}"><span>Quotes</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="row-table">
                            <div class="col-sm-6 card-block-big br">
                                <div class="row ">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-eye-alt text-success"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $projectReferences }}</h5>
                                        <a href="{{ route('project-reference.index') }}"><span>Project References</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 card-block-big">
                                <div class="row ">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-network-tower text-primary"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $pages }}</h5>
                                        <a href="{{ route('products.index') }}"><span>Pages</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- table card end -->
            </div>
            <div class="col-md-12 col-xl-6">
                <!-- table card start -->
                <div class="card table-card">
                    <div class="">
                        <div class="row-table">
                            <div class="col-sm-6 card-block-big br">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-eye-alt text-success"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $news }}</h5>
                                        <a href="{{ route('news.index') }}"><span>News</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 card-block-big">
                                <div class="row ">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-certificate text-primary"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $seo }}</h5>
                                        <a href="{{ route('seo.index') }}"><span>SEO</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="row-table">
                            <div class="col-sm-6 card-block-big br">
                                <div class="row ">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-envelope-open text-primary"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        <h5>{{ $tags }}</h5>
                                        <a href="{{ route('products.index') }}"><span>Tags</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 card-block-big">
                                <div class="row ">
                                    <div class="col-sm-4">
                                        <i class="icofont icofont-eye-alt text-success"></i>
                                    </div>
                                    <div class="col-sm-8 text-center">
                                        
                                        <a href="{{ route('configurations.edit') }}"><span>Configurations</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- table card end -->
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')
@section('title', 'Edit Role')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
            {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Assign Permissions</label>
                            <div class="col-sm-9">
                            @foreach ($permissions as $permission)
                            {{ Form::checkbox('permissions[]',  $permission->id, $role->permissions ) }}
                            {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>
                            @endforeach
                            </div>
                        </div>
                        {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                    </div>
                </div>
            {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
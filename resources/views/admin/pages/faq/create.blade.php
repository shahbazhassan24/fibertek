@extends('admin.layouts.app')
@section('title', 'Create FAQ')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                {{ Form::open(array('route' => array('faqs.store'), 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                                <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status') == "1" ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Index <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="Lower index will be showed first, leave all blank if want to arranged automatically by name."><i class="fa fa-question-circle"></i></a></label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" name="sort" id="sort" min="0" value="{{ old('sort', '0') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Question</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Question" name="question" id="question" value="{{ old('question') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Answer</label>
                            <div class="col-sm-9">
                                <textarea id="editor-answer" class="content-editor form-control" placeholder="Enter Answer" name="answer">{{ old('answer') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
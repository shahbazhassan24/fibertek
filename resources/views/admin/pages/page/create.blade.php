@extends('admin.layouts.app')
@section('title', 'Create Page')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                {{ Form::open(array('route' => array('pages.store'), 'files' => true)) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                                <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status') == "1" ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Title</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Title" name="title" id="title" value="{{ old('title') }}">
                            </div>
                        </div>
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Banner <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will use for top banner"><i class="fa fa-question-circle"></i></a>
                            </label>
                            <div class="col-sm-3">
                                <input type="file" name="banner" id="banner" class="form-control">
                                        <div class="image-size-note"><span>Note:</span>Width size should be 1920 Pixel.</div>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" placeholder="Alt Text" name="banner_alt" id="banner_alt" value="">
                            </div>
                            <div class="col-sm-3">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Content</label>
                            <div class="col-sm-9">
                                <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Content" name="content">{{ old('content') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
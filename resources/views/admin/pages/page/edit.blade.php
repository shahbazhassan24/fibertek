@extends('admin.layouts.app')
@section('title', 'Edit Page')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                {{ Form::model($page, array('route' => array('pages.update', $page->id), 'files' => true, 'method' => 'PUT')) }}
                <div class="row m-b-30">
                    <div class="col-sm-12">
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                                <input type="checkbox" class="js-single" name="status" id="status" value="1" {{ old('status', $page->status) == "1" ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Title</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Title" name="title" id="title" value="{{ old('title', $page->title) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Slug</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Slug" name="slug" id="slug" value="{{ old('slug', $page->slug) }}">
                            </div>
                        </div>
                        <div class="form-group row tooltip-link">
                            <label class="col-sm-3 col-form-label">Banner <a href="#!" data-placement="right" data-trigger="hover" data-toggle="tooltip" title="" data-original-title="This will use for top banner"><i class="fa fa-question-circle"></i></a>
                            </label>
                            <div class="col-sm-3">
                                <input type="file" name="banner" id="banner" class="form-control">
                                        <div class="image-size-note"><span>Note:</span>Width size should be 1920 Pixel.</div>
                            </div>
                            <div class="col-sm-3">
                                @if(isset($page->banner) && $page->banner)
                                <a href="{{ asset('images/page/'.$page->id)}}/{{ $page->banner }}" target="_blank">{{ $page->banner }}</a>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" placeholder="Alt Text" name="banner_alt" id="banner_alt" value="{{ ($page->banner_alt) ? $page->banner_alt : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Content</label>
                            <div class="col-sm-9">
                                <textarea id="editor-description" class="content-editor form-control" placeholder="Enter Content" name="content">{{ old('content', $page->content) }}</textarea>
                            </div>
                        </div>
                        @if($page->id == '1')
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="card-header">
                                            <div class="card-header-left">
                                                <h5>About Us Sections</h5>
                                            </div>
                                        </div>
                                        @php ($pageExtras = json_decode($page->extras, true))
                                        <div class="card-block">
                                            @for($extraCounter = '0'; $extraCounter <= '4'; $extraCounter++)
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Block {{ $extraCounter + 1 }} Image</label>
                                                <div class="col-sm-4">
                                                    <input type="file" name="extras[image][{{$extraCounter}}]" class="form-control">
                                                    <div class="image-size-note"><span>Note:</span>Width size should be 600 Pixel.</div>
                                                </div>
                                                <div class="col-sm-5">
                                                @if(isset($pageExtras[$extraCounter]['image']) && $pageExtras[$extraCounter]['image'])
                                                    <a href="{{ asset('images/page/'.$page->id.'/extra/')}}/{{ $pageExtras[$extraCounter]['image'] }}" target="_blank">{{ $pageExtras[$extraCounter]['image'] }}</a>
                                                @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Block {{ $extraCounter + 1 }} Content</label>
                                                <div class="col-sm-9">
                                                    <textarea id="editor-extra-description{{ $extraCounter + 1 }}" class="content-editor form-control" placeholder="Enter Content" name="extras[description][{{$extraCounter}}]">{{ old('extras.description.'.$extraCounter, $pageExtras[$extraCounter]['description']) }}</textarea>
                                                </div>
                                            </div>
                                            @endfor
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                {{ Form::submit('Submit', array('class' => 'btn btn-primary pull-right')) }}
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('frontend.includes.head')
</head>
<body>
    <div id="wrapper">
        @yield('content')
    </div>
    @include('frontend.includes.scripts')
    @stack('scripts')
</body>
</html>
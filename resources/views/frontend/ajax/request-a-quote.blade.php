<div class="shopping-cart">
    <ul class="shopping-cart-items">
        @if($quoteProducts)
            @foreach($quoteProducts as $quoteProduct)
            <li>
                <div class="cart-img">
                    <a href="{{ route('product', $quoteProduct['slug']) }}">
                        <img src="{{ asset('/images/product/'.$quoteProduct['id'].'/'.$quoteProduct['image']) }}" alt="{{ ($quoteProduct['image_alt']) ? $quoteProduct['image_alt'] : Helper::getImageAlt($quoteProduct['image']) }}">
                    </a>
                </div>
                <div class="cart-title">
                    <span class="item-name"><a href="{{ route('product', $quoteProduct['slug']) }}">{{ $quoteProduct['title'] }}</a></span>
                </div>
                <div class="cart-remove">
                    <button type="button" class="item-delete remove-request-a-quote" data-product-id="{{ $quoteProduct['id'] }}">&#10005;</button>
                </div>
            </li>
            @endforeach
        @endif
    </ul>
    <div class="cart_btn_wrap">
        @if($quoteProducts)
        <a href="./page/enquiry-cart" class="cart-btn">Send Enquiry</a>
        @else
        <a href="#" class="cart-btn btn-no-item-request-a-quote">No Request Quote</a>
        @endif
    </div>
</div> <!--end shopping-cart -->
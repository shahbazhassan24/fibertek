<table border="0" cellpadding="0" width="100%">
	<tbody>
		<tr>
			<td>
				<div class="logo">
					<a href="{{ route('home') }}" class="logo"><img src="{{ asset('/images/').'/'.$configuration['web_logo'] }}" alt="{{ Helper::getImageAlt($configuration['web_logo'], true) }}"></a>
				</div>
			</td>
			<td>
				<div class="site-tagline"><span>{{ $configuration['tagline'] }}</span></div>
			</td>
		</tr>
	</tbody>
</table>
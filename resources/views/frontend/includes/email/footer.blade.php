<div class="footer">
	<div class="contact-information">
		<ul>
			<li>Tel: {!! $configuration['contact_phone'] !!}</li>
			<li>Email: {!! $configuration['contact_email'] !!}</li>
			<li>Website: www.fibertek-ap.com</li>
		</ul>
	</div>
	<div class="copyright">
		Copyright © {{ date('Y') }} FiberTek.
	</div>
</div>
<div class="breadcrumbs add">
    <ul class="container list_none">
        <li><a href="{{ route('home') }}">Home</a></li>
        @foreach($breadcrumbLinks as $breadcrumbLink)
        <li>
        	@if(isset($breadcrumbLink['url']))
        	<a href="{{ $breadcrumbLink['url'] }}">{{ $breadcrumbLink['title'] }}</a>
        	@else
        	{{ $breadcrumbLink['title'] }}
        	@endif
    	</li>
        @endforeach
    </ul>
</div>
<div class="banner" style="background:#000 url({{ (isset($banner) && $banner) ? $banner : '' }}) no-repeat;">
@if((isset($banner) && $banner) && (isset($banner_alt) && $banner_alt))
    <img src="{{ str_replace('/images/', '/images/background/', $banner) }}" alt="{{ $banner_alt }}" class="hidden" />
@endif
    <div class="d_table">
        <div class="v_middle">
            <div class="container">
                <div class="banner_caption {{ isset($category_banner) ? 'light' : '' }} ">
                    @if(isset($title) && $title)
                    <h1 class="{{ !isset($category_banner) ? 'text-center' : '' }}">{{ $title }}</h1>
                    @endif
                    @if(isset($banner_description) && $banner_description)
                    <p>{!! $banner_description !!}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
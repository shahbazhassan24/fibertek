<script type="text/javascript">
    var appJSUrl = '{{ route('home') }}';
    var appAssetsUrl = '{{ asset('/') }}';
</script>
<script type="text/javascript" src="{{ asset('frontend/js/atc.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/jquery-3.2.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/slider.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/masonary.js') }}"></script>
<footer id="footer">
    <div class="container cols">
        @if(Helper::getConfigurationByKey('footer_links_1'))
        <div class="col">
            {!! Helper::getConfigurationByKey('footer_links_1') !!}
        </div>
        @endif
        @if(Helper::getConfigurationByKey('footer_links_2'))
        <div class="col">
            {!! Helper::getConfigurationByKey('footer_links_2') !!}
        </div>
        @endif
        @if(Helper::getConfigurationByKey('footer_links_3'))
        <div class="col">
            {!! Helper::getConfigurationByKey('footer_links_3') !!}
        </div>
        @endif
        @if(Helper::getConfigurationByKey('footer_links_4'))
        <div class="col">
            {!! Helper::getConfigurationByKey('footer_links_4') !!}
        </div>
        @endif
    </div>
    <div class="rights">
        {!! $configuration['copyright'] !!}
    </div>
</footer>
<a href="#" id="back-to-top-scroll"><span></span></a>
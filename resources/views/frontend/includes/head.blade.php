<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>
	@if(isset($seoInformation) && $seoInformation)
		{{ $seoInformation->meta_title }}
	@else
		@if(isset($metaTitle))
			{{ Helper::getConfigurationByKey('meta_name_keywords') | Helper::getConfigurationByKey('web_title') }}
		@else
			{{ 'FiberTek | '.Helper::getConfigurationByKey('web_title') }}
		@endif
	@endif
</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="allow-search" content="Yes">
<meta name="ROBOTS" content = "All">
@if(isset($seoInformation) && $seoInformation)
<meta name="keywords" content="{{ $seoInformation->meta_keywords }}" />
<meta name="description" content="{{ $seoInformation->meta_description }}" />
@else
<meta name="keywords" content="{{ Helper::getConfigurationByKey('meta_name_keywords') }}" />
<meta name="description" content="{{ Helper::getConfigurationByKey('meta_name_description') }}" />
@endif
<!-- Favicon icon -->
<link rel="icon" href="{{ asset('/images/').'/'.Helper::getConfigurationByKey('web_ico') }}" type="image/x-icon">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<base href="{{ url('/').'/' }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/addtocalendar.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/font-awesome.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/slick.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/slick-theme.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/all.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/responsive.css') }}">
{!! Helper::getConfigurationByKey('custom_script') !!}
<meta name="google-translate-customization" content="e4f8ddcdeb86d-900e4f5239d48169-gbc9772411070dec6-1c"></meta>
{!! ReCaptcha::htmlScriptTagJsApi() !!}
<header id="header">
    <div class="top_header">
        <div class="container">
            <div class="left">
                <p>{!! $configuration['tagline'] !!}</p>
                <a href="{!! $configuration['online_shop_link'] !!}" class="btn_cart mobile_view" target="_blank"><img src="{{ asset('frontend/images/icon_5.png') }}" alt="FiberTek Online Shop"> Our Online Shop</a>
            </div>
            <div class="right">
                <span>
                    <img src="{{ asset('frontend/images/icon_1.png') }}" alt="FiberTek Contact Number">
                    <a href="tel:{!! str_replace(' ', '', $configuration['contact_phone']) !!}" target="_blank">{!! $configuration['contact_phone'] !!}</a>
                </span>
                <span class="ewrap">
                    <img src="{{ asset('frontend/images/icon_2.png') }}" alt="FiberTek Email Address">
                    <a href="mailto:{!! $configuration['contact_email'] !!}" target="_blank">{!! $configuration['contact_email'] !!}</a>
                </span>
                <span class="langwrap">
                    <div id="google_translate_element"></div>
                    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                </span>
                <span class="desktop_view">
                    <img src="{{ asset('frontend/images/icon_4.png') }}" alt="FiberTek Sign In">
                    @if(!Auth::check())
                    <a href="{{ route('login') }}">Sign in</a>
                    @else
                    <a href="{{ route('user-dashboard') }}">My Account</a>
                    |
                    <a href="{{ route('logout') }}">Logout</a>
                    @endif
                </span>
                <span class="cart_wrap">
                    @php($requestQuoteProducts = request()->session()->get('request_quote_products'))
                    <a id="cart" href="#" class="btncart"><img src="{{ asset('frontend/images/cart-enq-icon.png') }}" alt="FiberTek Online Shop"><span class="cart-count">{{ ($requestQuoteProducts) ? count($requestQuoteProducts) : '0' }}</span></a>
                    <div class="shopping-cart">
                        <ul class="shopping-cart-items">
                            @if($requestQuoteProducts)
                                @foreach($requestQuoteProducts as $quoteProduct)
                                <li>
                                    <div class="cart-img">
                                        <a href="{{ route('product', $quoteProduct['slug']) }}">
                                            <img src="{{ asset('/images/product/'.$quoteProduct['id'].'/'.$quoteProduct['image']) }}" alt="{{ ($quoteProduct['image_alt']) ? $quoteProduct['image_alt'] : Helper::getImageAlt($quoteProduct['image']) }}">
                                        </a>
                                    </div>
                                    <div class="cart-title">
                                        <span class="item-name"><a href="{{ route('product', $quoteProduct['slug']) }}">{{ $quoteProduct['title'] }}</a></span>
                                    </div>
                                    <div class="cart-remove">
                                        <button type="button" class="item-delete remove-request-a-quote" data-product-id="{{ $quoteProduct['id'] }}">&#10005;</button>
                                    </div>
                                </li>
                                @endforeach
                            @endif
                        </ul>
                        <div class="cart_btn_wrap">
                            @if($requestQuoteProducts)
                            <a href="./page/enquiry-cart" class="cart-btn">Send Enquiry</a>
                            @else
                            <a href="#" class="cart-btn btn-no-item-request-a-quote">No Request Quote</a>
                            @endif
                        </div>
                    </div> <!--end shopping-cart -->
                </span>
            </div>
        </div>
    </div>
    <div class="nav_holder">
        <div class="container">
            <a href="{{ route('home') }}" class="logo"><img src="{{ asset('/images/').'/'.$configuration['web_logo'] }}" alt="{{ $configuration['web_logo_alt'] }}"></a>
            <a href="#" class="nav_opener"><i class="fa fa-bars" aria-hidden="true"></i><span>Menu</span></a>
            <nav id="nav">
                <ul class="list_none">
                    <li>
                        @php ($productMenuItem = Helper::getMenuItem('1', 'title'))
                        <a href="#">{{ $productMenuItem->title }}</a>
                        <div class="four_cols fluid">
                            <div class="holder">
                                <div class="left_side tabs">
                                    @if($mainCategories->isNotEmpty())
                                    <ul class="list_none">
                                        @foreach($mainCategories as $mainKey =>  $mainCategory)
                                        @if($mainCategory->status)
                                        <li class="{{ (!$mainKey) ? 'active' : '' }}"><a href="{{ route('category', $mainCategory->slug) }}" data-tab-link="#{{ $mainCategory->slug }}">{{ $mainCategory->title }}</a></li>
                                        @endif
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                                <div class="menu_holder">
                                    <a href="#" class="back_tray"><i class="fa fa-angle-left" aria-hidden="true"></i> Back</a>
                                    <div class="tab-content">
                                        @if($mainCategories->isNotEmpty())
                                        @foreach($mainCategories as $mainKey => $mainCategory)
                                        @if($mainCategory->status)
                                        <div id="{{ $mainCategory->slug }}" class="tab {{ (!$mainKey) ? 'active' : '' }}">
                                            <div class="four_cols three">
                                                <div class="holder">
                                                    @if(count($mainCategory->subCategories))
                                                    @foreach($mainCategory->subCategories as $subKey => $subCategory)
                                                    @if($subCategory->status)
                                                    <div class="col">
                                                        <div class="image-holder">
                                                            <div class="col-icon">
                                                                <img src="{{ asset('/images/category/'.$subCategory->id.'/'.$subCategory->image) }}" alt="{{ (isset($subCategory->images[0])) ? json_decode($subCategory->images[0]->alt_text)->image : '' }}" class="img">
                                                            </div>
                                                        </div>
                                                        <div class="col-information">
                                                            <div class="txt">
                                                                <strong><a href="{{ route('category', $subCategory->slug) }}">{{ $subCategory->title }}</a></strong>
                                                                @if(count($subCategory->subCategories))
                                                                @foreach($subCategory->subCategories->take(5) as $subSubKey => $subSubCategory)
                                                                @if($subSubCategory->status)
                                                                <span><a href="{{ route('category', $subSubCategory->slug) }}">{{ $subSubCategory->title }}</a></span>
                                                                @endif
                                                                @endforeach
                                                                @if(count($subCategory->subCategories) > 5)
                                                                <!-- <span class="view-more-categories"><a href="{{ route('category', $subCategory->slug) }}">View More >></a></span> -->
                                                                @endif
                                                                @elseif(count($subCategory->products))
                                                                @foreach($subCategory->products->take(5) as $productKey => $product)
                                                                @if($product->status)
                                                                <span><a href="{{ route('product', $product->slug) }}">{{ $product->title }}</a></span>
                                                                @endif
                                                                @endforeach
                                                                @if(count($subCategory->products) > 5)
                                                                <!-- <span class="view-more-categories"><a href="{{ route('category', $subCategory->slug) }}">View More >></a></span> -->
                                                                @endif
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @php ($parentMenuItems = Helper::getParentMenu())
                    @if($parentMenuItems->isNotEmpty())
                    @foreach($parentMenuItems as $parentMenuItem)
                    <li>
                        <a href="{{ $parentMenuItem->url }}" @if($parentMenuItem->new_tab) target="_blank" @endif>{{ $parentMenuItem->title }}</a>
                        @if($parentMenuItem->subMenus->isNotEmpty())
                        <div class="four_cols three vertical-menu-item">
                            <div class="holder">
                                @foreach($parentMenuItem->subMenus as $subMenuItem)
                                @if(($subMenuItem['status'] == '1'))
                                <div class="col">
                                    <a href="{{ $subMenuItem->url }}" class="ico" @if($subMenuItem->new_tab) target="_blank" @endif>
                                       <div class="col-icon" style="background-image: url({{ asset('/images/menu/'.$subMenuItem->id.'/'.$subMenuItem->image) }})"></div>
                                        @if($subMenuItem->image_alt)
                                            <img src="{{ str_replace('/images/', '/images/background/', asset('/images/menu/'.$subMenuItem->id.'/'.$subMenuItem->image)) }}" alt="{{ $subMenuItem->image_alt }}" class="hidden" />
                                        @endif 
                                    </a>
                                    <div class="col-information">
                                        <strong><a href="{{ $subMenuItem->url }}" @if($subMenuItem->new_tab) target="_blank" @endif>{{ $subMenuItem->title }}</a></strong>
                                        <p>{!! $subMenuItem->short_description !!}</p>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        @endif
                    </li>
                    @endforeach
                    @endif
                </ul>
            </nav>
            <div class="align_right">
                <form class="search_form" action="{{ route('search') }}">
                    <input type="search" placeholder="Search on FiberTek..." value="@if(isset($_REQUEST['search'])){{ $_REQUEST['search'] }}@endif" name="search">
                    <input type="submit" value="Search">
                </form>
                <span class="mobile_view imgwrap">
                    <div class="userlogin-mobile" role="navigation">
                        <ul>
                            <li><img src="{{ asset('frontend/images/icon_4.png') }}" alt="FiberTek Sign In">
                                <ul class="dropdown">
                                    @if(!Auth::check())
                                    <li><a href="{{ route('login') }}">Sign in</a></li>
                                    @else
                                    <li><a href="{{ route('user-dashboard') }}">{{ Auth::user()->name }}</a></li>					
                                    <li><a href="{{ route('logout') }}">Logout</a></li>
                                    @endif
                                </ul>
                            </li>
                        </ul>
                    </div>

                </span>
                <a href="{!! $configuration['online_shop_link'] !!}" class="btn_cart desktop_view" target="_blank"><img src="{{ asset('frontend/images/icon_5.png') }}" alt="FiberTek Online Shop"> Our Online Shop</a>
            </div>
        </div>
    </div>
</header>
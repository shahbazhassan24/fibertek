@extends('frontend.layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Dear Admin,</strong>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <strong class="section-title"><span>Details of Product Quote</span></strong>
            <div class="information">
                <div class="row">
                    <ul class="box">
                        <li><span>Name:</span><span>{{ $emailData['data']['name'] }}</span></li>
                        <li><span>Company Name:</span><span>{{ $emailData['data']['company'] }}</span></li>
                        <li><span>Email:</span><span>{{ $emailData['data']['email'] }}</span></li>
                        <li><span>Contact Number:</span><span>{{ $emailData['data']['phone'] }}</span></li>
                        <li><span>Enquiry:</span><span>{{ $emailData['data']['message'] }}</span></li>
                    </ul>
                </div>
                @if($emailData['data']['productsForQuote'])
                <div class="row">
                    <div class="shopping-cart">
                        <ul class="shopping-cart-items">
                            @foreach($emailData['data']['productsForQuote'] as $quoteProduct)
                            <li>
                                <div class="cart-img">
                                    <a href="{{ route('product', $quoteProduct['slug']) }}">
                                        <img src="{{ asset('/images/product/'.$quoteProduct['id'].'/'.$quoteProduct['image']) }}" alt="{{ Helper::getImageAlt($quoteProduct['image']) }}">
                                    </a>
                                </div>
                                <div class="cart-title">
                                    <span class="item-name"><a href="{{ route('product', $quoteProduct['slug']) }}">{{ $quoteProduct['title'] }}</a></span>
                                </div>
                                <div class="cart-quantity">({{ $quoteProduct['quantity'] }})</div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif  
            </div>
        </td>
    </tr>
</table>
@endsection
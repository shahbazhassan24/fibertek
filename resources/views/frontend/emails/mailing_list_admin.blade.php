@extends('frontend.layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Dear Admin,</strong>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <strong class="section-title"><span>Details of Subscriber</span></strong>
            <div class="information">
                <div class="row">
                    <ul class="box">
                        <li><span>Company Name:</span><span>{{ $emailData['data']['company'] }}</span></li>
                        <li><span>Name:</span><span>{{ $emailData['data']['name'] }}</span></li>
                        <li><span>Email:</span><span>{{ $emailData['data']['email'] }}</span></li>
                        <li><span>Country:</span><span>{{ Helper::getCountryByID($emailData['data']['country_id'])->title }}</span></li>
                    </ul>
                </div>   
            </div>
        </td>
    </tr>
</table>
@endsection
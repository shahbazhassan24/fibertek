@extends('frontend.layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Dear {{ $emailData['data']['name'] }},</strong>
                <div class="information">
                    <div class="row">
                        <p>Thank you for registering at FiberTek Pte Ltd. You can login using your details.</p>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <strong class="section-title"><span>Details</span></strong>
            <div class="information">
                <div class="row">
                    <ul class="box">
                        <li><span>Username:</span><span>{{ $emailData['data']['email'] }}</span></li>
                        <li><span>Password:</span><span>You entered while registration.</span></li>
                        <li><span>Company Name:</span><span>{{ $emailData['data']['company_name'] }}</span></li>
                        <li><span>Country:</span><span>{{ $emailData['data']['country'] }}</span></li>
                    </ul>
                </div>   
            </div>
        </td>
    </tr>
</table>
@endsection
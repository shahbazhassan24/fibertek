@extends('frontend.layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Dear {{ $emailData['data']['name'] }},</strong>
                <div class="row">
                    <p>Thank you for your interest in our products. We have received your request for a quotation.</p>
                    <p>We will respond to your request within 3 business days.</p>
                </div>
                <div class="row">
                    <p>If you should have any questions, please contact us at +65 6280 1966. Thank you!</p>
                </div>
                @if($emailData['data']['productsForQuote'])
                <div class="row">
                    <div class="shopping-cart">
                        <ul class="shopping-cart-items">
                            @foreach($emailData['data']['productsForQuote'] as $quoteProduct)
                            <li>
                                <div class="cart-img">
                                    <a href="{{ route('product', $quoteProduct['slug']) }}">
                                        <img src="{{ asset('/images/product/'.$quoteProduct['id'].'/'.$quoteProduct['image']) }}" alt="{{ Helper::getImageAlt($quoteProduct['image']) }}">
                                    </a>
                                </div>
                                <div class="cart-title">
                                    <span class="item-name"><a href="{{ route('product', $quoteProduct['slug']) }}">{{ $quoteProduct['title'] }}</a></span>
                                </div>
                                <div class="cart-quantity">({{ $quoteProduct['quantity'] }})</div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
            </div>
        </td>
    </tr>
</table>
@endsection
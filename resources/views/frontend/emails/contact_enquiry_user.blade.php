@extends('frontend.layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Dear {{ $emailData['data']['name'] }},</strong>
                <div class="row">
                    <p>Thank You For Contacting FiberTek. We have received your message and will revert within three business days.</p>
                </div>
            </div>
        </td>
    </tr>
</table>
@endsection
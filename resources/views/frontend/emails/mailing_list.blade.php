@extends('frontend.layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Dear {{ $emailData['data']['name'] }},</strong>
                <div class="row">
                    <p>Thank you for your subscription to our mailing list.</p>
                    <p>You will be updated on the latest happenings of our company such as new product launches, events and many more. Stay tuned!</p>
                </div>
            </div>
        </td>
    </tr>
</table>
@endsection
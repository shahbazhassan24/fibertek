@extends('frontend.layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Dear Admin,</strong>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <strong class="section-title"><span>Details of Enquiry</span></strong>
            <div class="information">
                <div class="row">
                    <ul class="box">
                        <li><span>Name:</span><span>{{ $emailData['data']['name'] }}</span></li>
                        <li><span>Company Name:</span><span>{{ $emailData['data']['company'] }}</span></li>
                        <li><span>Email:</span><span>{{ $emailData['data']['email'] }}</span></li>
                        <li><span>Contact Number:</span><span>{{ $emailData['data']['phone'] }}</span></li>
                        <li><span>Feedback/Enquiry:</span><span>{{ $emailData['data']['enquiry'] }}</span></li>
                        <li><span>Hear about us:</span><span>{{ $emailData['data']['hear'] }}</span></li>
                    </ul>
                </div>   
            </div>
        </td>
    </tr>
</table>
@endsection
@extends('frontend.layouts.email')
@section('content')
<table border="0" width="100%">
    <tr>
        <td align="left" valign="top">
            <div class="information">
                <strong class="inner-heading">Hello,</strong>
                <div class="information">
                    <div class="row">
                        <p>You are receiving this email because we received a password reset request for your account.</p>
                    </div>
                    <div class="row">
                        <div class="button-center">
                            <a href="{{ route('password.reset', $emailData['data']['token']) }}" class="btn btn_primary">Reset Password</a>
                        </div>
                    </div>
                    <div class="row">
                        <!-- <p>This password reset link will expire in 6000 minutes.</p> -->
                        <p>This password reset link will expire in 60 minutes.</p>
                    </div>
                    <div class="row">
                        <p>If you did not request a password reset, no further action is required.</p>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
@endsection
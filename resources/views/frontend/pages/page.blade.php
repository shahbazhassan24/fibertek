@extends('frontend.layouts.app')
@section('content')
<div class="container page {{ request()->segment(count(request()->segments())) }}">
    <div class="text">
        <h1>{{ $page->title }}</h1>
        {!! $page->content !!}
    </div>
</div>
@endsection
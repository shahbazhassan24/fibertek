@extends('frontend.layouts.app')
@section('content')
<div class="events_area">
    <div class="events_holder">
        <header class="sec_header text-center">
            {!! $page->content !!}
        </header>
        <?php if($projectReferences->isNotEmpty()){ ?>
        <ul class="projects list_none">
            <?php foreach ($projectReferences as $projectReference) { ?>
            <li>
                <a href="{{ route('project-reference-detail', $projectReference->slug) }}">
                    <img src="{{ asset('/images/project-reference/'.$projectReference->id.'/'.$projectReference->image) }}" alt="{{ $projectReference->image_alt }}" class="img-responsive">
                    <strong>{{ $projectReference->title }}</strong>
                </a>
            </li>
            <?php } ?>
        <?php } ?>
        </ul>
        <!-- <div class="btn_div">
            <a href="#" class="load_btn">LOAD MORE</a>
        </div> -->
    </div>
</div>
@endsection
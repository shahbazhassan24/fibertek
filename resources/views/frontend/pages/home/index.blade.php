@extends('frontend.layouts.app')
@section('content')
@if(isset($homeBanners) && $homeBanners->isNotEmpty())
<div class="gallery home-banner">
    <div class="mask">
        <div class="slideset">
        @foreach($homeBanners as $homeBanner)
            <div class="slide">
                <div class="banner home" style="background:#000 url({{ asset('/images/banner/'.$homeBanner->type.'/'.$homeBanner->id.'/'.$homeBanner->image) }}) no-repeat;">
                @if($homeBanner->image_alt)
                    <img src="{{ str_replace('/images/', '/images/background/', asset('/images/banner/'.$homeBanner->type.'/'.$homeBanner->id.'/'.$homeBanner->image)) }}" alt="{{ $homeBanner->image_alt }}" class="hidden" />
                @endif    
                    <div class="d_table">
                        <div class="v_middle">
                            <div class="container">
                                @if($homeBanner->description)
                                <div class="banner_caption">
                                    {!! $homeBanner->description !!}
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @if($homeBanners->count() > '1')
        <a href="#" class="btn-prev"><img src="{{ asset('frontend/images/slider_btn_left.png') }}" alt="<"></a>
        <a href="#" class="btn-next"><img src="{{ asset('frontend/images/slider_btn.png') }}" alt=">"></a>
        <div class="pagination"></div>
        @endif
    </div>
</div>
@endif
<div class="products">
    <div class="container explore">
        <h2 class="sec_heading">Explore our Products</h2>
        <div class="gallery add">
            <div class="mask">
                @if(!Helper::isMobile())
                <div class="slideset">
                    @if($homeCategories->isNotEmpty())
                        @foreach($homeCategories as $loopCategory)
                    <div class="slide">
                        @foreach($loopCategory as $homeCategory)
                        <div class="slide_holder">
                            <a href="{{ $homeCategory->url }}" class="img_holder">
                                <div class="inner_img">
                                    <img src="{{ asset('/images/home/explore-product/'.$homeCategory->id.'/'.$homeCategory->image) }}" alt="{{ $homeCategory->image_alt }}">
                                </div>
                                <span>{{ $homeCategory->title }}</span>
                            </a>
                        </div>
                        @endforeach
                    </div>
                        @endforeach
                    @endif
                </div>
                @endif
                @if(Helper::isMobile())
                <div class="slideset">
                    @if($homeCategories->isNotEmpty())
                        @foreach($homeCategories as $loopCategory)
                            @foreach($loopCategory as $homeCategory)
                    <div class="slide">
                        <div class="slide_holder">
                            <a href="{{$homeCategory->url}}" class="img_holder">
                                <div class="inner_img">
                                    <img src="{{ asset('/images/home/explore-product/'.$homeCategory->id.'/'.$homeCategory->image) }}" alt="{{ $homeCategory->image_alt }}">
                                </div>
                                <span>{{ $homeCategory->title }}</span>
                            </a>
                        </div>
                    </div>
                            @endforeach
                        @endforeach
                    @endif
                </div>
                @endif
                <a href="#" class="btn-prev"><img src="{{ asset('frontend/images/slider_btn_left.png') }}" alt="<"></a>
                <a href="#" class="btn-next"><img src="{{ asset('frontend/images/slider_btn.png') }}" alt=">"></a>
                <!-- <div class="pagination"></div> -->
            </div>
        </div>
    </div>
    @if($featuredProducts->isNotEmpty())
    <div class="featured_products">
        <div class="container">
            <h2 class="sec_heading">Featured Products</h2>
            <div class="gallery">
                <div class="mask">
                    <div class="slideset">
                        @foreach($featuredProducts as $featuredProduct)
                        <div class="slide">
                            <div class="featured_product">
                                @if($featuredProduct->images->isNotEmpty())
                                <div class="img-holder-wrapper">
                                    <img src="{{ asset('/images/product/'.$featuredProduct->id.'/'.$featuredProduct->images[0]->name) }}" alt="{{ ($featuredProduct->images[0]->alt_text) ? $featuredProduct->images[0]->alt_text : Helper::getImageAlt($featuredProduct->images[0]->name) }}">
                                </div>
                                @endif
                                <div class="slide_txt">
                                    <strong>{{ $featuredProduct->title }} 
                                        {{--<br>{{ $featuredProduct->categories->pluck('title')->implode(', ') }} --}}
                                    </strong>
                                    <p>{!! $featuredProduct->featured_description !!} </p>
                                    <a href="{{ route('product', $featuredProduct->slug) }}" class="details_btn">VIEW DETAILS</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <a href="#" class="btn-prev"><img src="{{ asset('frontend/images/slider_btn_left.png') }}" alt="<"></a>
                    <a href="#" class="btn-next"><img src="{{ asset('frontend/images/slider_btn.png') }}" alt=">"></a>
                    <div class="pagination"></div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@if($homeNewsHighlights->isNotEmpty())
<div class="hightlights container">
    <h2 class="sec_heading">News & Highlights</h2>
    <ul class="list_none">
        @foreach($homeNewsHighlights as $homeNewsHighlight)
        <li style="background:url({{ asset('/images/home/news-highlight/'.$homeNewsHighlight->id.'/'.$homeNewsHighlight->image) }}) no-repeat; background-size:cover;">
        @if($homeNewsHighlight->image_alt)
            <img src="{{ str_replace('/images/', '/images/background/', asset('/images/home/news-highlight/'.$homeNewsHighlight->id.'/'.$homeNewsHighlight->image)) }}" alt="{{ $homeNewsHighlight->image_alt }}" class="hidden" />
        @endif    
            <div class="txt">
                <span>{{ $homeNewsHighlight->label }} </span>
                <a href="{{ $homeNewsHighlight->url }}">
                    <strong class="title">{{ $homeNewsHighlight->title }}</strong>
                </a>
            </div>
        </li>
        @endforeach
    </ul>
</div>
@endif
@include('frontend.includes.follow_newsletter')
@endsection
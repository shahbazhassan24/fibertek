@extends('frontend.layouts.app')
@section('content')
<div class="container">
    <div class="pages_content">
        <aside id="sidebar">
            <?php if($categories->isNotEmpty()){ ?>
            <h2>FILTERS</h2>
            <div class="filter_form">
                <?php foreach ($categories as $category) { ?>
                <input type="checkbox" id="category-filter{{ $category->id }}" class="hidden category-filter" data-filter = "{{ $category->id }}">
                <label for="category-filter{{ $category->id }}" class="custom_check">{{ $category->title }}</label>
                <?php } ?>
            </div>
            <?php } ?>
        </aside>
        <div id="content" class="news">
            <?php if($news->isNotEmpty()){ ?>
            <div class="posts list_none manuals">
                <?php foreach ($news as $singleNews) { ?>
                <div class="event shadow item <?=implode(' ', preg_filter('/^/', 'category-', $singleNews->categories->pluck('id')->toArray()));?>">
                    <div class="img_holder">
                        <a href="{{ route('news-detail', $singleNews->slug) }}">
                            <img src="{{ asset('/images/news/'.$singleNews->id.'/'.$singleNews->image) }}" alt="{{ $singleNews->image_alt }}" class="img-responsive">
                        </a>
                    </div>
                    <div class="txt">
                        <h2><a href="{{ route('news-detail', $singleNews->slug) }}">{{ $singleNews->title }}</a></h2>
                        <span class="country">{{ $singleNews->country->title }}</span>
                        <span class="date">{{ \Carbon\Carbon::parse($singleNews->start_date)->format('d M Y') }} - {{ \Carbon\Carbon::parse($singleNews->end_date)->format('d M Y') }}</span>
                        <div title="Add to Calendar" class="addeventatc blue-btn" data-styling="none">
                        <img src="{{ asset('frontend/images/calendar_icon.png') }}" alt= "Add to Calendar">
                        <span class="calender-btn-title">Add to Calendar</span>
                        <span class="start">06/18/2019</span>
                        <span class="end">06/22/2019</span>
                        <span class="timezone">Asia/Singapore</span>
                        <span class="title">{{ $singleNews->title }}</span>
                        <span class="description">{{ $singleNews->title }}</span>
                        <!-- <span class="location">Location of the event</span>
                        <span class="organizer">Organizer</span>
                        <span class="organizer_email">Organizer e-mail</span>
                        <span class="all_day_event">false</span> -->
                        </div>
                        <!-- <div class="toggle_content">
                            <p>{!! $singleNews->description !!}</p>
                        </div> -->
                    </div>
                </div>
                <?php } ?>
            </div>
            <?php }else{ ?>
            <div class="nothing-found">
                Nothing Found!
            </div>
        <?php } ?>
        </div>
    </div>
</div>
@endsection
@extends('frontend.layouts.app')
@section('content')
<div class="search-page">
    <div class="container">
        <div class="rac_head">
            <strong>Search Website</strong>
        </div>
        <ul class="searched-links">
        @if($pages->isNotEmpty())
        @foreach($pages as $page)
            <li><a href="{{ route('page-slug', $page->slug) }}">{{ $page->title }}</a></li>
        @endforeach
        @endif
        @if($career)
            <li><a href="{{ route('page-slug', $cmsPages['2']['slug']) }}">{{ $cmsPages['2']['title'] }}</a></li>
        @endif
        @if($projectReference->isNotEmpty())
            <li><a href="{{ route('page-slug', $cmsPages['3']['slug']) }}">{{ $cmsPages['3']['title'] }}</a></li>
        @endif
        @if($catalogue)
            <li><a href="{{ route('page-slug', $cmsPages['4']['slug']) }}">{{ $cmsPages['4']['title'] }}</a></li>
        @endif
        @if($certification)
            <li><a href="{{ route('page-slug', $cmsPages['5']['slug']) }}">{{ $cmsPages['5']['title'] }}</a></li>
        @endif
        @if($manual)
            <li><a href="{{ route('page-slug', $cmsPages['6']['slug']) }}">{{ $cmsPages['6']['title'] }}</a></li>
        @endif        
        @if($faq)
            <li><a href="{{ route('page-slug', $cmsPages['7']['slug']) }}">{{ $cmsPages['7']['title'] }}</a></li>
        @endif
        </ul>
    </div>
    @if($posts->isNotEmpty())
    <div class="container">
        <div class="rac_head">
            <strong>Blog</strong>
        </div>
        <ul class="searched-links">
        @foreach($posts as $post)
            <li><a href="{{ route('blog-detail', $post->slug) }}">{{ $post->title }}</a></li>
        @endforeach
        </ul>
    </div>
    @endif
    @if($news->isNotEmpty())
    <div class="container">
        <div class="rac_head">
            <strong>News</strong>
        </div>
        <ul class="searched-links">
        @foreach($news as $singleNews)
            <li><a href="{{ route('news-detail', $singleNews->slug) }}">{{ $singleNews->title }}</a></li>
        @endforeach
        </ul>
    </div>
    @endif
    @if($categories->isNotEmpty())
    <div class="events_area">
        <div class="events_holder">
            <div id="content" class="fluid main-category">
                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        @foreach($categories as $category)
                        <div class="event">
                            <div class="event_holder">
                                <a href="{{ route('category', $category->slug) }}" class="detail_btn">View Products</a>
                                @if($category->image)
                                <div class="img_holder">
                                    <a href="{{ route('category', $category->slug) }}">
                                        <img src="{{ asset('/images/category/'.$category->id.'/'.$category->image) }}" alt="{{ (isset($category->images[0])) ? json_decode($category->images[0]->alt_text)->image : '' }}" class="img-responsive max_width">
                                    </a>
                                </div>
                                @endif
                                <div class="txt add {{ (!$category->image) ? 'no-image' : '' }}">
                                    <a href="{{ route('category', $category->slug) }}">
                                        <h2>{{ $category->title }}</h2>
                                    </a>
                                    <p>{!! $category->short_description !!}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($products->isNotEmpty())
    <div class="rack_products sub-category">
        <div class="container">
            <div class="rac_head">
                <select name="sort" id="product-sort">
                    <option value="">Sort By</option>
                    <option value="name-asc">Sort by Name A to Z</option>
                    <option value="name-desc">Sort by Name Z to A</option>
                    <option value="added-asc">Oldest to Newest</option>
                    <option value="added-desc">Newest to Oldest</option>
                </select>
                <strong>Products</strong>
            </div>
            <ul class="products_list list_none">
                @foreach($products as $product)
                <li data-time="{{ strtotime($product->created_at) }}">
                @if($product->images->isNotEmpty())
                    <a href="{{ route('product', $product->slug) }}">
                        <img src="{{ asset('/images/product/'.$product->id.'/'.$product->images[0]->name) }}" alt="{{ Helper::getImageAlt($product->images[0]->title) }}">
                    </a>
                @endif
                    <strong><a href="{{ route('product', $product->slug) }}">{{ $product->title }}</a></strong>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
</div>
@endsection
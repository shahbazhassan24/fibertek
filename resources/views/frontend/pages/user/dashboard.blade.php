@extends('frontend.layouts.app')
@section('content')
<div class="container">
  <div class="user-details-wrapper">
      <div class="fields">
        <div class="profile-info-wrapper">
        {{ Form::model($user, array('route' => 'user-update', 'method' => 'PUT', 'class' => 'update-information-form')) }}
        <header class="sec_header">
            <h2 class="sec_heading">User Details</h2>
        </header>
        <div class="field_row">
            <div class="field_col">
                <input type="text" placeholder="Your Name *" name="name" required="" value="{{ old('name', $user->name) }}">
            </div>
            <div class="field_col">
                <input type="email" placeholder="Your Email *" name="email" required="" value="{{ old('email', $user->email) }}">
            </div>
        </div>
        <div class="field_row">
            <div class="field_col">
                <input type="text" placeholder="Company Name" name="company_name" value="{{ old('company_name', $user->company_name) }}">
            </div>
            <div class="field_col">
                <select name="country_id" class="form-control required autocomplete="country_id">
                    <option value="">Country</option>
                    @foreach($countries as $country)
                    <option value="{{ $country->id }}" {{ old('country_id', $user->country_id) ==  $country->id ? 'selected' : '' }}>{{ $country->title }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="field_row submit_field">
            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            <div class="success-message">Information has been updated!</div>
        </div>
        {{ Form::close() }}
        </div>
        <div class="password-info-wrapper">
    {{ Form::model($user, array('route' => 'user-password', 'method' => 'PUT', 'class' => 'update-information-form')) }}
        <header class="sec_header">
            <h2 class="sec_heading">Update Password</h2>
        </header>
        <div class="field_row">
            <div class="field_col">
                <input type="password" placeholder="Old Password *" name="old_password" required="">
            </div>
        </div>
        <div class="field_row">
            <div class="field_col">
                <input type="password" placeholder="New Password *" name="password" required="">
            </div>
        </div>
        <div class="field_row">
            <div class="field_col">
                <input type="password" placeholder="New Password Confirm *" name="password_confirmation" required="">
            </div>
        </div>
        <div class="field_row submit_field">
            {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
            <div class="success-message">Information has been updated!</div>
        </div>
        {{ Form::close() }}
    </div>
    </div>
  </div>
  @if($downloads->isNotEmpty())
  <div class="user-downloads-wrapper">
  <table>
            <thead>
                <tr>
                    <th>Sr #</th>
                    <th>Document</th>
                    <th>Type</th>
                    <th>Download</th>
                </tr>
            </thead>
            <tbody>
            @php ($i = 1)
            @foreach ($downloads as $download)
                @if($download->type == 'catalogue')
                    @php ($title = $download->catalogue->title)
                    @php ($url = asset('/documents/catalogue/'.$download->catalogue->id.'/'.$download->catalogue->document))
                @elseif($download->type == 'certification')
                    @php ($title = $download->certification->title)
                    @php ($url = asset('/documents/certification/'.$download->certification->id.'/'.$download->certification->document))
                @elseif($download->type == 'manual')
                    @php ($title = $download->manual->title)
                    @php ($url = asset('/documents/manual/'.$download->manual->id.'/'.$download->manual->document))
                @elseif($download->type == 'project_reference')
                    @php ($title = $download->project_reference->title)
                    @php ($url = asset('/documents/project-reference/'.$download->project_reference->id.'/'.$download->project_reference->document))
                @elseif($download->type == 'product')
                    @php ($title = $download->product->title)
                    @php ($url = asset('/documents/product/'.$download->product->id.'/'.$download->product->documents[0]->name))
                @endif
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $title }}</td>
                <td>{{ ucfirst($download->type) }}</td>
                <td class="download-link"><a href="{{ $url }}" target="_blank">Download</a></td>
            </tr>
            @endforeach
            </tbody>
        </table>
  </div>
  @endif
</div>
@endsection
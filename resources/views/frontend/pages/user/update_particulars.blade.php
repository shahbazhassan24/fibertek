@extends('layouts.user')
@section('content')
<div class="text_area add tab_text" >
  <div class="tab_holder contact-form">
    @include('includes.error_messages')
     <h2>Update Particulars</h2>
     <form action="" class="tab_form" method="post" accept-charset="utf-8" id="updateParticularsForm">
        <?=csrf_field();?>
        <div class="row active">
            <span>Full Name *</span>
            <input type="text" name="name" placeholder="John Doe" value="@if(old('name')){{ old('name') }}@elseif($user->name){{ $user->name }}@endif">
        </div>
        <div class="row active">
            <span>Company Name *</span>
            <input type="text" name="company_name" placeholder="XYZ Construction" value="@if(old('company_name')){{ old('company_name') }}@elseif($user->company_name){{ $user->company_name }}@endif">
        </div>
        <div class="row active">
            <span>Company Address *</span>
            <input type="text" name="company_address" placeholder="" value="@if(old('company_address')){{ old('company_address') }}@elseif($user->company_address){{ $user->company_address }}@endif">
        </div>
        <div class="row active">
            <span>Job Designation/ Appointment *</span>
            <input type="text" name="job_designation" placeholder="" value="@if(old('job_designation')){{ old('job_designation') }}@elseif($user->job_designation){{ $user->job_designation }}@endif">
        </div>
        <div class="row active">
            <span>Office Number *</span>
            <input type="text" name="office_number" placeholder="" value="@if(old('office_number')){{ old('office_number') }}@elseif($user->office_number){{ $user->office_number }}@endif">
        </div>
        <div class="row active">
        <span>Mobile Number *</span>
        <input type="text" name="mobile_number" placeholder="" value="@if(old('mobile_number')){{ old('mobile_number') }}@elseif($user->mobile_number){{ $user->mobile_number }}@endif">
        </div>
        <div class="row active">
            <span>ACRA *</span>
            <input type="text" name="acra" placeholder="" value="@if(old('acra')){{ old('acra') }}@elseif($user->acra){{ $user->acra }}@endif">
        </div>
        <div class="row text-right submit_row"style="width:50%;float:left;">
          <input type="submit" class="submit_btn" value="Submit">
        </div>
     </form>
  </div>
</div>
@endsection
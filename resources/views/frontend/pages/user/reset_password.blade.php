@extends('layouts.user')
@section('content')
<div class="text_area add tab_text" >
  <div class="tab_holder contact-form">
    @include('includes.error_messages')
     <h2>Update Password</h2>
     <form action="" class="tab_form" method="post" accept-charset="utf-8" id="resetPasswordForm">
        <?=csrf_field();?>
        <div class="row">
          <span>Current Password *</span>
          <input type="password" placeholder="" name="current_password" value="">
        </div>
        <div class="row">
          <span>New Password *</span>
          <input type="password" placeholder="" name="new_password" value="">
        </div>
        <div class="row">
          <span>Confirm New Password *</span>
          <input type="password" placeholder="" name="cnew_password" value="">
        </div>
        <div class="password-notes">
          <ul>
              <li>Minimum length 8 characters</li>
              <li>At least 1 lower case letter [a-z]</li>
              <li>At least 1 upper case letter [A-Z]</li>
              <li>At least 1 numeric character [0-9]</li>
              <li>At least 1 special character</li>
          </ul>
        </div>
        <div class="row text-right submit_row"style="width:50%;float:left;">
          <input type="submit" class="submit_btn" value="Submit">
        </div>
     </form>
  </div>
</div>
@endsection
@extends('frontend.layouts.app')
@section('content')
<?php
$textWidth = '47';
?>
<div class="grow">
    <div class="container">
        <?php
        if($images->isEmpty()){
            $textWidth = '100';
        }
        ?>
        <div class="text" style="width: {{ $textWidth }};">
            {!! $page->content !!}
        </div>
        <?php if($images->isNotEmpty()){ ?>
        <div class="slider">
            <div class="gallery">
                <div class="mask">
                    <div class="slideset">
                        <?php foreach ($images as $image) { ?>
                        <div class="slide">
                            <img src="{{ asset('/images/career/'.$image->name) }}" alt="{{ Helper::getImageAlt($image->name) }}" class="img-responsive">
                        </div>
                        <?php } ?>
                    </div>
                    <a href="#" class="btn-prev"><img src="{{ asset('frontend/images/slider_btn_left.png') }}" alt="<"></a>
                    <a href="#" class="btn-next"><img src="{{ asset('frontend/images/slider_btn.png') }}" alt=">"></a>
                    <div class="pagination"></div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php if($lifeFibertek->isNotEmpty()){ ?>
<div class="culture">
    <div class="container">
        <h2 class="sec_heading">Life @ FiberTek</h2>
        <ul class="list_none">
            <?php foreach ($lifeFibertek as $singleLifeFibertek) { ?>
            <li>
                <div class="holder">
                    <img src="{{ asset('/images/career/lifeFibertek/'.$singleLifeFibertek->image) }}" alt="{{ $singleLifeFibertek->title }}" class="img-responsive">
                    <div class="caption">
                        <div class="d_table">
                            <div class="v_bottom">
                                <strong>{{ $singleLifeFibertek->title }}</strong>
                                <p class="anim">{!! $singleLifeFibertek->description !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<?php } ?>
<?php if($jobs->isNotEmpty()){ ?>
<div class="jobs">
    <div class="container">
        <h2 class="sec_heading">Hot Jobs</h2>
        <div class="holder">
            <?php foreach ($jobs as $job) { ?>
            <div class="job">
                <a href="#" class="job_opener">
                    <i>
                        <img src="{{ asset('frontend/images/plus.png') }}" alt="+">
                        <img src="{{ asset('frontend/images/close.png') }}" alt="x" class="close">
                    </i>
                    <span>{{ $job->title }} <em>{{ $job->country->title }}</em></span>
                </a>
                <div class="job_text">
                    <div class="date">
                        <p><b>Date Posted:</b> {{ $job->created_at->format('d M Y') }}</p>
                        <p><b>Category:</b> {{ $job->categories->pluck('title')->implode(', ') }}</p>
                    </div>
                    @if($job->job_description)
                    <div class="job_desc">
                        <strong class="title">Job Description</strong>
                        {!! $job->job_description !!}
                    </div>
                    @endif
                    @if($job->qualification)
                    <div class="job_reqs">
                        <strong class="title">Qualifications & Requirements</strong>
                        {!! $job->qualification !!}
                    </div>
                    @endif
                    <a href="mailto:{{ $configuration['email_enquiry_career'] }}?subject={{ $job->title }}" class="btn_apply" target="_blank">APPLY NOW</a>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>
@include('frontend.includes.follow_newsletter')
@endsection
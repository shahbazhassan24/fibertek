@extends('frontend.layouts.app')
@section('content')
<div class="jobs faqs">
    <div class="container">
        <form class="q_searcher" id="faq-search-form">
            <input type="search" name="search" placeholder="Do you want to know more? Ask a question." value="{{ old('search', $search) }}">
            <button type="submit"></button>
        </form>
        <?php if($faqs->isNotEmpty()){ ?>
        <div class="holder">
        <?php foreach ($faqs as $faq) { ?>
            <div class="job">
                <a href="#" class="job_opener">
                    <i>
                        <img src="{{ asset('frontend/images/plus.png') }}" alt="+">
                        <img src="{{ asset('frontend/images/close.png') }}" alt="x" class="close">
                    </i>
                    <span>{{ $faq->question }}</span>
                </a>
                <div class="job_text">
                {!! $faq->answer !!}
                </div>
            </div>
        <?php } ?>
        </div>
        <?php }else{ ?>
            <div class="nothing-found">
                Nothing Found!
            </div>
        <?php } ?>
    </div>
</div>
@include('frontend.includes.follow_newsletter')
@endsection
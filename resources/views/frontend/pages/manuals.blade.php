@extends('frontend.layouts.app')
@section('content')
<div class="container">
    <ul class="list_none pages">
        <li><a href="{{ route('blog') }}">BLOG</a></li>
        @if(isset($pageSlugs['4']) && ($pageSlugs['4']['status'] == '1'))
        <li><a href="{{ route('page-slug', $pageSlugs['4']['slug']) }}">{{ strtoupper($pageSlugs['4']['title']) }}</a></li>
        @endif
        @if(isset($pageSlugs['5']) && ($pageSlugs['5']['status'] == '1'))
        <li><a href="{{ route('page-slug', $pageSlugs['5']['slug']) }}">{{ strtoupper($pageSlugs['5']['title']) }}</a></li>
        @endif
        @if(isset($pageSlugs['6']) && ($pageSlugs['6']['status'] == '1'))
        <li class="active"><a href="{{ route('page-slug', $pageSlugs['6']['slug']) }}">{{ strtoupper($pageSlugs['6']['title']) }}</a></li>
        @endif
    </ul>
    <div class="pages_content">
        <div class="container">
            <aside id="sidebar">
                {{ Form::open(array('route' => array('page-slug', $pageSlugs['6']['slug']), 'method' => 'GET')) }}
                <!-- <div class="q_searcher">
                    <input type="text" placeholder="Enter Keyword" name="keyword" value="{{ (isset($_GET['keyword']) && $_GET['keyword'] != "") ? $_GET['keyword'] : '' }}">
                </div> -->
                {{ Form::close() }}
                <?php if($categories->isNotEmpty()){ ?>
                <h2>FILTERS</h2>
                <div class="filter_form">
                    <?php foreach ($categories as $category) { ?>
                    <input type="checkbox" id="category-filter{{ $category->id }}" class="hidden category-filter" data-filter = "{{ $category->id }}">
                    <label for="category-filter{{ $category->id }}" class="custom_check">{{ $category->title }}</label>
                    <?php } ?>
                </div>
                <?php } ?>
            </aside>
            <div id="content">
                <?php if($manuals->isNotEmpty()){ ?>
                <ul class="posts list_none manuals">
                    <?php foreach ($manuals as $manual) { ?>
                    <li class="item <?=implode(' ', preg_filter('/^/', 'category-', $manual->categories->pluck('id')->toArray()));?>">
                        <div class="manual_holder">
                            <img src="{{ asset('/images/manual/'.$manual->id.'/'.$manual->image) }}" alt="{{ $manual->image_alt }}" class="img-responsive">
                            <div class="txt">
                                <h2>{{ $manual->title }}</h2>
                                <p>{!! $manual->description !!}</p>
                                @if(!Auth::check())
                                <a href="#" class="pdf_btn open-modal" data-modal="required-login">
                                @else
                                <a href="{{ asset('/documents/manual/'.$manual->id.'/'.$manual->document) }}" class="pdf_btn add-to-downloads" data-type="manual" data-document-id="{{ $manual->id }}" target="_blank">
                                @endif
                                    <img src="{{ asset('frontend/images/pdf_icon.png') }}" alt="Download PDF"> Download PDF
                                </a>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <?php }else{ ?>
                <div class="nothing-found">
                    Nothing Found!
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
</div>
@endsection
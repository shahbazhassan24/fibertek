@extends('frontend.layouts.app')
@section('content')
<div class="events_area">
    <div class="events_holder">
        <header class="sec_header text-center">
            {!! $projectReference->description !!}
        </header>
        @if(isset($projectReference->projects))
        <table>
            <thead>
                <tr>
                    <th>Year</th>
                    <th>Project Name</th>
                    <th>Country</th>
                    <th>Product</th>
                </tr>
            </thead>
            <tbody>
                @php ($projects = json_decode($projectReference->projects, true))
                @for($projectCounter = 0; $projectCounter < $projectReference->projectsCount; $projectCounter++)
                <tr>
                    <td>{{ $projects['year'][$projectCounter] }}</td>
                    <td>{{ $projects['name'][$projectCounter] }}</td>
                    <td>{{ $projects['country'][$projectCounter] }}</td>
                    <td>{{ $projects['product'][$projectCounter] }}</td>
                </tr>
                @endfor
            </tbody>
        </table>
        @endif
        @if($projectReference->document)
        <div class="btn_div">
        @if(!Auth::check())
            <a href="#" class="pdf_btn open-modal" data-modal="required-login">
        @else
            <a href="{{ asset('/documents/project-reference/'.$projectReference->id.'/'.$projectReference->document)}}" class="load_btn export add-to-downloads" data-type="project_reference" data-document-id="{{ $projectReference->id }}" target="_blank">
        @endif
                <img src="{{ asset('frontend/images/pdf_icon.png') }}" alt="Download List"> Download List
            </a>
        </div>
        @endif
    </div>
</div>
@endsection
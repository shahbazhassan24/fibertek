@extends('frontend.layouts.app')
@section('content')
<div class="container map_area contact-page">
    <div class="map_holder">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.7455374254005!2d103.89094181426539!3d1.3286993620143503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x47ea83c5f665164c!2sFiberTek+Pte+Ltd!5e0!3m2!1sen!2s!4v1563790998895!5m2!1sen!2s" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="text">
        <h2>FiberTek Pte Ltd</h2>
        <p>Blk 3015A <br> Ubi Road 1 #04-13 <br> Singapore 408705</p>
        <ul class="list_none">
            <li><img src="{{ asset('frontend/images/phone_icon.png') }}" alt="FiberTek Contact Number"> <a href="tel:{!! str_replace(' ', '', $configuration['contact_phone']) !!}">{!! $configuration['contact_phone'] !!}</a></li>
            <li><img src="{{ asset('frontend/images/printer_icon.jpg') }}" alt="FiberTek Fax Number"> <a href="tel:{!! str_replace(' ', '', $configuration['fax_number']) !!}">{!! $configuration['fax_number'] !!}</a></li>
            <li><img src="{{ asset('frontend/images/mail_icon.jpg') }}" alt="FiberTek Email Address"> <a href="mailto:{!! $configuration['contact_email'] !!}" class="blue">{!! $configuration['contact_email'] !!}</a></li>
        </ul>
    </div>
    <div class="share">
        <em><a href="#" class="btn_social share-btn"><img src="{{ asset('frontend/images/share_icon.png') }}" alt="FiberTek Share"></a></em>
        <div class="share-listing">
            <ul class="list_none">
                <li><a href="{{ Helper::getSocialShareLink(Request::url()) }}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="{{ Helper::getSocialShareLink(Request::url(), 'twitter') }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="{{ Helper::getSocialShareLink(Request::url(), 'linkedin') }}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="{{ Helper::getSocialShareLink(Request::url(), 'whatsapp') }}" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                <li><a href="mailto:?Subject=Contact%20Us%20Fibertek&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 {{ Request::url() }}" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
</div>
<form class="contact_form" id="contact-enquiry-form">
    <h2 class="sec_heading">Get in Touch</h2>
    <p>Please send us your feedback or enquiry, we will get back to you soonest possible.</p>
    <div class="fields">
        <div class="field_row">
            <div class="field_col">
                <input type="text" placeholder="Your Name *" name="name" required>
            </div>
            <div class="field_col">
                <input type="email" placeholder="Your Email *" name="email" required>
            </div>
        </div>
        <div class="field_row">
            <div class="field_col">
                <input type="text" placeholder="Company Name" name="company">
            </div>
            <div class="field_col">
                <input type="text" placeholder="Contact Number" name="phone">
            </div>
        </div>
        <div class="field_row">
            <div class="field_col fluid">
                <textarea placeholder="Your Feedback or Enquiry *" name="enquiry" required></textarea>
            </div>
        </div>
        <div class="field_row captcha">
            <div class="field_col">
                <select name="how_hear" required>
                    <option value="">How did you hear about us?</option>
                    <option value="Search Engine (Google, Yahoo etc)">Search Engine (Google, Yahoo etc)</option>
                    <option value="Social Media ( Facebook, YouTube etc)">Social Media ( Facebook, YouTube etc)</option>
                    <option value="Print Media">Print Media</option>
                    <option value="Existing Customer">Existing Customer</option>
                </select>
            </div>
            <div class="field_col">
                {!! htmlFormSnippet() !!}
                <span class="captcha-error-message">Captcha Required.</span>
            </div>
        </div>
        <div class="field_row submit_field">
            <div class="submit-btn">
                <input type="submit" value="SEND MESSAGE" class="btn-ajax-submit">
                <div class="ajax-loader hide"></div>
            </div>
            <div class="success-message">Request has been submitted!</div>
        </div>
    </div>
</form>
@include('frontend.includes.follow_newsletter')
@endsection
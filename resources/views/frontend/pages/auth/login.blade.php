@extends('frontend.layouts.login')
@section('content')
<div class="login">
    <div class="d_table">
        <div class="v_middle">
            <div class="login_form">
                <form method="POST" action="{{ route('login') }}" autocomplete="off" id="login-form">
                    @csrf
                    <strong>Login to your account</strong>
                    <div class="fields">
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/user.png') }}" alt="Email Address">
                            </div>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="johndoe@gmail.com" autofocus autocomplete="off">
                        </div>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/pass.png') }}" alt="Password">
                            </div>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" autocomplete="off">
                        </div>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="row no_border">
                            <div class="label_holder">
                                <input class="form-check-input hidden" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember" class="custom_check">Remember me</label>
                            </div>
                            <input type="submit" value="Login">
                        </div>
                        <div class="user_links">
                            @if (Route::has('password.request'))
                                <a href="{{ route('password.request') }}">
                                    {{ __('Forgot your Password?') }}
                                </a>
                            @endif
                            @if (Route::has('register'))
                                <a href="{{ route('register') }}">
                                    {{ __('Create a new account') }}
                                </a>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
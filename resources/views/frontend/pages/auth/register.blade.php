@extends('frontend.layouts.login')
@section('content')
<div class="login">
    <div class="d_table">
        <div class="v_middle">
            <div class="register-left-side">
                <div class="info-wrapper">
                    <div class="why-title">Why sign up for an account?</div>
                    <ul>
                        <li>View & Track downloaded activities</li>
                        <li>Stay alert on the latest company update</li>
                    </ul>
                </div>
            </div>
            <div class="login_form">
                <form method="POST" action="{{ route('register') }}" autocomplete="off" id="register-form">
                    @csrf
                    <strong>Register new account</strong>
                    <div class="fields">
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/user.png') }}" alt="Name">
                            </div>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="John Doe *" required autocomplete="name" autofocus>
                        </div>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/user.png') }}" alt="Email Address">
                            </div>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="johndoe@gmail.com *" required autocomplete="off">
                        </div>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/user.png') }}" alt="Company">
                            </div>
                            <input id="company_name" type="text" class="form-control @error('company_name') is-invalid @enderror" name="company_name" value="{{ old('company_name') }}" placeholder="Company Name *" required autocomplete="company_name" autofocus>
                        </div>
                        @error('company_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/user.png') }}" alt="Country">
                            </div>
                            <select name="country_id" class="form-control @error('country_id') is-invalid @enderror" required autocomplete="country_id" autofocus>
                                <option value="">Country</option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{ old('country_id') ==  $country->id ? 'selected' : '' }}>{{ $country->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('country')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/pass.png') }}" alt="Password">
                            </div>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password *" required autocomplete="off">
                        </div>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/pass.png') }}" alt="Password">
                            </div>
                            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required autocomplete="off" placeholder="Confirm Password *">
                        </div>
                        <div class="row no_border register-checkbox-alerts">
                            <div class="label_holder">
                                <input class="form-check-input hidden" type="checkbox" name="stay_alert" id="stay_alert" checked value="1">
                                <label for="stay_alert" class="custom_check">Stay alert on the latest developments of FiberTek</label>
                            </div>
                            <div class="label_holder">
                                <input class="form-check-input" type="checkbox" name="terms" id="terms" checked value="1">
                                <label for="terms" class="custom_check">Agree to the term of use of the website</label>
                            </div>
                        </div>
                        <div class="row no_border">
                            {!! htmlFormSnippet() !!}
                        </div>
                        @error('g-recaptcha-response')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="captcha-error-message" for="captcha">Captcha Required.</label>
                        <div class="row no_border">
                            <input type="submit" value="Register">
                        </div>
                        <div class="user_links">
                            @if (Route::has('password.request'))
                                <a href="{{ route('password.request') }}">
                                    {{ __('Forgot your Password?') }}
                                </a>
                            @endif
                            @if (Route::has('login'))
                                <a href="{{ route('login') }}">
                                    {{ __('Already has account') }}
                                </a>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

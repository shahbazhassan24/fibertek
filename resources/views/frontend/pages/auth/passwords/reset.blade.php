@extends('frontend.layouts.login')
@section('content')
<div class="login">
    <div class="d_table">
        <div class="v_middle">
            <div class="login_form">
                <form method="POST" action="{{ route('password.update') }}" autocomplete="off" id="reset-password-form">
                    @csrf
                    <strong>{{ __('Reset Password') }}</strong>
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="fields">
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/user.png') }}" alt="Email Address">
                            </div>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="johndoe@gmail.com" autofocus autocomplete="off">
                        </div>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/pass.png') }}" alt="Password">
                            </div>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" autocomplete="off">
                        </div>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="row">
                            <div class="img_holder">
                                <img src="{{ asset('frontend/images/pass.png') }}" alt="Password">
                            </div>
                            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" autocomplete="off" placeholder="Confirm Password">
                        </div>
                        <div class="row no_border">
                            <input type="submit" value="Reset Password">
                        </div>
                        <div class="user_links">
                            @if (Route::has('login'))
                                <a href="{{ route('login') }}">
                                    {{ __('Already has account') }}
                                </a>
                            @endif
                            @if (Route::has('register'))
                                <a href="{{ route('register') }}">
                                    {{ __('Create a new account') }}
                                </a>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

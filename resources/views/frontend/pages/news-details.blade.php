@extends('frontend.layouts.app')
@section('content')
<div class="container blog">
    <div class="text">
        <h1>{{ $news->title }}</h1>
        <span>{{ strtoupper($news->created_at->format('jS M Y')) }}</span>
        @if($news->image)
        <div class="featured-image">
            <img src="{{ asset('/images/news/'.$news->id.'/'.$news->image) }}" alt="{{ $news->image_alt }}">
        </div>
        @endif
        {!! $news->description !!}
    </div>
</div>
@endsection
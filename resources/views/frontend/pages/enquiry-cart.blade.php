@extends('frontend.layouts.app')
@section('content')
<div class="container enquiry-cart-page" id="request-quote">
    <div class="enquiry-list_wraper">
        <h2 class="secheading">Enquiry Cart</h2>
        <ul class="enquiry-list">
        @php($requestQuoteProducts = request()->session()->get('request_quote_products'))
        @if($requestQuoteProducts)
            @foreach($requestQuoteProducts as $quoteProduct)
            <li>
                <div class="cart-img">
                    <a href="{{ route('product', $quoteProduct['slug']) }}">
                        <img src="{{ asset('/images/product/'.$quoteProduct['id'].'/'.$quoteProduct['image']) }}" alt="{{ ($quoteProduct['image_alt']) ? $quoteProduct['image_alt'] : Helper::getImageAlt($quoteProduct['image']) }}">
                    </a>
                </div>
                <div class="en-cart-title_wrap">
                    <a href="{{ route('product', $quoteProduct['slug']) }}" class="en-cart-title">{{ $quoteProduct['title'] }}</a>
                </div>
                <div class="qty-wrap" data-product-id="{{ $quoteProduct['id'] }}">
                    <span>Quantity:</span> <input type="number" min="1" step="1" value="1">
                </div>
                <div class="enq-cart-remove">
                    <button type="button" class="item-delete remove-request-a-quote" data-product-id="{{ $quoteProduct['id'] }}">&#10005;</button>
                </div>
            </li>
            @endforeach
        @else
            <li><div class="en-cart-title_wrap no-product-enquire">No Product for Request a Quote</div></li>
        @endif
        </ul>
    </div> <!--end shopping-cart -->
    @if($requestQuoteProducts)
    <form class="contact_form request-quote-form" id="request-quote-form">
        <!-- <p>Please send us your feedback or enquiry, we will get back to you soonest possible.</p> -->
        @csrf
        <div class="fields">
            <div class="field_row">
                <div class="field_col">
                    <input type="text" placeholder="Your Name *" name="name" required>
                </div>
                <div class="field_col">
                    <input type="text" placeholder="Your Email *" name="email" required>
                </div>
            </div>
            <div class="field_row">
                <div class="field_col">
                    <input type="text" placeholder="Company Name" name="company">
                </div>
                <div class="field_col">
                    <input type="text" placeholder="Contact Number" name="phone">
                </div>
            </div>
            <div class="field_row">
                <div class="field_col fluid">
                    <textarea placeholder="Your Enquiry *" name="enquiry" required></textarea>
                </div>
            </div>
            <div class="field_row captcha">
                <div class="field_col pull-right">
                    {!! htmlFormSnippet() !!}
                    <span class="captcha-error-message">Captcha Required.</span>
                </div>
            </div>
            <div class="field_row submit_field">
                <div class="submit-btn">
                    <input type="submit" value="SEND ENQUIRY" class="btn-ajax-submit">
                    <div class="ajax-loader hide"></div>
                </div>
                <div class="success-message">Request has been submitted!</div>
            </div>
            @if($requestQuoteProducts)
                @foreach($requestQuoteProducts as $quoteProduct)
                <input type="hidden" name="quoteProduct[product][{{$quoteProduct['id']}}][id]" value="{{ $quoteProduct['id'] }}">
                <input type="hidden" name="quoteProduct[product][{{$quoteProduct['id']}}][title]" value="{{ $quoteProduct['title'] }}">
                <input type="hidden" name="quoteProduct[product][{{$quoteProduct['id']}}][slug]" value="{{ $quoteProduct['slug'] }}">
                <input type="hidden" name="quoteProduct[product][{{$quoteProduct['id']}}][image]" value="{{ $quoteProduct['image'] }}">
                <input type="hidden" name="quoteProduct[product][{{$quoteProduct['id']}}][quantity]" value="1" class="enquiry-cart-product-quantity-{{$quoteProduct['id']}}">
                @endforeach
            @endif
        </div>
    </form>
    @endif
</div>
@include('frontend.includes.follow_newsletter')
@endsection
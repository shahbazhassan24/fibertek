@extends('frontend.layouts.app')
@section('content')
<div class="events_area who-we-are">
    <div class="events_holder">
        <header class="sec_header text-center">
            {!! $page->content !!}
        </header>
        @if($page->extras)
        @php ($pageExtras = json_decode($page->extras, true))
        <div class="twocols">
            <div class="col">
                @if($pageExtras['0']['image'])
                <img src="{{ asset('/images/page/'.$page->id.'/extra/'.$pageExtras['0']['image']) }}" alt="{{ Helper::getImageAlt($pageExtras['0']['image']) }}" class="img-responsive">
                @endif
                @if($pageExtras['0']['description'])
                <div class="col_caption">
                    {!! $pageExtras['0']['description'] !!}
                </div>
                @endif
            </div>
            <div class="col">
                @if($pageExtras['1']['image'])
                <img src="{{ asset('/images/page/'.$page->id.'/extra/'.$pageExtras['1']['image']) }}" alt="{{ Helper::getImageAlt($pageExtras['1']['image']) }}" class="img-responsive">
                @endif
                @if($pageExtras['1']['description'])
                <div class="col_caption">
                    {!! $pageExtras['1']['description'] !!}
                    <ul class="list_none">
                    @if($mainCategories->isNotEmpty())
                        @foreach($mainCategories as $mainCategory)
                            @if($mainCategory->status)
                            <!-- <li>
                                <a href="{{ route('category', $mainCategory->slug) }}">
                                    {{ $mainCategory->title }}
                                </a>
                            </li> -->
                            @endif
                        @endforeach
                    @endif
                    </ul>
                </div>
                @endif
            </div>
        </div>
        <div class="twocols three">
            <div class="col">
                @if($pageExtras['2']['image'])
                <img src="{{ asset('/images/page/'.$page->id.'/extra/'.$pageExtras['2']['image']) }}" alt="{{ Helper::getImageAlt($pageExtras['2']['image']) }}" class="img-responsive">
                @endif
                @if($pageExtras['2']['description'])
                <div class="col_caption">
                    {!! $pageExtras['2']['description'] !!}
                </div>
                @endif
            </div>
            <div class="col">
                @if($pageExtras['3']['image'])
                <img src="{{ asset('/images/page/'.$page->id.'/extra/'.$pageExtras['3']['image']) }}" alt="{{ Helper::getImageAlt($pageExtras['3']['image']) }}" class="img-responsive">
                @endif
                @if($pageExtras['3']['description'])
                <div class="col_caption">
                    {!! $pageExtras['3']['description'] !!}
                </div>
                @endif
            </div>
            <div class="col">
                @if($pageExtras['4']['image'])
                <img src="{{ asset('/images/page/'.$page->id.'/extra/'.$pageExtras['4']['image']) }}" alt="{{ Helper::getImageAlt($pageExtras['4']['image']) }}" class="img-responsive">
                @endif
                @if($pageExtras['4']['description'])
                <div class="col_caption">
                    {!! $pageExtras['4']['description'] !!}
                </div>
                @endif
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
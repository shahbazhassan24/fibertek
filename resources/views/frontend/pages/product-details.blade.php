@extends('frontend.layouts.app')
@section('content')
<div class="product_galery">
    <div class="container">
        @if($product->images->isNotEmpty())
        <div class="align_left">
            <div class="sh-product-gallery">
                <div class="sh-p-gallery-main watermarked-image-disable">
                    @foreach($product->images as $productImage)
                    <div class="main_image_holder"><img src="{{ asset('/images/product/'.$product->id.'/'.$productImage->name) }}" alt="{{ ($productImage->alt_text) ? $productImage->alt_text : Helper::getImageAlt($productImage->title) }}" class="img-responsive img_pop_opener"></div>
                    @endforeach
                </div>
                <span class="msg img_pop_opener">Click to open expanded view</span>
                <div class="sh-p-gallery-thumb">
                    @foreach($product->images as $productImage)
                    <div class="thumb_holder"><img src="{{ asset('/images/product/'.$product->id.'/'.$productImage->name) }}" alt="{{ ($productImage->alt_text) ? $productImage->alt_text : Helper::getImageAlt($productImage->title) }}"></div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        <div class="align_right">
            <h2>{{ $product->title }}</h2>
            {!! $product->short_description !!}
            <div class="b_part">
                <div class="product-social-wrapper">
                    <div class="product-social-lable">
                        Share :
                    </div>
                    <div class="product-social-icons">
                        <ul class="social_icons list_none">
                            <li><a href="{{ Helper::getSocialShareLink(route('product', $product->slug )) }}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="{{ Helper::getSocialShareLink(route('product', $product->slug ), 'twitter') }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="{{ Helper::getSocialShareLink(route('product', $product->slug ), 'linkedin') }}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="{{ Helper::getSocialShareLink(route('product', $product->slug ), 'whatsapp') }}" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                            <li><a href="mailto:?Subject={{ $product->title }}&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 {{ route('product', $product->slug ) }}" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="left">
                    @if(isset($product->documents[0]))
                        @if(!Auth::check())
                        <a href="#" class="pdf_btn open-modal" data-modal="required-login">
                        @else
                        <a href="{{ asset('/documents/product/'.$product->id.'/'.$product->documents[0]->name) }}" class="pdf_btn add-to-downloads" data-type="product" data-document-id="{{ $product->id }}" target="_blank">
                        @endif
                            <img src="{{ asset('frontend/images/pdf_icon.png') }}" alt="Download Datasheet"> Datasheet
                        </a>
                    @endif
                        @php($requestQuoteProducts = request()->session()->get('request_quote_products'))
                        @php($requestAQuoteText = 'Request a Quote')
                        @php($requestAQuoteClass = 'add-request-a-quote')
                        @if((!empty($requestQuoteProducts) && isset($requestQuoteProducts[$product->id])))
                        @php($requestAQuoteText = 'Added for Quote')
                        @php($requestAQuoteClass = 'remove-request-a-quote')
                        @endif
                        <a href="#" class="active {{ $requestAQuoteClass }}" data-product-id="{{ $product->id }}"><img src="{{ asset('frontend/images/pencil_icon.png') }}" alt="{{ $requestAQuoteText }}"><span>{{ $requestAQuoteText }}</span></a>
                </div>
                <div class="alert alert-success request-a-quote-message" role="alert">
                    {{ $product->title }} has been <span class="request-a-quote-action">added to</span> your enquiry cart.
                </div>
            </div>
        </div>
    </div>
    <div class="container product_tabs-holder">
        <ul class="list_none tabs product_tabs">
            <li class="active"><a href="#description">Description</a></li>
            @if($product->linkedProducts->isNotEmpty())
            <li><a href="#related">Related Products</a></li>
            @endif
            <!-- <li><a href="#downloads">Downloads</a></li> -->
        </ul>
        <div class="tab-content">
            <div id="description" class="tab active">
                {!! $product->description !!}
            </div>
            @if($product->linkedProducts->isNotEmpty())
            <div id="related" class="tab">
                <div id="content" class="fluid">
                    @foreach($product->linkedProducts as $relatedProduct)
                    <div class="event p_cat_details">
                        <span class="detail_btn_holder"><a href="{{ route('product', $relatedProduct->product->slug) }}" class="detail_btn">View Product</a></span>
                        <div class="event_holder">
                            @if(isset($relatedProduct->product->images[0]))
                            <div class="img_holder">
                                <img src="{{ asset('/images/product/'.$relatedProduct->product->id.'/'.$relatedProduct->product->images[0]->name) }}" alt="{{ ($relatedProduct->product->images[0]->alt_text) ? $relatedProduct->product->images[0]->alt_text : Helper::getImageAlt($relatedProduct->product->images[0]->title) }}" class="img-responsive max_width">
                            </div>
                            @endif
                            <div class="txt add">
                                <h2>{{ $relatedProduct->product->title }}</h2>
                                <p>{!! $relatedProduct->product->short_description !!}  </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- <div class="loading_div text-center">
                    <a href="#"><img src="{{ asset('frontend/images/load_btn.jpg') }}" alt="Load more"></a>
                </div> -->
            </div>
            @endif
            <div id="downloads" class="tab">
                <div id="content" class="fluid">

                </div>
                <!-- <div class="loading_div text-center">
                    <a href="#"><img src="{{ asset('frontend/images/load_btn.jpg') }}" alt="Load more"></a>
                </div> -->
            </div>
        </div>
    </div>
</div>
@endsection
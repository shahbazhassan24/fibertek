@extends('frontend.layouts.app')
@section('content')
<div class="container">
    <ul class="list_none pages">
        <li><a href="{{ route('blog') }}">BLOG</a></li>
        @if(isset($pageSlugs['4']) && ($pageSlugs['4']['status'] == '1'))
        <li class="active"><a href="{{ route('page-slug', $pageSlugs['4']['slug']) }}">{{ strtoupper($pageSlugs['4']['title']) }}</a></li>
        @endif
        @if(isset($pageSlugs['5']) && ($pageSlugs['5']['status'] == '1'))
        <li><a href="{{ route('page-slug', $pageSlugs['5']['slug']) }}">{{ strtoupper($pageSlugs['5']['title']) }}</a></li>
        @endif
        @if(isset($pageSlugs['6']) && ($pageSlugs['6']['status'] == '1'))
        <li><a href="{{ route('page-slug', $pageSlugs['6']['slug']) }}">{{ strtoupper($pageSlugs['6']['title']) }}</a></li>
        @endif
    </ul>
    <div class="pages_content">
        <div class="container">
            <aside id="sidebar">
                <?php if($categories->isNotEmpty()){ ?>
                <h2>CATEGORIES</h2>
                <div class="filter_form">
                    <?php foreach ($categories as $category) { ?>
                    <input type="checkbox" id="category-filter{{ $category->id }}" class="hidden category-filter" data-filter = "{{ $category->id }}">
                    <label for="category-filter{{ $category->id }}" class="custom_check">{{ $category->title }}</label>
                    <?php } ?>
                </div>
                <?php } ?>
            </aside>
            <div id="content">
                <?php if($catalogues->isNotEmpty()){ ?>
                <ul class="posts list_none manuals">
                    <?php foreach ($catalogues as $catalogue) { ?>
                    <li class="item <?=implode(' ', preg_filter('/^/', 'category-', $catalogue->categories->pluck('id')->toArray()));?>">
                        <div class="manual_holder">
                            <img src="{{ asset('/images/catalogue/'.$catalogue->id.'/'.$catalogue->image) }}" alt="{{ $catalogue->image_alt }}" class="img-responsive">
                            <div class="txt">
                                <h2>{{ $catalogue->title }}</h2>
                                <p>{!! $catalogue->description !!}</p>
                                @if(!Auth::check())
                                <a href="#" class="pdf_btn open-modal" data-modal="required-login">
                                @else
                                <a href="{{ asset('/documents/catalogue/'.$catalogue->id.'/'.$catalogue->document) }}" class="pdf_btn add-to-downloads" data-type="catalogue" data-document-id="{{ $catalogue->id }}" target="_blank">
                                @endif
                                    <img src="{{ asset('frontend/images/pdf_icon.png') }}" alt="Download PDF"> Download PDF
                                </a>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            <?php }else{ ?>
                <div class="nothing-found">
                    Nothing Found!
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
</div>
@endsection
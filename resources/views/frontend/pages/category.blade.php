@extends('frontend.layouts.app')
@section('content')
<div class="events_area">
    <div class="events_holder">
        <header class="sec_header add">
            {!! $category->description !!}
        </header>
        <div id="content" class="fluid main-category">
            <div class="tab-content">
                <div id="tab1" class="tab active">
                    @if($category->subCategories)
                        @foreach($category->subCategories as $subCategory)
                            @if($subCategory->status)
                    <div class="event">
                        <div class="event_holder">
                            <a href="{{ route('category', $subCategory->slug) }}" class="detail_btn">View Products</a>
                            @if($subCategory->image)
                            <div class="img_holder">
                                <a href="{{ route('category', $subCategory->slug) }}">
                                    <img src="{{ asset('/images/category/'.$subCategory->id.'/'.$subCategory->image) }}" alt="{{ (isset($subCategory->images[0])) ? json_decode($subCategory->images[0]->alt_text)->image : '' }}" class="img-responsive max_width">
                                </a>
                            </div>
                            @endif
                            <div class="txt add {{ (!$subCategory->image) ? 'no-image' : '' }}">
                                <a href="{{ route('category', $subCategory->slug) }}">
                                    <h2>{{ $subCategory->title }}</h2>
                                </a>
                                <p>{!! $subCategory->short_description !!}</p>
                            </div>
                        </div>
                    </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
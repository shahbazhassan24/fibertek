@extends('frontend.layouts.app')
@section('content')
<div class="product_upper">
    <div class="container">
        @if($category->video_link)
        <div class="vid_holder">
            <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="400" height="230" type="text/html" src="{!! Helper::getYoutubeEmbedUrl($category->video_link) !!}?autoplay=0&start=0&end=0" allowfullscreen></iframe>
        </div>
        @elseif($category->demonstration_image)
        <div class="vid_holder">
            <!-- <a href="#"> -->
                <img src="{{ asset('/images/category/'.$category->id.'/'.$category->demonstration_image) }}" alt="{{ (isset($subCategory->images[0])) ? json_decode($subCategory->images[0]->alt_text)->demonstration : '' }}" class="img-responsive">
            <!-- </a> -->
        </div>
        @endif
        <div class="text {{ (!$category->video_link) ? 'no-video' : '' }} {{ (!$category->demonstration_image) ? 'no-image' : '' }}">
            <h2>{{ $category->title }}</h2>
            {!! $category->description !!}
        </div>
    </div>
</div>
@if($category->subCategories->isNotEmpty())
<div class="events_area">
    <div class="events_holder">
        <div id="content" class="fluid main-category">
            <div class="tab-content">
                <div id="tab1" class="tab active">
                    @foreach($category->subCategories as $subCategory)
                        @if($subCategory->status)
                    <div class="event">
                        <div class="event_holder">
                            <a href="{{ route('category', $subCategory->slug) }}" class="detail_btn">View Products</a>
                            @if($subCategory->image)
                            <div class="img_holder">
                                <a href="{{ route('category', $subCategory->slug) }}">
                                    <img src="{{ asset('/images/category/'.$subCategory->id.'/'.$subCategory->image) }}" alt="{{ (isset($subCategory->images[0])) ? json_decode($subCategory->images[0]->alt_text)->image : '' }}" class="img-responsive max_width">
                                </a>
                            </div>
                            @endif
                            <div class="txt add {{ (!$subCategory->image) ? 'no-image' : '' }}">
                                <a href="{{ route('category', $subCategory->slug) }}">
                                    <h2>{{ $subCategory->title }}</h2>
                                </a>
                                <p>{!! $subCategory->short_description !!}</p>
                            </div>
                        </div>
                    </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@if($category->products->isNotEmpty())
<div class="rack_products sub-category">
    <div class="container">
        <div class="rac_head">
            <select name="sort" id="product-sort">
                <option value="">Sort By</option>
                <option value="name-asc">Sort by Name A to Z</option>
                <option value="name-desc">Sort by Name Z to A</option>
                <option value="added-asc">Oldest to Newest</option>
                <option value="added-desc">Newest to Oldest</option>
            </select>
            <strong>Products</strong>
        </div>
        <ul class="products_list list_none">
            @foreach($category->products as $product)
                @if($product->status)
            <li data-time="{{ strtotime($product->created_at) }}">
                @if($product->images->isNotEmpty())
                <a href="{{ route('product', $product->slug) }}">
                    <img src="{{ asset('/images/product/'.$product->id.'/'.$product->images[0]->name) }}" alt="{{ ($product->images[0]->alt_text) ? $product->images[0]->alt_text : Helper::getImageAlt($product->images[0]->name) }}">
                </a>
                @endif
                <strong><a href="{{ route('product', $product->slug) }}">{{ $product->title }}</a></strong>
            </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>
@endif
@endsection
@extends('frontend.layouts.app')
@section('content')
<div class="container blog">
    <div class="text">
        <h1>{{ $post->title }}</h1>
        <span>{{ strtoupper($post->created_at->format('jS M Y')) }}</span>
        @if($post->image)
        <div class="featured-image">
            <img src="{{ asset('/images/post/'.$post->id.'/'.$post->image) }}" alt="{{ $post->title }}">
        </div>
        @endif
        {!! $post->description !!}
    </div>
</div>
@endsection
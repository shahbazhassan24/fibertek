@extends('frontend.layouts.app')
@section('content')
<div class="container">
    <ul class="list_none pages">
        <li class="active"><a href="{{ route('blog') }}">BLOG</a></li>
        @if(isset($pageSlugs['4']) && ($pageSlugs['4']['status'] == '1'))
        <li><a href="{{ route('page-slug', $pageSlugs['4']['slug']) }}">{{ strtoupper($pageSlugs['4']['title']) }}</a></li>
        @endif
        @if(isset($pageSlugs['5']) && ($pageSlugs['5']['status'] == '1'))
        <li><a href="{{ route('page-slug', $pageSlugs['5']['slug']) }}">{{ strtoupper($pageSlugs['5']['title']) }}</a></li>
        @endif
        @if(isset($pageSlugs['6']) && ($pageSlugs['6']['status'] == '1'))
        <li><a href="{{ route('page-slug', $pageSlugs['6']['slug']) }}">{{ strtoupper($pageSlugs['6']['title']) }}</a></li>
        @endif
    </ul>
    <div class="pages_content">
        <div class="container">
            <aside id="sidebar" class="right">
                {{ Form::open(array('route' => array('blog'), 'method' => 'GET')) }}
                    <div class="q_searcher">
                        <input type="text" placeholder="Enter Keyword" name="keyword" value="{{ (isset($_GET['keyword']) && $_GET['keyword'] != "") ? $_GET['keyword'] : '' }}">
                    </div>
                    <div class="filter_form">
                        <h2>FILTERS</h2>
                        <div>
                            <input type="radio" name="filter" id="filter1" class="hidden" value="month" {{ (isset($_GET['filter']) && $_GET['filter'] == "month") ? 'checked' : '' }}>
                            <label for="filter1" class="custom_check">Past Month</label>
                            <input type="radio" name="filter" id="filter2" class="hidden" value="year" {{ (isset($_GET['filter']) && $_GET['filter'] == "year") ? 'checked' : '' }}>
                            <label for="filter2" class="custom_check">Past 12 Months</label>
                            <input type="radio" name="filter" id="filter3" class="hidden" value="3-years" {{ (isset($_GET['filter']) && $_GET['filter'] == "3-years") ? 'checked' : '' }}>
                            <label for="filter3" class="custom_check">Past 3 Years</label>
                            <input type="radio" name="filter" id="filter4" class="hidden" value="5-years" {{ (isset($_GET['filter']) && $_GET['filter'] == "5-years") ? 'checked' : '' }}>
                            <label for="filter4" class="custom_check">Past 5 Years</label>
                        </div>
                        <div class="btns">
                            <input type="reset" value="Clear">
                            <input type="submit" value="Search">
                        </div>
                    </div>
                {{ Form::close() }}
                <?php if($tags->isNotEmpty()){ ?>
                <h2>POPULAR TAGS</h2>
                <ul class="count_list list_none">
                    <?php foreach ($tags as $tag) { ?>
                    <li>
                        <a href="{{ route('blog-tag', $tag->slug) }}">
                        <em>{{ $tag->posts->count() }}</em>
                        <span>{{ $tag->title }}</span>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </aside>
            <div id="content" class="left">
                <div class="masonry blog-posts">
                    <?php if($posts->isNotEmpty()){ ?>
                    <ul class="grid js-masonry posts list_none">
                        <?php foreach ($posts as $post) { ?>
                        <li class="item">
                            <div class="holder">
                                <img src="{{ asset('/images/post/'.$post->id.'/'.$post->image) }}" alt="{{ $post->title }}" class="img-responsive">
                                <div class="post_cap">
                                    <div class="share">
                                        <em><a href="#" class="share-btn"><i class="fa fa-share-alt" aria-hidden="true"></i></a></em>
                                        <div class="share-listing">
                                            <ul class="list_none">
                                                <li><a href="{{ Helper::getSocialShareLink(route('blog-detail', $post->slug )) }}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="{{ Helper::getSocialShareLink(route('blog-detail', $post->slug ), 'twitter') }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                <li><a href="{{ Helper::getSocialShareLink(route('blog-detail', $post->slug ), 'linkedin') }}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                                <li><a href="{{ Helper::getSocialShareLink(route('blog-detail', $post->slug ), 'whatsapp') }}" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                <li><a href="mailto:?Subject={{ $post->title }}&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 {{ route('blog-detail', $post->slug ) }}" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <span>{{ strtoupper($post->created_at->format('jS M Y')) }}</span>
                                    </div>
                                    <a href="{{ route('blog-detail', $post->slug) }}">{{ $post->title }}</a>
                                </div>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                    <?php }else{ ?>
                        <div class="nothing-found">
                            Nothing Found!
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
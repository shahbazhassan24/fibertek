@extends('frontend.layouts.app')
@section('content')
<div class="container">
    <ul class="list_none pages">
        <li><a href="{{ route('blog') }}">BLOG</a></li>
        @if(isset($pageSlugs['4']) && ($pageSlugs['4']['status'] == '1'))
        <li><a href="{{ route('page-slug', $pageSlugs['4']['slug']) }}">{{ strtoupper($pageSlugs['4']['title']) }}</a></li>
        @endif
        @if(isset($pageSlugs['5']) && ($pageSlugs['5']['status'] == '1'))
        <li class="active"><a href="{{ route('page-slug', $pageSlugs['5']['slug']) }}">{{ strtoupper($pageSlugs['5']['title']) }}</a></li>
        @endif
        @if(isset($pageSlugs['6']) && ($pageSlugs['6']['status'] == '1'))
        <li><a href="{{ route('page-slug', $pageSlugs['6']['slug']) }}">{{ strtoupper($pageSlugs['6']['title']) }}</a></li>
        @endif
    </ul>
    <div class="pages_content">
        <div class="container">
            <div id="content" class="center">
                <?php if($certifications->isNotEmpty()){ ?>
                <ul class="posts list_none manuals certifications">
                    <?php foreach ($certifications as $certification) { ?>
                    <li>
                        <div class="manual_holder">
                        @if(!Auth::check())
                            <a href="#" class="pdf_btn open-modal" data-modal="required-login">
                        @else
                            <a href="{{ asset('/documents/certification/'.$certification->id.'/'.$certification->document) }}" class="add-to-downloads" data-type="certification" data-document-id="{{ $certification->id }}" target="_blank">
                        @endif
                                <img src="{{ asset('/images/certification/'.$certification->id.'/'.$certification->image) }}" alt="{{ $certification->image_alt }}" class="img-responsive">
                            </a>
                            <div class="txt">
                                <h2>
                                @if(!Auth::check())
                                    <a href="#" class="open-modal" data-modal="required-login">
                                @else
                                    <a href="{{ asset('/documents/certification/'.$certification->id.'/'.$certification->document) }}" class="add-to-downloads" data-type="certification" data-document-id="{{ $certification->id }}" target="_blank">
                                @endif
                        {{ $certification->title }}</a></h2>
                                <p>{!! $certification->description !!}</p>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <?php }else{ ?>
                <div class="nothing-found">
                    Nothing Found!
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
</div>
@endsection
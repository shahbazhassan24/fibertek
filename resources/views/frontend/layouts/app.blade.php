<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('frontend.includes.head')
</head>
<body>
    <div id="wrapper">
        @include('frontend.includes.header')
        <main id="main">
            @if(isset($banner) && !empty($banner))
                @include('frontend.includes.banner', $banner)
            @endif
            @if(isset($breadcrumbLinks))
                @include('frontend.includes.breadcrumb', $breadcrumbLinks)
            @endif
            @include('frontend.includes.error_messages')
            @yield('content')
        </main>
        @include('frontend.includes.footer')
    </div>
    @include('frontend.includes.popups')
    @include('frontend.includes.scripts')
</body>
</html>
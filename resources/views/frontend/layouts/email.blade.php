<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>FiberTek</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <style type="text/css" media="all">
        body{
            font-family: 'Poppins', sans-serif;
            color: #7c7c7c;
        }
        a {
            text-decoration: none;
        }
        a.underline{
            text-decoration: underline;
        }
        ul {
            list-style: none;
            padding-left: 0px;
        }
        #wrapper{
            width: 650px;
            margin: 0px auto;
            -webkit-box-shadow: 0px 0px 30px -20px #000;
            -moz-box-shadow: 0px 0px 30px -20px #000;
            box-shadow: 0px 0px 30px -20px #000;
        }
        #mail-header{
            background-color: #ffffff;
            padding: 10px 10px;
        }
        .logo {
            width: 140px;
            display: block;
        }
        .logo img {
            width: 100%;
        }
        .site-tagline{
            text-align: right;
            color: #000000;
            font-size: 14px;
        }
        #content {
            margin: 50px 20px;
        }
        #content .main-heading {
            font-size: 24px;
            color: #008ad6;
            padding-bottom: 30px;
            font-weight: 600;
        }
        #content .inner-heading {
            font-size: 18px;
            padding-bottom: 0px;
            display: inline-block;
        }
        #content .section-title {
            margin: 20px 0px;
            display: block;
        }

        #content .section-title span {
            display: inline-block;
            vertical-align: middle;
            background: #ececec;
            border-radius: 30px;
            font-size: 14px;
            color: #3b3f3e;
            font-weight: 700;
            padding: 7px 24px;
            text-transform: uppercase;
        }
        .button-center {
            text-align: center;
        }
        .btn_primary {
            margin-left: 10px;
            padding: 11px 24px;
            display: inline-block;
            vertical-align: top;
            background-color: #01267f;
            font-weight: 600;
            text-transform: uppercase;
            text-align: center;
            border-radius: 30px;
            font-size: 17px;
            color: #fff;
            position: relative;
        }
        a.btn_primary{
            /* margin-top: 50px; */
            display: inline-block;
        }

        #content .information{
            font-size: 14px;
            margin-bottom: 15px;
        }
        .information .row{
            line-height: 20px;
            margin-bottom: 15px;
        }
        .information .row.small-margin{
            margin-bottom: 5px;
        }
        .information strong{
            margin-bottom: 0px;
            display: inline-block;
        }
        .information .row ul.links{
            padding-left: 0px;
        }
        .information .row ul.links li {
            padding-bottom: 5px;
        }
        .information .row ul.links li a {
            color: #0091db;
            font-size: 16px;
        }
        .information .row ul.box.background {
            background-color: #ececec;
            border-radius: 10px;
            padding: 30px;
        }
        .information .row ul.box li {
            padding-bottom: 5px;
            vertical-align: top;
        }
        .information .row ul.box li span{
            width: 50%;
            display: inline-block;
        }
        .information .row ul.box span:first-child{
            font-weight: 600;
        }
        ul.pending-applications {
            margin: 0px 0px 10px 0px;
            padding: 0px;
            border-bottom: 1px solid #e6e6e6;
        }
        ul.pending-applications li {
            display: inline-block;
            width: 340px;
            vertical-align: top;
        }
        ul.pending-applications li:first-child{
            width: 100px;
        }
        ul.pending-applications li:last-child{
            width: 150px;
        }
        ul.pending-applications li .information .row.small-margin {
            margin-bottom: 0px;
        }
        .information .row .status-approved,
        .information .row .status-rejected,
        .information .row .status-queried {
            font-weight: 600;
        }
        .information .row .status-approved {
            color: #2ab766;
        }
        .information .row .status-rejected {
            color: #cd3b48;
        }
        .information .row .status-queried {
            color: #ea7d23;
        }
        .shopping-cart .shopping-cart-items li .cart-img,
        .shopping-cart .shopping-cart-items li .cart-title,
        .shopping-cart .shopping-cart-items li .cart-quantity {
            display: inline-block;
        }
        .shopping-cart .shopping-cart-items li .cart-img {
            width: 130px;
        }
        .shopping-cart .shopping-cart-items li .cart-title {
            width: 250px;
        }
        .shopping-cart .shopping-cart-items li .cart-img img {
            max-height: 50px;
            object-fit: contain;
        }
        .shopping-cart .shopping-cart-items li .cart-title,
        .shopping-cart .shopping-cart-items li .cart-quantity {
            vertical-align: top;
            margin-top: 20px;
            margin-left: 20px;
        }
        .shopping-cart .shopping-cart-items li .cart-title .item-name a {
            color: #222;
        }
        #footer{
            padding: 10px 0px;
            background-color: #ececec;
        }
        .footer {
            font-size: 14px;
        }
        .footer .contact-information {
            display: block;
            width: 100%;
            text-align: center;
        }
        .footer .contact-information ul {
            list-style: none;
        }
        .footer .contact-information ul li {
            display: inline-block;
            padding: 5px;
        }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <table border="0" cellpadding="0" cellspacing="0" width="650" id="emailContainer">
                <tr>
                    <td align="center" valign="top">
                        <div id="mail-header">
                            @include('frontend.includes.email.header')
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <div id="content">
                            <!-- Content Wrapper. Contains page content -->
                            @yield('content')
                            <!-- /.content-wrapper -->
                        </div> 
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <div id="footer">
                            @include('frontend.includes.email.footer')
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <!-- ./wrapper -->
    </body>
</html>

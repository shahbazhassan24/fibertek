<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

// Authentication Routes...
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register')->middleware('mailSettings');

// Password Reset Routes...
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email')->middleware('mailSettings');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('/page/{slug}', array('as' => 'page-slug', 'uses' => 'CmsController@index'));
Route::get('/blog', array('as' => 'blog', 'uses' => 'CmsController@blog'));
Route::get('/blog/{slug}', array('as' => 'blog-detail', 'uses' => 'CmsController@blogDetail'));
Route::get('/blog/tag/{slug}', array('as' => 'blog-tag', 'uses' => 'CmsController@blog'));
Route::get('/project-reference-list/{slug}', array('as' => 'project-reference-detail', 'uses' => 'CmsController@projectReferenceDetail'));
Route::get('/news', array('as' => 'news', 'uses' => 'CmsController@news'));
Route::get('/news/{slug}', array('as' => 'news-detail', 'uses' => 'CmsController@newsDetail'));
Route::get('/category/{slug}', array('as' => 'category', 'uses' => 'CmsController@categoryDetail'));
Route::get('/product/{slug}', array('as' => 'product', 'uses' => 'CmsController@productDetail'));
Route::get('/search', 'CmsController@search')->name('search');

Route::get('/who-we-are', 'HomeController@whoweare')->name('whoweare');

//Ajax Routes
Route::post('subscribe-newsletter', 'CmsController@subscribeNewsletter')->name('subscribe-newsletter')->middleware('mailSettings');
Route::post('contact-enquiry', 'CmsController@contactUsEnquiry')->name('contact-enquiry')->middleware('mailSettings');
Route::post('product-quote', 'CmsController@productQuote')->name('product-quote')->middleware('mailSettings');
Route::post('add-to-downloads', 'CmsController@saveDownloads')->name('add-to-downloads');
Route::post('add-request-a-quote', 'CmsController@addRequestQuote')->name('add-request-a-quote');
Route::post('remove-request-a-quote', 'CmsController@removeRequestQuote')->name('remove-request-a-quote');

//Static Routes
Route::get('/sitemap.xml', 'CmsController@getSitemapContent');
Route::get('/robots.txt', 'CmsController@getRobotsContent');

Route::group(['middleware' => ['auth']], function () {
	Route::get('/dashboard', 'CmsController@dashboard')->name('user-dashboard');
	Route::put('/user-update', 'CmsController@userDetailsUpdate')->name('user-update');
	Route::put('/user-password', 'CmsController@userPasswordUpdate')->name('user-password');
	Route::get('/documents/{type}/{id}/{document}', 'CmsController@getDownloadeableDocuments');
});
// Route::get('/login', 'HomeController@login')->name('user-login');

Route::get('/images/background/{type}/{id}/{img}', 'CmsController@renderResizeImage');
Route::get('/images/background/{main}/{type}/{id}/{img}', 'CmsController@renderResizeImageSub');

Route::namespace('Admin')->group(function () {
    // Controllers Within The "App\Http\Controllers\Admin" Namespace
    Route::prefix('admin')->group(function () {
    	Route::group(['middleware' => ['isAdmin']], function () {
    		Route::get('/dashboard', 'DashboardController@index')->name('admin-dashboard');

    		//Post Tags Routes
			Route::get('/posts/tag', 'PostController@tags')->name('post-tag.index');
			Route::get('/posts/tag/create', 'PostController@createTag')->name('post-tag.create');
			Route::post('/posts/tag', 'PostController@storeTag')->name('post-tag.store');
			Route::get('/posts/tag/{id}', 'PostController@showTag')->name('post-tag.show');
			Route::get('/posts/tag/{id}/edit', 'PostController@editTag')->name('post-tag.edit');
			Route::put('/posts/tag/{id}', 'PostController@updateTag')->name('post-tag.update');
			Route::delete('/posts/tag/{id}', 'PostController@destroyTag')->name('post-tag.destroy');

    		//Career Categories Routes
			Route::get('/career/category', 'CareerController@categories')->name('career-category.index');
			Route::get('/career/category/create', 'CareerController@createCategory')->name('career-category.create');
			Route::post('/career/category', 'CareerController@storeCategory')->name('career-category.store');
			Route::get('/career/category/{id}', 'CareerController@showCategory')->name('career-category.show');
			Route::get('/career/category/{id}/edit', 'CareerController@editCategory')->name('career-category.edit');
			Route::put('/career/category/{id}', 'CareerController@updateCategory')->name('career-category.update');
			Route::delete('/career/category/{id}', 'CareerController@destroyCategory')->name('career-category.destroy');
			//Career Image Routes
			Route::get('/career/image', 'CareerController@images')->name('career-image.index');
			Route::post('/career/image', 'CareerController@storeImage')->name('career-image.store');
			//Career Life@Fibertek Routes
			Route::get('/career/life-fibertek', 'CareerController@lifeFibertek')->name('career-life-fibertek.index');
			Route::get('/career/life-fibertek/create', 'CareerController@createLifeFibertek')->name('career-life-fibertek.create');
			Route::post('/career/life-fibertek', 'CareerController@storeLifeFibertek')->name('career-life-fibertek.store');
			Route::get('/career/life-fibertek/{id}', 'CareerController@showLifeFibertek')->name('career-life-fibertek.show');
			Route::get('/career/life-fibertek/{id}/edit', 'CareerController@editLifeFibertek')->name('career-life-fibertek.edit');
			Route::put('/career/life-fibertek/{id}', 'CareerController@updateLifeFibertek')->name('career-life-fibertek.update');
			Route::delete('/career/life-fibertek/{id}', 'CareerController@destroyLifeFibertek')->name('career-life-fibertek.destroy');

			//Catalogue Category Routes
			Route::get('/catalogues/category', 'CatalogueController@categories')->name('catalogues-category.index');
			Route::get('/catalogues/category/create', 'CatalogueController@createCategory')->name('catalogues-category.create');
			Route::post('/catalogues/category', 'CatalogueController@storeCategory')->name('catalogues-category.store');
			Route::get('/catalogues/category/{id}', 'CatalogueController@showCategory')->name('catalogues-category.show');
			Route::get('/catalogues/category/{id}/edit', 'CatalogueController@editCategory')->name('catalogues-category.edit');
			Route::put('/catalogues/category/{id}', 'CatalogueController@updateCategory')->name('catalogues-category.update');
			Route::delete('/catalogues/category/{id}', 'CatalogueController@destroyCategory')->name('catalogues-category.destroy');

			//Manual Category Routes
			Route::get('/manuals/category', 'ManualController@categories')->name('manuals-category.index');
			Route::get('/manuals/category/create', 'ManualController@createCategory')->name('manuals-category.create');
			Route::post('/manuals/category', 'ManualController@storeCategory')->name('manuals-category.store');
			Route::get('/manuals/category/{id}', 'ManualController@showCategory')->name('manuals-category.show');
			Route::get('/manuals/category/{id}/edit', 'ManualController@editCategory')->name('manuals-category.edit');
			Route::put('/manuals/category/{id}', 'ManualController@updateCategory')->name('manuals-category.update');
			Route::delete('/manuals/category/{id}', 'ManualController@destroyCategory')->name('manuals-category.destroy');

			//News Category Routes
			Route::get('/news/category', 'NewsController@categories')->name('news-category.index');
			Route::get('/news/category/create', 'NewsController@createCategory')->name('news-category.create');
			Route::post('/news/category', 'NewsController@storeCategory')->name('news-category.store');
			Route::get('/news/category/{id}', 'NewsController@showCategory')->name('news-category.show');
			Route::get('/news/category/{id}/edit', 'NewsController@editCategory')->name('news-category.edit');
			Route::put('/news/category/{id}', 'NewsController@updateCategory')->name('news-category.update');
			Route::delete('/news/category/{id}', 'NewsController@destroyCategory')->name('news-category.destroy');

			//Home Section Links
			Route::get('/home/banners', 'BannerController@homeBanners')->name('home-section.banner');

			//Footer Links
			Route::get('/footer-links', 'ConfigurationController@footerLinksEdit')->name('footer-links.index');
			Route::post('/footer-links', 'ConfigurationController@footerLinksUpdate')->name('footer-links.update');

    		Route::resources([
			    'users' => 'UserController',
			    'roles' => 'RoleController',
				'permissions' => 'PermissionController',
				'menus' => 'MenuController',
			    'pages' => 'PageController',
			    'posts' => 'PostController',
			    'categories' => 'CategoryController',
			    'products' => 'ProductController',
			    'catalogues' => 'CatalogueController',
			    'certifications' => 'CertificationController',
			    'manuals' => 'ManualController',
			    'quotes' => 'QuoteController',
			    'banners' => 'BannerController',
			    'seo' => 'SeoController',
			    'news' => 'NewsController',
				'subscriber' => 'SubscriberController',
				'contact-enquiries' => 'ContactEnquiryController',
			    'career' => 'CareerController',
			    'faqs' => 'FaqController',
			    'project-reference' => 'ProjectReferenceController',
			    'home-exlpore-product' => 'HomeExploreProductController',
			    'home-news-highlight' => 'HomeNewsHighlightController',
			]);

			//View User Downloads
			Route::get('/user/{id}/downloads', 'UserController@viewDownloads')->name('users.downloads');
			
			Route::post('/products/import', 'ProductController@importProducts')->name('products.import');

			Route::get('/configurations', 'ConfigurationController@edit')->name('configurations.edit');
			Route::put('/configurations', 'ConfigurationController@update')->name('configurations.update');
			Route::post('/ckeditor/image_upload', 'CKEditorUploadController@upload')->name('ckeditor-upload');
			Route::post('/image_upload', 'MiscellaneousController@upload')->name('image-upload');
			Route::post('/image_remove', 'MiscellaneousController@remove')->name('image-remove');
			//Mailing List Export
			Route::get('/subscriber/downloadExcel/{type}', 'SubscriberController@downloadExcel')->name('export-subscribers');
			//Contact Enquiries Export
			Route::get('/contact-enquiries/downloadExcel/{type}', 'ContactEnquiryController@downloadExcel')->name('export-contact-enquiries');
			//Contact Enquiries Export
			Route::get('/quotes/downloadExcel/{type}', 'QuoteController@downloadExcel')->name('export-quotes');

			Route::get('/apply-watermark', 'ProductController@applyWaterMarkImages')->name('apply-watermark');
    	});
		// Authentication Routes...
		Route::get('/login', 'Auth\LoginController@showLoginForm')->name('admin-login');
		Route::post('/login', 'Auth\LoginController@login');
		Route::post('/logout', 'Auth\LoginController@logout')->name('admin-logout');
    	Route::get('/logout', 'Auth\LoginController@logout')->name('admin-logout');

		// Registration Routes...
		Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('admin-register');
		Route::post('/register', 'Auth\RegisterController@register');

		// Password Reset Routes...
		Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin-password.request');
		Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin-password.email');
		Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin-password.reset');
		Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('admin-password.update');
    });
    //Route::match(['get', 'post'], '/', function () {});
});
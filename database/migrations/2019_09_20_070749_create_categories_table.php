<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->longText('short_description')->nullable();
            $table->longText('description')->nullable();
            $table->longText('banner_description')->nullable();
            $table->string('image')->nullable();
            $table->string('banner')->nullable();
            $table->string('video_link')->nullable();
            $table->integer('sort')->default('0');
            $table->boolean('is_featured')->default('0');
            $table->boolean('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}

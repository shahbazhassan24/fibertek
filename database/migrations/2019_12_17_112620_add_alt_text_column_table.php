<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAltTextColumnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->text('image_alt')->after('image')->nullable();
        });
        Schema::table('catalogues', function (Blueprint $table) {
            $table->text('image_alt')->after('image')->nullable();
        });
        Schema::table('certifications', function (Blueprint $table) {
            $table->text('image_alt')->after('image')->nullable();
        });
        Schema::table('home_explore_products', function (Blueprint $table) {
            $table->text('image_alt')->after('image')->nullable();
        });
        Schema::table('home_news_highlights', function (Blueprint $table) {
            $table->text('image_alt')->after('image')->nullable();
        });
        Schema::table('life_fiberteks', function (Blueprint $table) {
            $table->text('image_alt')->after('image')->nullable();
        });
        Schema::table('manuals', function (Blueprint $table) {
            $table->text('image_alt')->after('image')->nullable();
        });
        Schema::table('menus', function (Blueprint $table) {
            $table->text('image_alt')->after('image')->nullable();
        });
        Schema::table('news', function (Blueprint $table) {
            $table->text('image_alt')->after('image')->nullable();
        });
        Schema::table('pages', function (Blueprint $table) {
            $table->text('banner_alt')->after('banner')->nullable();
        });
        Schema::table('posts', function (Blueprint $table) {
            $table->text('image_alt')->after('image')->nullable();
        });
        Schema::table('project_references', function (Blueprint $table) {
            $table->text('image_alt')->after('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banners', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('catalogues', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('certifications', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('home_explore_products', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('home_news_highlights', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('life_fiberteks', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('manuals', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('menus', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn('banner_alt');
        });
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('project_references', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogueCategoryCatalogueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogue_catalogue_category', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('id');
            $table->unsignedBigInteger('catalogue_category_id');
            $table->unsignedBigInteger('catalogue_id');

            $table->foreign('catalogue_category_id')->references('id')->on('catalogue_categories')->onDelete('cascade');
            $table->foreign('catalogue_id')->references('id')->on('catalogues')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogue_category_catalogue');
    }
}

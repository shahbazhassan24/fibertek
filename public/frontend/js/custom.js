jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var isMobile = window.matchMedia("only screen and (max-width: 1000px)").matches;
var isSmallMobile = false;
if (jQuery(window).width() < 768) {
    isSmallMobile = true;
}
/**
 * Disable right-click of mouse, F12 key, and save key combinations on page
 * By Arthur Gareginyan (arthurgareginyan@gmail.com)
 * For full source code, visit https://mycyberuniverse.com
 */
window.onload = function() {
    document.addEventListener("contextmenu", function(e) {
        e.preventDefault();
    }, false);
    document.addEventListener("keydown", function(e) {
        //document.onkeydown = function(e) {
        // "I" key
        if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
            disabledEvent(e);
        }
        // "J" key
        if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
            disabledEvent(e);
        }
        // "S" key + macOS
        if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
            disabledEvent(e);
        }
        // "U" key
        if (e.ctrlKey && e.keyCode == 85) {
            disabledEvent(e);
        }
        // "F12" key
        if (event.keyCode == 123) {
            disabledEvent(e);
        }
    }, false);

    function disabledEvent(e) {
        if (e.stopPropagation) {
            e.stopPropagation();
        } else if (window.event) {
            window.event.cancelBubble = true;
        }
        e.preventDefault();
        return false;
    }
};
jQuery(document).ready(function() {
    jQuery('.job_opener').click(function(e) {
        e.preventDefault();
        jQuery(this).closest('.job').find('.job_text').slideToggle();
        jQuery('.job').find('job_opener').removeClass('active');
        jQuery(this).toggleClass('active');
    });

    jQuery('#nav .tabs a').click(function(e) {
        if (!isMobile) {
            jQuery('#main .tab-content .tab').hide();
            var cur_tab = jQuery(this).data("tab-link");
            jQuery('#main .tab-content').find(cur_tab).show();
            return true;
        } else {
            e.preventDefault();
        }
    });

    jQuery('#main .tabs a').click(function(e) {
        e.preventDefault();
        jQuery('#main .tabs li').removeClass('active');
        jQuery(this).closest('li').addClass('active');
        var cur_tab = jQuery(this).attr("href");
        if (jQuery(this).data("tab-link"))
            cur_tab = jQuery(this).data("tab-link");
        jQuery('#main .tab-content .tab').hide();
        jQuery('#main .tab-content').find(cur_tab).show();
    });

    jQuery("#header .left_side.tabs li a").hover(function(e) {
        if (isMobile) {
            e.preventDefault();
        }
        jQuery("#header .left_side.tabs li").removeClass('active');
        jQuery('#header .tab-content .tab').hide();
        jQuery(this).parent().addClass("active");
        var cur_tab = jQuery(this).data("tab-link");
        jQuery('#header .tab-content').find(cur_tab).show();
        return false;
    });

    jQuery('.info_opener').click(function(e) {
        e.preventDefault();
        jQuery(this).toggleClass('active');
        jQuery(this).closest('.txt').find('.toggle_content').slideToggle();
    });

    jQuery('.pop_opener').click(function(e) {
        e.preventDefault();

        var cur_popup = jQuery(this).attr('href');
        jQuery(cur_popup).addClass('.active')
    });

    jQuery('.pop_closer').click(function(e) {
        e.preventDefault();
        jQuery('.popup').hide();
    });

    jQuery('.thumb_holder img').click(function() {
        var cur_img = jQuery(this).attr('src');
        jQuery('.full_img').attr('src', cur_img);
    });

    jQuery('.nav_opener').click(function(e) {
        e.preventDefault();
        jQuery('#nav').slideToggle();
    });

    jQuery('#nav ul li:has(ul,div)').addClass('has-drop');

    jQuery('#nav ul>li.has-drop>a').click(function(e) {
        e.preventDefault();
    });

    jQuery('.img_pop_opener').click(function() {
        jQuery('#img_popup').show();
        var demo_img = jQuery('.full_img').attr('src');
        jQuery('.full_image').attr('src', demo_img);
    });

    if ($(window).width() < 768) {
        jQuery('#nav .left_side a').click(function() {
            jQuery(this).closest('.four_cols').find('.menu_holder').addClass('active');
        });
    }
    jQuery('.back_tray').click(function(e) {
        e.preventDefault();
        jQuery('.menu_holder').removeClass('active');
    });

    jQuery(".scroll-to-element").click(function(e) {
        e.preventDefault();
        jQuery('html, body').animate({
                scrollTop: jQuery(jQuery(this).attr('href')).offset().top - 10
            },
            'slow');
    });

    jQuery(".open-modal").click(function(e) {
        e.preventDefault();
        jQuery('#' + jQuery(this).data('modal')).show();
    });

    jQuery(".close-modal").click(function(e) {
        e.preventDefault();
        jQuery('#' + jQuery(this).data('dismis')).hide();
    });

    jQuery(".filter_form .category-filter").click(function(e) {
        jQuery('#content .posts .item').hide();
        jQuery('.filter_form .category-filter').each(function(index, value) {
            if (jQuery(this).is(':checked'))
                jQuery('#content .posts .item.category-' + jQuery(this).data('filter')).show();
        });
        if (jQuery('.filter_form .category-filter:checkbox:checked').length == '0')
            jQuery('#content .posts .item').show();
    });

    jQuery(".share-btn").on('click', function(e) {
        e.preventDefault();
        jQuery(".share-listing").hide();
        jQuery(this).parent().parent().find(".share-listing").toggle();
    });

    //Slider for Product Gallery
    if (jQuery('.sh-p-gallery-main').length > '0') {
        //Set Defaut Big Image
        jQuery('.sh-p-gallery-main .main_image_holder:first-child img').addClass('full_img');
        //Make Sliders
        jQuery('.sh-p-gallery-main').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.sh-p-gallery-thumb'
        });
        jQuery('.sh-p-gallery-thumb').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.sh-p-gallery-main',
            //dots: true,
            centerMode: true,
            centerPadding: "20px",
            focusOnSelect: true,
            prevArrow: '<a href="#" class="slick-prev"><img src="' + appAssetsUrl + 'frontend/images/slider_btn_left.png" alt="<"></a>',
            nextArrow: '<a href="#" class="slick-next"><img src="' + appAssetsUrl + 'frontend/images/slider_btn.png" alt=">"></a>',
            //infinite: false,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            }, {
                breakpoint: 640,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            }, {
                breakpoint: 420,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            }]
        });
        // On after slide change
        // jQuery('.sh-p-gallery-main').on('afterChange', function(event, slick, currentSlide) {
        //     jQuery('.sh-p-gallery-main .main_image_holder.slick-slide img').removeClass('full_img');
        //     jQuery('.sh-p-gallery-main .main_image_holder.slick-slide.slick-current img').addClass('full_img');
        // });
        if (jQuery('.sh-p-gallery-main .main_image_holder.slick-slide').length <= '5') {
            jQuery('.sh-p-gallery-thumb').slick('slickSetOption', 'centerMode', false);
        }
    }

    //Form Validations Start
    jQuery("form#register-form").validate({
        rules: {
            name: { required: true },
            email: { required: true, email: true },
            company_name: { required: true },
            country_id: { required: true },
            password: { required: true, minlength: 8 },
            password_confirmation: { required: true, equalTo: '#password' },
            terms: { required: true, minlength: 1 }
        },
        messages: {
            name: {
                required: "Please enter Name"
            },
            email: {
                required: "Please enter Email Address",
                email: "Enter valid Email Address"
            },
            company_name: {
                required: "Please enter Company Name"
            },
            country_id: {
                required: "Please choose Country"
            },
            password: {
                required: "Please enter Password",
                minlength: "Password min length is {0}"
            },
            password_confirmation: {
                required: "Please enter Confirm Password",
                equalTo: "Confirm Password not same as Password"
            },
            terms: {
                required: "Please select Terms of use"
            }
        },
        errorPlacement: function(error, element) {
            /*here we add the error label to the div that is after the br tag 
            using the next method provided by jquery to navigate on the DOM*/
            error.insertAfter(element.parent());
        },
        // specifying a submitHandler prevents the default submit, good for the demo
        submitHandler: function(form) {
            var response = grecaptcha.getResponse();
            if (response.length == 0) {
                jQuery('.captcha-error-message').addClass('error');
                jQuery('.captcha-error-message').css({ 'display': 'block' });
                return false;
            } else {
                form.submit();
            }
        }
    });
    jQuery("form#login-form").validate({
        rules: {
            email: { required: true, email: true },
            password: { required: true, minlength: 8 }
        },
        messages: {
            email: {
                required: "Please enter Email Address",
                email: "Enter valid Email Address"
            },
            password: {
                required: "Please enter Password",
                minlength: "Password min length is {0}"
            }
        },
        errorPlacement: function(error, element) {
            /*here we add the error label to the div that is after the br tag 
            using the next method provided by jquery to navigate on the DOM*/
            error.insertAfter(element.parent());
        },
        // specifying a submitHandler prevents the default submit, good for the demo
        submitHandler: function(form) {
            form.submit();
        }
    });

    jQuery("form#forgot-password-form").validate({
        rules: {
            email: { required: true, email: true }
        },
        messages: {
            email: {
                required: "Please enter Email Address",
                email: "Enter valid Email Address"
            }
        },
        errorPlacement: function(error, element) {
            /*here we add the error label to the div that is after the br tag 
            using the next method provided by jquery to navigate on the DOM*/
            error.insertAfter(element.parent());
        },
        // specifying a submitHandler prevents the default submit, good for the demo
        submitHandler: function(form) {
            var response = grecaptcha.getResponse();
            if (response.length == 0) {
                jQuery('.captcha-error-message').addClass('error');
                jQuery('.captcha-error-message').css({ 'display': 'block' });
                return false;
            } else {
                form.submit();
            }
        }
    });

    jQuery("form#reset-password-form").validate({
        rules: {
            email: { required: true, email: true },
            password: { required: true, minlength: 8 },
            password_confirmation: { required: true, equalTo: "#password" }
        },
        messages: {
            email: {
                required: "Please enter Email Address",
                email: "Enter valid Email Address"
            },
            password: {
                required: "Please enter Password",
                minlength: "Password min length is {0}"
            },
            password_confirmation: {
                required: "Please enter Confirm Password",
                equalTo: "Confirm Password not same as Password"
            }
        },
        errorPlacement: function(error, element) {
            /*here we add the error label to the div that is after the br tag 
            using the next method provided by jquery to navigate on the DOM*/
            error.insertAfter(element.parent());
        },
        // specifying a submitHandler prevents the default submit, good for the demo
        submitHandler: function(form) {
            form.submit();
        }
    });

    jQuery('form#join-list-form').on('submit', function(e) {
        e.preventDefault();
        var form = jQuery(this);
        jQuery.ajax({
            type: 'POST',
            url: appJSUrl + '/subscribe-newsletter',
            data: form.serialize(),
            dataType: 'json',
            beforeSend: toggleAjaxButton,
            success: function(data) {
                toggleAjaxButton();
                form.find('span.error-message').remove();
                jQuery('.success-message').html(data.message).show().delay(5000).fadeOut();
                form.trigger("reset");
            },
            error: function(err) {
                toggleAjaxButton();
                form.find('span.error-message').remove();
                if (err.status == 422) { // when status code is 422, it's a validation issue        
                    // display errors on each form field
                    jQuery.each(err.responseJSON.errors, function(i, error) {
                        var el = jQuery(document).find('[name="' + i + '"]');
                        el.after(jQuery('<span class="error-message">' + error[0] + '</span>'));
                    });
                }
            }
        });
    });

    jQuery('form#contact-enquiry-form').on('submit', function(e) {
        e.preventDefault();
        var form = jQuery(this);
        var response = grecaptcha.getResponse();
        if (response.length == 0) {
            jQuery('.captcha-error-message').show();
            return false;
        } else {
            jQuery('.captcha-error-message').hide();
        }
        jQuery.ajax({
            type: 'POST',
            url: appJSUrl + '/contact-enquiry',
            data: form.serialize(),
            dataType: 'json',
            beforeSend: toggleAjaxButton,
            success: function(data) {
                toggleAjaxButton();
                form.find('span.error-message').remove();
                jQuery('.success-message').text(data.success).show().delay(5000).fadeOut();
                form.trigger("reset");
                grecaptcha.reset();
            },
            error: function(err) {
                toggleAjaxButton();
                form.find('span.error-message').remove();
                if (err.status == 422) { // when status code is 422, it's a validation issue        
                    // display errors on each form field
                    jQuery.each(err.responseJSON.errors, function(i, error) {
                        var el = jQuery(document).find('[name="' + i + '"]');
                        el.after(jQuery('<span class="error-message">' + error[0] + '</span>'));
                    });
                }
            }
        });
    });


    jQuery('.enquiry-list_wraper .enquiry-list .qty-wrap input[type="number"]').on('change', function(e) {
        jQuery('.enquiry-cart-product-quantity-' + jQuery(this).parent().data('product-id')).val(jQuery(this).val());
    });

    jQuery('form#request-quote-form').on('submit', function(e) {
        e.preventDefault();
        var form = jQuery(this);
        var response = grecaptcha.getResponse();
        if (response.length == 0) {
            jQuery('.captcha-error-message').show();
            return false;
        } else {
            jQuery('.captcha-error-message').hide();
        }
        jQuery.ajax({
            type: 'POST',
            url: appJSUrl + '/product-quote',
            data: form.serialize(),
            dataType: 'json',
            beforeSend: toggleAjaxButton,
            success: function(data) {
                toggleAjaxButton();
                form.find('span.error-message').remove();
                jQuery('.enquiry-cart-page .enquiry-list').html('<li><div class="en-cart-title_wrap no-product-enquire">' + data.success + '</div></li>');
                jQuery('#request-quote-form').remove();
                jQuery('.cart_wrap .cart-count').text('0');
                setTimeout(function() { window.location.href = appJSUrl; }, 3000);
                //jQuery('.success-message').text(data.success).show().delay(5000).fadeOut();
                // form.trigger("reset");
                // grecaptcha.reset();
            },
            error: function(err) {
                toggleAjaxButton();
                form.find('span.error-message').remove();
                if (err.status == 422) { // when status code is 422, it's a validation issue        
                    // display errors on each form field
                    jQuery.each(err.responseJSON.errors, function(i, error) {
                        var el = jQuery(document).find('[name="' + i + '"]');
                        el.after(jQuery('<span class="error-message">' + error[0] + '</span>'));
                    });
                }
            }
        });
    });

    jQuery('.add-to-downloads').on('click', function(e) {
        //e.preventDefault();
        var download = jQuery(this);
        var downloadData = { type: download.data('type'), document_id: download.data('document-id') };
        jQuery.ajax({
            type: 'POST',
            url: appJSUrl + '/add-to-downloads',
            data: downloadData,
            dataType: 'json',
            success: function(data) {},
            error: function(err) {}
        });
    });

    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('#back-to-top-scroll').show();
        } else {
            jQuery('#back-to-top-scroll').hide();
        }
    });
    jQuery('#back-to-top-scroll').click(function() {
        jQuery("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    jQuery('#product-sort').on('change', function(e) {
        var products = jQuery('ul.products_list li');
        if (jQuery(this).val() == 'name-asc') {
            products.sort(function(a, b) {
                return (jQuery(b).find('strong a').text().toUpperCase()) <
                    (jQuery(a).find('strong a').text().toUpperCase()) ? 1 : -1;
            });
        } else if (jQuery(this).val() == 'name-desc') {
            products.sort(function(a, b) {
                return (jQuery(b).find('strong a').text().toUpperCase()) >
                    (jQuery(a).find('strong a').text().toUpperCase()) ? 1 : -1;
            });
        } else if (jQuery(this).val() == 'added-asc') {
            products.sort(function(a, b) {
                return +jQuery(a).data('time') - +jQuery(b).data('time');
            });
        } else if (jQuery(this).val() == 'added-desc') {
            products.sort(function(a, b) {
                return jQuery(b).data("time") - jQuery(a).data("time");
            });
        }
        products.appendTo('ul.products_list');
    });
    if (isSmallMobile) {
        jQuery(".search_form input[type='search']").attr("placeholder", "Search Site");
    }
});

jQuery(document).on('click', function(e) {
    if (isSmallMobile) {
        if (jQuery(e.target).parent().attr('class') != 'nav_opener') {
            if (jQuery(e.target).closest("#nav").length === 0) {
                jQuery("#nav").hide();
            }
        }
    }
});

jQuery(document).on('click', '.add-request-a-quote', function(e) {
    e.preventDefault();
    jQuery(this).find('span').text('Added for Quote');
    jQuery(this).find('img').attr('alt', 'Added for Quote');
    jQuery(this).toggleClass('add-request-a-quote remove-request-a-quote');
    jQuery('.request-a-quote-message').slideToggle('slow').delay(5000).fadeOut();
    jQuery('.request-a-quote-message .request-a-quote-action').text('added to');
    jQuery('.cart_wrap .cart-count').text(parseInt(jQuery('.cart_wrap .cart-count').text()) + 1);
    var productData = { product: jQuery(this).data('product-id') };
    jQuery.ajax({
        type: 'POST',
        url: appJSUrl + '/add-request-a-quote',
        data: productData,
        dataType: 'json',
        success: function(data) {
            jQuery('.shopping-cart').replaceWith(data.html);
        },
        error: function(err) {}
    });
});

jQuery(document).on('click', '.remove-request-a-quote', function(e) {
    e.preventDefault();
    jQuery(this).find('span').text('Request a Quote');
    jQuery(this).find('img').attr('alt', 'Request a Quote');
    if (jQuery(this).parent().parent().hasClass('shopping-cart-items'))
        jQuery(this).parent().remove();
    else if (jQuery(this).parent().parent().parent().hasClass('enquiry-list'))
        jQuery(this).parent().parent().remove();
    jQuery(this).toggleClass('add-request-a-quote remove-request-a-quote');
    jQuery('.request-a-quote-message').slideToggle('slow').delay(5000).fadeOut();

    jQuery('.request-a-quote-message .request-a-quote-action').text('removed from');
    jQuery('.cart_wrap .cart-count').text(parseInt(jQuery('.cart_wrap .cart-count').text()) - 1);
    var productData = { product: jQuery(this).data('product-id') };
    jQuery.ajax({
        type: 'POST',
        url: appJSUrl + '/remove-request-a-quote',
        data: productData,
        dataType: 'json',
        success: function(data) {
            if (jQuery('.enquiry-cart-page .enquiry-list li').length <= '0') {
                jQuery('.enquiry-cart-page .enquiry-list').html('<li><div class="en-cart-title_wrap no-product-enquire">No Product for Request a Quote</div></li>');
                jQuery('#request-quote-form').remove();
            }
            jQuery('.shopping-cart').replaceWith(data.html);
        },
        error: function(err) {}
    });
});

jQuery(document).on('click', '.btn-no-item-request-a-quote', function(e) {
    e.preventDefault();
});

function googleTranslateElementInit() {
    var langWidgetLayout = google.translate.TranslateElement.InlineLayout.SIMPLE;
    if (isSmallMobile) {
        langWidgetLayout = google.translate.TranslateElement.InlineLayout.VERTICAL;
    }
    new google.translate.TranslateElement({
        pageLanguage: 'en',
        layout: langWidgetLayout
    }, 'google_translate_element');
}

// fn to handle button-toggling
var toggleAjaxButton = function() {
    var ajaxSubmitButton = $(".btn-ajax-submit");
    //ajaxSubmitButton.val() == "SEND MESSAGE" ? ajaxSubmitButton.val('SENDING...') : ajaxSubmitButton.val('SEND MESSAGE');
    ajaxSubmitButton.next().toggleClass('hide show');
    ajaxSubmitButton.prop('disabled', function() {
        return !$(this).prop('disabled');
    });
}

// jQuery(window).on('click', function(event){
//     console.log(event);
//     // if (event.target == modal) {
//     //     modal.style.display = "none";
//     //   }
// });
var timmer;
jQuery(document).on("mouseover", '#cart', function() {
    jQuery(".shopping-cart").css({ "display": "block" });
});
jQuery(document).on("mouseleave", '#cart', function() {
    timmer = setTimeout(function() {
        jQuery(".shopping-cart").css({ "display": "none" });
    }, 1500);

});
jQuery(document).on("mouseover", '.shopping-cart', function() {
    clearTimeout(timmer);
    jQuery(".shopping-cart").css({ "display": "block" });
});
jQuery(document).on("mouseleave", '.shopping-cart', function() {
    jQuery(".shopping-cart").css({ "display": "none" });
});
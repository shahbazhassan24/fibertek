<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Models\Country;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('*', function ($view)
        {
            $configurations = Helper::getConfigurations();
            $pageSlugs = Helper::getPageSlugs();
            $mainCategories = Helper::getProductCategories(['status' => '1', 'parent_id' => NULL]);
            $seoInformation = Helper::getSeoInformation(Request()->getPathInfo());
            $countries = Helper::getCountries();
            $view->with(
                [
                    'configuration' => $configurations, 
                    'pageSlugs' => $pageSlugs,
                    'mainCategories' => $mainCategories,
                    'seoInformation' => $seoInformation,
                    'countries' => $countries
                ]
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}

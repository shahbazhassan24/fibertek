<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\NewsCategory;

class News extends Model
{
    protected $fillable = [
        'title', 'slug', 'description', 'country_id', 'start_date', 'end_date', 'image', 'image_alt', 'document', 'status'
    ];

    public function categories()
    {
        return $this->belongsToMany(NewsCategory::class);
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function setTitleAttribute($value) {
        $this->attributes['title'] = $value;
        $slug = Str::slug($value, '-');
        $slugCount = count(Model::whereRaw("(slug = '$slug' or slug LIKE '$slug-%')")->get());
        $this->attributes['slug'] = $slugCount == 0 ? $slug : $slug.'-'.$slugCount;
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class RelatedProduct extends Model
{
    public function products()
    {
        return $this->hasMany(Product::class, 'id', 'related_product_id');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'related_product_id');
    }
}

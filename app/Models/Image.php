<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'type', 'item_id', 'title', 'name', 'alt_text', 'sort', 'status'
    ];
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\Image;
use App\Models\Document;
use App\Models\RelatedProduct;

class Product extends Model
{
	protected $fillable = [
        'title', 'slug', 'short_description', 'description', 'sku', 'price', 'netweight', 'sort', 'is_featured', 'featured_description', 'status'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'item_id')->where(['type' => 'product'])->orderBy('sort', 'ASC');
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'item_id')->where(['type' => 'product']);
    }

    public function linkedProducts()
    {
        return $this->hasMany(RelatedProduct::class, 'product_id');
    }

    public function relatedProducts()
    {
        return $this->belongsToMany(RelatedProduct::class, 'related_products', 'product_id', 'related_product_id');
    }
}
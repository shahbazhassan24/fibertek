<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    /**
     *
     * @var string
     */
    protected $table = 'seo';

    protected $fillable = [
        'url', 'meta_title', 'meta_description', 'meta_keywords'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'type', 'item_id', 'title', 'name', 'description', 'status'
    ];
}

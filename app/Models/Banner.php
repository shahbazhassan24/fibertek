<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
        'type', 'item_id', 'title', 'name', 'image', 'image_alt', 'description', 'status'
    ];
}
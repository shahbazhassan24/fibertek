<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\News;

class NewsCategory extends Model
{
    protected $fillable = [
        'title', 'slug', 'status'
    ];

    public function news()
    {
        return $this->belongsToMany(News::class);
    }

    public function setTitleAttribute($value) {
        $this->attributes['title'] = $value;
        $slug = Str::slug($value, '-');
        $slugCount = count(Model::whereRaw("(slug = '$slug' or slug LIKE '$slug-%')")->get());
        $this->attributes['slug'] = $slugCount == 0 ? $slug : $slug.'-'.$slugCount;
    }
}

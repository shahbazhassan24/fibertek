<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Catalogue;

class CatalogueCategory extends Model
{
    protected $fillable = [
        'title', 'status'
    ];

    public function catalogues()
    {
        return $this->belongsToMany(Catalogue::class);
    }
}
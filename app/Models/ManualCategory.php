<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Manual;

class ManualCategory extends Model
{
    protected $fillable = [
        'title', 'status'
    ];

    public function manuals()
    {
        return $this->belongsToMany(Manual::class);
    }
}

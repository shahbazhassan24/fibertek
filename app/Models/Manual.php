<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ManualCategory;

class Manual extends Model
{
    protected $fillable = [
        'title', 'slug', 'description', 'image', 'image_alt', 'document', 'status'
    ];

    public function categories()
    {
        return $this->belongsToMany(ManualCategory::class);
    }
}

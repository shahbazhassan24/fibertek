<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Quote extends Model
{
    protected $fillable = [
        'products', 'name', 'company', 'email', 'phone', 'message', 'status'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}

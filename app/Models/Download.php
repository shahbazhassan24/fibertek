<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
	protected $fillable = [
        'user_id', 'type', 'document_id'
    ];

    public function catalogue()
    {
        return $this->hasOne(Catalogue::class, 'id', 'document_id');
    }

    public function manual()
    {
        return $this->hasOne(Manual::class, 'id', 'document_id');
    }

    public function certification()
    {
        return $this->hasOne(Certification::class, 'id', 'document_id');
    }

    public function project_reference()
    {
        return $this->hasOne(ProjectReference::class, 'id', 'document_id');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'document_id');
    }
}
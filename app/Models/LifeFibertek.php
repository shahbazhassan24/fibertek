<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LifeFibertek extends Model
{
    protected $fillable = [
        'title', 'description', 'image', 'image_alt', 'status'
    ];
}
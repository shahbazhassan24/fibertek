<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CatalogueCategory;

class Catalogue extends Model
{
    protected $fillable = [
        'title', 'slug', 'description', 'image', 'image_alt', 'document', 'status'
    ];

    public function categories()
    {
        return $this->belongsToMany(CatalogueCategory::class);
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeNewsHighlight extends Model
{
    protected $fillable = [
        'title', 'url', 'label', 'image', 'image_alt', 'sort', 'status'
    ];
}
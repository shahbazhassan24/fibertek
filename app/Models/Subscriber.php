<?php

namespace App\Models;
use App\Models\Country;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $fillable = [
        'name', 'company', 'email', 'how', 'how_source', 'country_id', 'status'
    ];

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }
}
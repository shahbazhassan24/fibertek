<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\Tag;

class Post extends Model
{
	protected $fillable = [
        'title', 'slug', 'description', 'image', 'image_alt', 'status'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function setTitleAttribute($value) {
        $this->attributes['title'] = $value;
        $slug = Str::slug($value, '-');
        $slugCount = count(Model::whereRaw("(slug = '$slug' or slug LIKE '$slug-%')")->get());
        $this->attributes['slug'] = $slugCount == 0 ? $slug : $slug.'-'.$slugCount;
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Career;

class CareerCategory extends Model
{
	protected $fillable = [
        'title', 'status'
    ];

    public function careers()
    {
        return $this->belongsToMany(Career::class);
    }
}
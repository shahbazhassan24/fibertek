<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    protected $fillable = [
        'title', 'slug', 'description', 'image', 'image_alt', 'document', 'status'
    ];
}
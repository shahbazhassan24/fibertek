<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ProjectReference extends Model
{
    protected $fillable = [
        'parent_id', 'title', 'slug', 'description', 'projects', 'image', 'image_alt', 'sort', 'status'
    ];

    public function setTitleAttribute($value) {
        $this->attributes['title'] = $value;
        $slug = Str::slug($value, '-');
        $slugCount = count(Model::whereRaw("(slug = '$slug' or slug LIKE '$slug-%')")->get());
        $this->attributes['slug'] = $slugCount == 0 ? $slug : $slug.'-'.$slugCount;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

	protected $fillable = [
        'parent_id', 'title', 'url', 'short_description', 'image', 'image_alt', 'new_tab', 'sort', 'status'
    ];

    /**
	 * Get the sub menus for the category.
	 */
	public function subMenus()
	{
	    return $this->hasMany(Menu::class, 'parent_id')->orderby('sort', 'asc');
	}

	/**
	 * Get the parent category that owns the category.
	 */
	public function parent()
	{
	    return $this->belongsTo(Menu::class);
	}

	public function getParentsAttribute()
	{
	    $parents = collect([]);
	    $parent = $this->parent;
	    while(!is_null($parent)) {
	        $parents->push($parent);
	        $parent = $parent->parent;
	    }
	    return $parents;
	}
}
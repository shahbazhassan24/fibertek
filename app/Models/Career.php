<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CareerCategory;
use App\Models\Image;
use App\Models\Country;

class Career extends Model
{

	protected $fillable = [
        'title', 'job_description', 'qualification', 'country_id', 'status'
    ];

    public function categories()
    {
        return $this->belongsToMany(CareerCategory::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'item_id')->where(['type' => 'career']);
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }
}
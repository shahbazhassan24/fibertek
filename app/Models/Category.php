<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Models\Image;
use App\Models\Document;

class Category extends Model
{

	protected $fillable = [
        'parent_id', 'title', 'slug', 'short_description', 'description', 'banner_description', 'image', 'banner', 'video_link', 'demonstration_image', 'sort', 'is_featured', 'status'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class)->orderby('sort', 'asc');
    }

    /**
	 * Get the sub categories for the category.
	 */
	public function subCategories()
	{
	    return $this->hasMany(Category::class, 'parent_id')->orderby('sort', 'asc');
	}

	/**
	 * Get the parent category that owns the category.
	 */
	public function parent()
	{
	    return $this->belongsTo(Category::class);
	}

	public function images()
    {
        return $this->hasMany(Image::class, 'item_id')->where(['type' => 'category'])->orderBy('sort', 'ASC');
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'item_id');
    }

	public function getParentsAttribute()
	{
	    $parents = collect([]);
	    $parent = $this->parent;
	    while(!is_null($parent)) {
	        $parents->push($parent);
	        $parent = $parent->parent;
	    }
	    return $parents;
	}
}
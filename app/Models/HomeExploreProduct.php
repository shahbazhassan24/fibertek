<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeExploreProduct extends Model
{
    protected $fillable = [
        'title', 'url', 'image', 'image_alt', 'sort', 'status'
    ];
}
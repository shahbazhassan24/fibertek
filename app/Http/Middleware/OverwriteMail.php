<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Mail\TransportManager;
use Illuminate\Support\Facades\Crypt;

use Closure;
use Mail;
use Config;
use App;

use App\Helpers\Helper;

class OverwriteMail
{

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Mail Configurations
        $emailConfigurations = [
            'driver' => 'smtp',
            'host' => Helper::getConfigurationByKey('smtp_host'),
            'port' => Helper::getConfigurationByKey('smtp_port'),
            'encryption' => 'tls',
            'username' => Helper::getConfigurationByKey('smtp_user'),
            'password' => Crypt::decryptString(Helper::getConfigurationByKey('smtp_password')),
            'from' => [
                'address' => Helper::getConfigurationByKey('email_from'),
                'name' => 'FiberTek'
            ] 
        ];
        
        Config::set('mail', $emailConfigurations);

        $app = App::getInstance();
        $app->register('Illuminate\Mail\MailServiceProvider');

        return $next($request);
    }
}

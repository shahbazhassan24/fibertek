<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ClearanceMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {        
        if (Auth::user()->hasPermissionTo('Administrator')) //If user has this //permission 
        {
            return $next($request);
        }

        if ($request->is('/*/create'))
         {
            echo 'hello1';
            if (!Auth::user()->hasPermissionTo('Create')) {
                return redirect()->route('admin-dashboard');
            }
        }

        if ($request->is('/*/*/edit')) //If user is editing a post
         {
            echo 'hello2';
            if (!Auth::user()->hasPermissionTo('Edit')) {
                return redirect()->route('admin-dashboard');
            }
        }

        if ($request->isMethod('Delete')) //If user is deleting a post
        {
            echo 'hello3';
            if (!Auth::user()->hasPermissionTo('Delete')) {
                return redirect()->route('admin-dashboard');
            }
        }
        echo 'hello4';exit;
        return $next($request);
    }
}
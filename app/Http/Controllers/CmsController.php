<?php

namespace App\Http\Controllers;

use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

use App\Mail\SendMail;

use App\Models\Country;
use App\Models\Banner as AppBanner;
use App\Models\Image as AppImage;
use App\Models\Document as AppDocument;

use App\Models\Page;
use App\Models\Post;
use App\Models\Tag;
use App\Models\Career;
use App\Models\CareerCategory;
use App\Models\LifeFibertek;
use App\Models\ProjectReference;
use App\Models\Catalogue;
use App\Models\CatalogueCategory;
use App\Models\Certification;
use App\Models\Manual;
use App\Models\ManualCategory;
use App\Models\FAQ;
use App\Models\News;
use App\Models\NewsCategory;
use App\Models\Subscriber;
use App\Models\ContactEnquiry;
use App\Models\Quote;

use App\Models\Product;
use App\Models\Category;
use App\Models\RelatedProduct;
use App\Models\Download;
use App\Models\User;


//Helper
use App\Helpers\Helper;

class CmsController extends Controller
{
    protected $pageImagePath, $postImagePath, $projectReferenceImagePath, $catalogueImagePath, $certificationImagePath, $manualImagePath, $productImagePath, $categoryImagePath, $newsImagePath; 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->pageImagePath = asset('/images/page/');
        $this->postImagePath = asset('/images/post/');
        $this->projectReferenceImagePath = asset('/images/project-reference/');
        $this->catalogueImagePath = asset('/images/catalogue/');
        $this->certificationImagePath = asset('/images/certification/');
        $this->manualImagePath = asset('/images/manual/');
        $this->productImagePath = asset('/images/product/');
        $this->categoryImagePath = asset('/images/category/');
        $this->newsImagePath = asset('/images/news/');  
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, $slug)
    {
        if($slug){
            $page = Page::where(['slug' => $slug])->first();
            if(!$page){
                return redirect()->route('home')->with('error', 'Page, not found');
            }
            if($page){
                switch($page->id){
                    case "1":
                        return $this->aboutUs($request, $page);
                    break;
                    case "2":
                        return $this->careers($request, $page);
                    break;
                    case "3":
                        return $this->projectReference($request, $page);
                    break;
                    case "4":
                        return $this->catalogues($request, $page);
                    break;
                    case "5":
                        return $this->certifications($request, $page);
                    break;
                    case "6":
                        return $this->manuals($request, $page);
                    break;
                    case "7":
                        return $this->faqs($request, $page);
                    break;
                    case "10":
                        return $this->contactus($request, $page);
                    break;
                    case "13":
                        return $this->enquiryCart($request, $page);
                    break;
                    default:
                        return $this->cmsPage($request, $page);
                    break;
                }
            }else{
                return redirect()->route('home');
            }
        }else{
            return redirect()->route('home');
        }       
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutUs(Request $request, $page) {
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = ['0' => ['title' => $page->title]];
        return view('frontend.pages.who-we-are', compact('page', 'banner', 'breadcrumbLinks'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function careers(Request $request, $page) {
        $jobs = Career::with(['categories', 'country'])->where(['status' => '1'])->get();
        $images = AppImage::where(['type' => 'career', 'status' => '1'])->get();
        $lifeFibertek = LifeFibertek::where(['status' => '1'])->get();
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = ['0' => ['title' => $page->title]];
        return view('frontend.pages.career', compact('page', 'jobs', 'images', 'lifeFibertek', 'banner', 'breadcrumbLinks'));
    }

    public function projectReference(Request $request, $page)
    {
        $projectReferences = ProjectReference::where(['status' => '1'])->get();
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = ['0' => ['title' => $page->title]];
        return view('frontend.pages.project-reference', compact('page', 'projectReferences', 'banner', 'breadcrumbLinks'));
    }

    public function projectReferenceDetail(Request $request, $slug)
    {
        $page = Page::where(['id' => '3'])->first();
        $projectReference = ProjectReference::where(['slug' => $slug])->first();
        if($projectReference->projects){
            $projects = json_decode($projectReference->projects, true);
            $projectReference->projectsCount = count($projects['year']);      
        }
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $projectReference->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = [
            '0' => ['url' => route('page-slug', $page->slug), 'title' => $page->title],
            '1' => ['title' => $projectReference->title]];
        return view('frontend.pages.project-reference-subpage', compact('page', 'projectReference', 'banner', 'breadcrumbLinks'));
    }

    public function blog(Request $request, $slug = false)
    {
        $tags = Tag::withCount('posts')->latest('posts_count')->orderby('title', 'asc')->take(3)->with('posts')->get();
        $page = Page::where(['id' => '8'])->first();
        $posts = Post::where(['status' => '1']);
        //Set Banner Options
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        //Set Breadcrumb
        $breadcrumbLinks = ['0' => ['title' => $page->title]];

        if($request->segment(2) == 'tag' && $slug){
            $tag = Tag::whereSlug($slug)->first();
            $posts->whereHas('tags', function($q) use ($request, $tag) {
                $q->where('tag_id', $tag->id);
            });
            //Set Tag Breadcrumb
            $breadcrumbLinks = [
                '0' => ['url' => route('blog'), 'title' => $page->title],
                '1' => ['title' => $tag->title]];
        }
        $keyword = $request->input('keyword');
        $filter = $request->input('filter');
        if($keyword || $filter){
            $posts->where(function ($q) use ($request, $keyword, $filter){
                $q->orWhere('title','LIKE',"%{$keyword}%")
                    ->orWhere('description','LIKE',"%{$keyword}%");
            });
            if($filter == 'month'){
                $duration = Carbon::now()->subMonth();
            }elseif($filter == 'year'){
                $duration = Carbon::now()->subYear();
            }elseif($filter == '3-years'){
                $duration = Carbon::now()->subYears(3);
            }elseif($filter == '5-years'){
                $duration = Carbon::now()->subYears(5);
            }
            if(isset($duration))
                $posts->latest()->whereDate('created_at', '>', $duration);
        }
        $posts = $posts->get();
        return view('frontend.pages.blogs', compact('page', 'posts', 'tags', 'banner', 'breadcrumbLinks'));
    }

    public function blogDetail(Request $request, $slug)
    {
        $page = Page::where(['id' => '8'])->first();
        $post = Post::whereSlug($slug)->first();
        if(!$post){
            return redirect()->route('home')->with('error', 'Page, not found');
        }
        //Set Banner Options
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        //Set Tag Breadcrumb
        $breadcrumbLinks = [
            '0' => ['url' => route('blog'), 'title' => $page->title],
            '1' => ['title' => $post->title]];
        return view('frontend.pages.blog-details', compact('page', 'post', 'banner', 'breadcrumbLinks'));
    }

    public function catalogues(Request $request, $page)
    {
        $catalogues = Catalogue::with(['categories'])->where(['status' => '1'])->get();
        $categories = CatalogueCategory::where(['status' => '1'])->get();
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = ['0' => ['title' => $page->title]];
        return view('frontend.pages.catalogues', compact('page', 'catalogues', 'categories', 'banner', 'breadcrumbLinks'));
    }

    public function certifications(Request $request, $page)
    {
        $certifications = Certification::where(['status' => '1'])->orderby('title', 'asc')->get();
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = ['0' => ['title' => $page->title]];
        return view('frontend.pages.certifications', compact('page', 'certifications', 'banner', 'breadcrumbLinks'));
    }

    public function manuals(Request $request, $page)
    {
        $manuals = Manual::with(['categories'])->where(['status' => '1'])->get();
        $categories = ManualCategory::where(['status' => '1'])->get();
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = ['0' => ['title' => $page->title]];
        return view('frontend.pages.manuals', compact('page', 'manuals', 'categories', 'banner', 'breadcrumbLinks'));
    }
    
    public function news(Request $request)
    {
        $page = Page::where(['id' => '9'])->first();
        $news = News::with(['categories', 'country'])->where(['status' => '1'])->get();
        $categories = NewsCategory::where(['status' => '1'])->get();
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = ['0' => ['title' => $page->title]];
        return view('frontend.pages.news-events', compact('page', 'news', 'categories', 'banner', 'breadcrumbLinks'));
    }

    public function newsDetail(Request $request, $slug)
    {
        $page = Page::where(['id' => '9'])->first();
        $news = News::whereSlug($slug)->first();
        if(!$news){
            return redirect()->route('home')->with('error', 'Page, not found');
        }
        //Set Banner Options
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        //Set Tag Breadcrumb
        $breadcrumbLinks = [
            '0' => ['url' => route('news'), 'title' => $page->title],
            '1' => ['title' => $news->title]];
        return view('frontend.pages.news-details', compact('page', 'news', 'banner', 'breadcrumbLinks'));
    }

    public function contactus()
    {
        $page = Page::where(['id' => '10'])->first();
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = ['0' => ['title' => $page->title]];
        return view('frontend.pages.contact-us', compact('page', 'banner', 'breadcrumbLinks'));
    }

    public function faqs(Request $request, $page)
    {
        $search = '';
        $faqQuery = FAQ::where(['status' => '1']);
        if($request->search){
            $search = $request->search;
            $faqQuery->where(function($q) use ($search) {
                $q->where('question', 'LIKE', '%'.$search.'%')
                  ->orWhere('answer', 'LIKE', '%'.$search.'%');
            });
        }
        $faqs = $faqQuery->orderby('sort', 'asc')->get();
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = ['0' => ['title' => $page->title]];
        return view('frontend.pages.faqs', compact('page', 'faqs', 'search', 'banner', 'breadcrumbLinks'));
    }

    public function cmsPage(Request $request, $page)
    {
        $banner = [];
        if($page->banner)
            $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = ['0' => ['title' => $page->title]];
        return view('frontend.pages.page', compact('page', 'banner', 'breadcrumbLinks'));
    }

    public function categoryDetail(Request $request, $slug)
    {
        $category = Category::where('status', '=', '1')->whereSlug($slug)->first();
        if(!$category){
            return redirect()->route('home')->with('error', 'Page, not found');
        }
        $parents = collect([]);
        if($category->parent_id)
            $parents = $category->getParentsAttribute()->reverse();
        $loadView = '';
        $banner = [];
        if($parents->isEmpty()) {
            //Set Banner Options
            $banner = ['banner' => $this->categoryImagePath.'/'.$category->id.'/'.$category->banner, 'title' => $category->title, 'banner_description' => $category->banner_description, 'category_banner' => true];
            if(isset($category->images[0])){
                $banner['banner_alt'] = json_decode($category->images[0]->alt_text)->banner;
            }
        }
        //Set Tag Breadcrumb
        $breadcrumbLinks = [];
        if($parents->isNotEmpty()) {
            foreach ($parents as $parent) {
                $breadcrumbLinks[] = ['url' => route('category', $parent->slug), 'title' => $parent->title];
            }
            $loadView = '-sub';
        }
        $breadcrumbLinks[count($breadcrumbLinks)] = ['title' => $category->title];
        return view('frontend.pages.category'.$loadView, compact('category', 'parents', 'banner', 'breadcrumbLinks'));
    }

    public function productDetail(Request $request, $slug)
    {
        $product = Product::with(['linkedProducts.product'])->where('status', '=', '1')->whereSlug($slug)->first();
        if(!$product){
            return redirect()->route('home')->with('error', 'Page, not found');
        }
        // $relatedProducts = Product::whereHas('categories', function($query) use ($product) {
        //     $query->whereIn('category_id', $product->categories->pluck('id')->toArray());
        // })->get();

        $parents = collect([]);
        if($product->categories[0]->parent_id)
            $parents = $product->categories[0]->getParentsAttribute()->reverse();
        //Set Tag Breadcrumb
        $breadcrumbLinks = [];
        if($parents->isNotEmpty()) {
            foreach ($parents as $parent) {
                $breadcrumbLinks[] = ['url' => route('category', $parent->slug), 'title' => $parent->title];
            }
        }
        $breadcrumbLinks[count($breadcrumbLinks)] = ['url' => route('category', $product->categories[0]->slug), 'title' => $product->categories[0]->title];
        $breadcrumbLinks[count($breadcrumbLinks)] = ['title' => $product->title];
        return view('frontend.pages.product-details', compact('product', 'parents', 'breadcrumbLinks'));
    }

    public function search(Request $request)
    {
        $term = $request->search;

        //Search Pages
        $pages = Page::where('title', 'LIKE', '%'.$term.'%')
        ->orWhere('content', 'LIKE', '%'.$term.'%')
        ->skip(0)->take(10)->get();

        //Search Post
        $posts = Post::where('title', 'LIKE', '%'.$term.'%')
        ->orWhere('description', 'LIKE', '%'.$term.'%')
        ->skip(0)->take(10)->get();

        //Search Catalogue
        $catalogue = Catalogue::where('title', 'LIKE', '%'.$term.'%')
        ->orWhere('description', 'LIKE', '%'.$term.'%')
        ->first();

        //Search Certification
        $certification = Certification::where('title', 'LIKE', '%'.$term.'%')
        ->orWhere('description', 'LIKE', '%'.$term.'%')
        ->first();

        //Search Manual
        $manual = Manual::where('title', 'LIKE', '%'.$term.'%')
        ->orWhere('description', 'LIKE', '%'.$term.'%')
        ->first();

        //Search Project Reference
        $projectReference = ProjectReference::where('title', 'LIKE', '%'.$term.'%')
        ->orWhere('description', 'LIKE', '%'.$term.'%')
        ->skip(0)->take(10)->get();

        //Search Faq
        $faq = FAQ::where('question', 'LIKE', '%'.$term.'%')
        ->orWhere('answer', 'LIKE', '%'.$term.'%')
        ->first();

        //Search News
        $news = News::where('title', 'LIKE', '%'.$term.'%')
        ->orWhere('description', 'LIKE', '%'.$term.'%')
        ->skip(0)->take(10)->get();

        //Search Career
        $career = Career::where('title', 'LIKE', '%'.$term.'%')
        ->orWhere('job_description', 'LIKE', '%'.$term.'%')
        ->orWhere('qualification', 'LIKE', '%'.$term.'%')
        ->first();

        //Search Products
        $products = Product::where('title', 'LIKE', '%'.$term.'%')
        ->orWhere('short_description', 'LIKE', '%'.$term.'%')
        ->orWhere('description', 'LIKE', '%'.$term.'%')
        ->where('status', '=', '1')
        ->skip(0)->take(10)->get();

        //Search Categories
        
        $categories = Category::whereNotNull('parent_id')->where(function ($searchQuery) use ($term){
            $searchQuery->orWhere('title', 'LIKE', '%'.$term.'%')
            ->orWhere('short_description', 'LIKE', '%'.$term.'%')
            ->orWhere('description', 'LIKE', '%'.$term.'%')
            ->where('status', '=', '1');
        })->skip(0)->take(10)->get();

        //For Cms Pages Links
        $cmsPages = [];
        if(
            $career || 
            $projectReference ||
            $manual ||
            $certification ||
            $catalogue ||
            $faq
        ) {
            $cmsPagesArray = Page::whereIn('id', ['2', '3', '4', '5', '6', '7'])->get();
            if($cmsPagesArray->isNotEmpty()){
                foreach($cmsPagesArray as $cmsPage){
                    $cmsPages[$cmsPage->id] = ['slug' => $cmsPage->slug, 'title' => $cmsPage->title];
                }
            }
        }

        //dd($cmsPages);
        $banner = ['banner' => asset('/images/search-page-banner.jpg'), 'title' => 'Search: "'.$term.'"'];

        return view('frontend.pages.search', compact('pages', 'posts', 'catalogue', 'certification', 'manual', 'projectReference', 'news', 'career', 'faq', 'products', 'categories', 'cmsPages', 'banner'));
    }

    public function enquiryCart(Request $request)
    {
        $page = Page::where(['id' => '13'])->first();
        $banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title, 'banner_alt' => $page->banner_alt];
        $breadcrumbLinks = ['0' => ['title' => $page->title]];
        return view('frontend.pages.enquiry-cart', compact('page', 'banner', 'breadcrumbLinks'));
    }

    public function dashboard(Request $request)
    {
        $downloads = Download::with(['catalogue', 'manual', 'certification', 'project_reference'])->where(['user_id' => Auth::user()->id])->get();
        $user = User::findOrFail(Auth::user()->id);
        $countries = Country::orderby('title', 'asc')->get();
        //$banner = ['banner' => $this->pageImagePath.'/'.$page->id.'/'.$page->banner, 'title' => $page->title];
        $banner = [];
        //dd($downloads);
        //Set Breadcrumb
        $breadcrumbLinks = ['0' => ['title' => 'Dashboard']];
        return view('frontend.pages.user.dashboard', compact('user', 'downloads', 'countries', 'banner', 'breadcrumbLinks'));
    }

    public function userDetailsUpdate(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users,email,'.Auth::user()->id,
            'company_name'=>'required',
            //'password'=>'required|min:6|confirmed'
        ]);

        $user = User::findOrFail(Auth::user()->id); //Get role specified by id
        $input = $request->only(['name', 'email', 'company_name', 'country_id']);
        $user->fill($input)->save();
        return redirect()->back()
            ->with('success_message',
             'Information successfully update.');
    }

    public function userPasswordUpdate(Request $request)
    {

        $this->validate($request, [
            'old_password'=>'required',
            'password'=>'required|confirmed',
            'password_confirmation'=>'required'
            //'password'=>'required|min:6|confirmed'
        ]);

        $user = User::findOrFail(Auth::user()->id); //Get role specified by id
        if (Hash::check($request->old_password, $user->password)) {
            $input = $request->only(['password']);
            $user->fill($input)->save();
            return redirect()->back()
                ->with('success_message',
                'Password successfully update.');
        }

        return redirect()->back()
                ->with('error_message',
                'Old Password is not correct. Try again with correct old password');
    }

    public function subscribeNewsletter(Request $request)
    {
        //Validating title and body field
        $this->validate($request, [
                'name' => 'required',
                //'company' => 'required',
                'email' => 'required|email|unique:subscribers,email',
                'country_id' => 'required',
            ],
            [
                'email.unique' => 'This email address already existed in Subscribers List.'
            ]);
            
            $data['name'] = $request['name'];
            $data['email'] = $request['email'];
            $data['company'] = $request['company'];
            $data['country_id'] = $request['country_id'];
            $data['how'] = (isset($request['how']) ? $request['how'] : NULL);
            $data['how_source'] = (isset($request['how_source']) ? $request['how_source'] : NULL);
            $data['status'] = '1';
            $subscriber = Subscriber::create($data);

            $emailData['data'] = $data;

            //Subscriber Email to Admin
            $emailData['view'] = 'mailing_list_admin';
            $emailData['subject'] = 'New Subscriber to FiberTek’s mailing list';
            Mail::to(Helper::getConfigurationByKey('email_enquiry'))->send(new SendMail($emailData));

            $emailData['view'] = 'mailing_list';
            $emailData['subject'] = 'Thank You For Subscribing to FiberTek’s mailing list';
            Mail::to($request['email'])->send(new SendMail($emailData));

            return response()->json(['success' => 'Successfully Subscribed!']);
    }

    public function contactUsEnquiry(Request $request)
    {
        //Validating title and body field
        $this->validate($request, [
            'name' => 'required',
            'enquiry' => 'required',
            'email' => 'required|email',
            'how_hear' => 'required',
            'g-recaptcha-response' => 'recaptcha'
        ]);

        $data['name'] = $request['name'];
        $data['email'] = $request['email'];
        $data['company'] = $request['company'];
        $data['phone'] = $request['phone'];
        $data['enquiry'] = $request['enquiry'];
        $data['hear'] = (isset($request['how_hear']) ? $request['how_hear'] : '');

        ContactEnquiry::create($data);

        $emailData['data'] = $data;
        //Enquiry Email to Admin
        $emailData['view'] = 'contact_enquiry';
        $emailData['subject'] = 'Contact Us Enquiry!';
        Mail::to(Helper::getConfigurationByKey('email_enquiry'))->send(new SendMail($emailData));

        //Enquiry Email to User
        $emailData['view'] = 'contact_enquiry_user';
        $emailData['subject'] = 'Thank You For Contacting FiberTek';
        Mail::to($request['email'])->send(new SendMail($emailData));

        return response()->json(['success' => 'Successfully Sent!']);
    }

    public function productQuote(Request $request)
    {
        //Validating title and body field
        $this->validate($request, [
            'name' => 'required',
            'enquiry' => 'required',
            'email' => 'required|email',
            //'company' => 'required',
            'g-recaptcha-response' => 'recaptcha'
        ]);

        //$productsForQuote = $request->session()->get('request_quote_products');
        $productsForQuote = $request['quoteProduct']['product'];

        $data['name'] = $request['name'];
        $data['email'] = $request['email'];
        $data['company'] = $request['company'];
        $data['products'] = json_encode($productsForQuote);
        $data['phone'] = $request['phone'];
        $data['message'] = $request['enquiry'];
        $data['status'] = '1';
        $quote = Quote::create($data);

        $data['productsForQuote'] = $productsForQuote;

        $emailData['data'] = $data;
        //Product Quote Email to Admin
        $emailData['view'] = 'product_quote';
        $emailData['subject'] = 'Product Quote!';
        Mail::to(Helper::getConfigurationByKey('email_enquiry'))->send(new SendMail($emailData));

        //Product Quote Email to User
        $emailData['view'] = 'product_quote_user';
        $emailData['subject'] = 'Thank You for Requesting a Quote at FiberTek';
        Mail::to($request['email'])->send(new SendMail($emailData));

        $request->session()->forget('request_quote_products');

        return response()->json(['success' => 'Thank you for your interest in our products. We will respond to your request within 3 business days.']);
    }

    public function saveDownloads(Request $request)
    {
        $data['user_id'] = Auth::user()->id;
        $data['type'] = $request['type'];
        $data['document_id'] = $request['document_id'];
        $download = Download::create($data);
    }

    public function addRequestQuote(Request $request)
    {
        $productsForQuote = $request->session()->get('request_quote_products');
        $product = Product::where('id', $request->product)->first();
        $productsForQuote[$product->id] = ['id' => $product->id, 'title' => $product->title, 'slug' => $product->slug, 'image' => $product->images[0]->name, 'image_alt' => $product->images[0]->alt_text];
        $request->session()->put('request_quote_products', $productsForQuote);
        $returnHTML = view('frontend.ajax.request-a-quote')->with(['quoteProducts' => $productsForQuote])->render();
        return response()->json(array('html' => $returnHTML)); 
    }

    public function removeRequestQuote(Request $request)
    {
        $productsForQuote = $request->session()->get('request_quote_products');
        unset($productsForQuote[$request->product]);
        $request->session()->put('request_quote_products', $productsForQuote);
        $returnHTML = view('frontend.ajax.request-a-quote')->with(['quoteProducts' => $productsForQuote])->render();
        return response()->json(array('html' => $returnHTML)); 
    }

    public function renderResizeImage(Request $request, $type, $id, $img, $h=5, $w=5)
    {
        return \Image::make(public_path("/images/$type/$id/$img"))->resize($h, $w)->response('jpg'); 
    }
    public function renderResizeImageSub(Request $request, $main, $type, $id, $img, $h=5, $w=5)
    {
        return \Image::make(public_path("/images/$main/$type/$id/$img"))->resize($h, $w)->response('jpg'); 
    }
    public function getDownloadeableDocuments(Request $request, $type, $id, $document)
    {
        $documentPath = public_path("/documents/$type/$id/$document");
        if( ! \File::exists($documentPath)){
            return redirect()->route('home');
        }
        $mimeType = mime_content_type($documentPath);
        $headers = array(
            'Content-Type' => $mimeType,
            'Content-Disposition' => 'inline; filename="'.$document.'"'
        );
        return Response::make(file_get_contents($documentPath), 200, $headers);
    }
    public function getSitemapContent(Request $request)
    {
        if(file_exists(public_path() . '/static'.$request->getPathInfo()))
            return response(file_get_contents(public_path() . '/static'.$request->getPathInfo()), 200, [
                'Content-Type' => 'application/xml'
            ]);
    }
    public function getRobotsContent(Request $request)
    {
        if(file_exists(public_path() . '/static'.$request->getPathInfo()))
            return \File::get(public_path() . '/static'.$request->getPathInfo());
    }
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ProjectReference;

use App\Helpers\Helper;

class ProjectReferenceController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $items = ProjectReference::all();
        return view('admin.pages.project_reference.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.project_reference.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title
        $this->validate($request, [
            'title'=>'required'
            ]);

        $data['title'] = $request['title'];
        $data['description'] = $request['description'];
        $data['projects'] = json_encode($request['projects']);
        $data['sort'] = (!empty($request['sort']) ? $request['sort'] : '0');
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['image_alt'] = $request['image_alt'];

        $projectReference = ProjectReference::create($data);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/project-reference/'.$projectReference->id.'/'), $image);
            $projectReference->image = $image;
            $projectReference->save();
        }

        if(!empty($request['document'])){
            $document = $request['document']->getClientOriginalName();
            $request['document']->move( public_path('/documents/project-reference/'.$projectReference->id.'/'), $document);
            $projectReference->document = $document;
            $projectReference->save();
        }

        //Display a successful message upon save
        return redirect()->route('project-reference.index')
            ->with('success', 'Project Reference,
             '. $projectReference->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $projectReference = ProjectReference::findOrFail($id); //Find category of id = $id
        return view ('admin.pages.project_reference.show', compact('projectReference'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $projectReference = ProjectReference::findOrFail($id);
        if($projectReference->projects){
            $projects = json_decode($projectReference->projects, true);
            $projectReference->projectsCount = count($projects['year']);      
        }
        return view('admin.pages.project_reference.edit', compact('projectReference'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //Validating title
        $this->validate($request, [
            'title'=>'required'
            ]);

        $projectReference = ProjectReference::findOrFail($id);

        $projectReference->title = $request['title'];
        $projectReference->slug = $request['slug'];
        $projectReference->description = $request['description'];
        $projectReference->projects = json_encode($request['projects']);
        $projectReference->sort = (!empty($request['sort']) ? $request['sort'] : '0');
        $projectReference->status = (isset($request['status']) ? $request['status'] : '0');
        $projectReference->image_alt = $request['image_alt'];

        if(!empty($request['image'])){
            if($projectReference->image){
                if(file_exists(public_path('/images/project-reference/'.$projectReference->id.'/').$projectReference->image))
                    unlink(public_path('/images/project-reference/'.$projectReference->id.'/').$projectReference->image);
            }
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/project-reference/'.$projectReference->id.'/'), $image);
            $projectReference->image = $image;
        }

        if(!empty($request['document'])){
            if($projectReference->document){
                if(file_exists(public_path('/documents/project-reference/'.$projectReference->id.'/').$projectReference->document))
                    unlink(public_path('/documents/project-reference/'.$projectReference->id.'/').$projectReference->document);
            }
            $document = $request['document']->getClientOriginalName();
            $request['document']->move( public_path('/documents/project-reference/'.$projectReference->id.'/'), $document);
            $projectReference->document = $document;
        }
        $projectReference->save();

        return redirect()->route('project-reference.index')->with('success', 
            'Project Reference, '. $projectReference->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $projectReference = ProjectReference::findOrFail($id);
        if($projectReference->image){
            $imagesDirectory = public_path('/images/project-reference/'.$projectReference->id.'/');
            if(file_exists($imagesDirectory.$projectReference->image))
                unlink($imagesDirectory.$projectReference->image);
            //Remove Images Directory
            Helper::deleteDirectory($$imagesDirectory);
        }
        if($projectReference->document){
            $documentDirectory = public_path('/documents/project-reference/'.$projectReference->id.'/');
            if(file_exists($documentDirectory.$projectReference->document))
                unlink($documentDirectory.$projectReference->document);
            //Remove Document Directory
            Helper::deleteDirectory($documentDirectory);
        }
        $projectReference->delete();

        return redirect()->route('project-reference.index')
            ->with('success',
             'Project Reference successfully deleted');
    }
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

use App\Models\Career;
use App\Models\CareerCategory;
use App\Models\Country;
use App\Models\LifeFibertek;
use App\Models\Image as AppImage;

class CareerController extends Controller
{
    public $leftMainActive;

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
        $this->leftMainActive = 'career';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $careers = Career::all();
        return view('admin.pages.career.index', compact('careers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = CareerCategory::where(['status' => '1'])->get();
        $countries = Country::orderby('title', 'asc')->get();
        return view('admin.pages.career.create', compact('categories', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title' => 'required',
            'categories' => 'required',
            'country_id' => 'required'
            ]);
        $data['title'] = $request['title'];
        $data['job_description'] = $request['job_description'];
        $data['qualification'] = $request['qualification'];
        $data['country_id'] = $request['country_id'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $career = Career::create($data);

        if(!empty($request['categories']))
            $career->categories()->sync($request['categories']);

        //Display a successful message upon save
        return redirect()->route('career.index')
            ->with('success', 'Career,
             '. $career->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $career = Career::findOrFail($id); //Find seo of id = $id
        return view ('admin.pages.career.show', compact('career'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $career = Career::with(['categories'])->findOrFail($id);
        $categories = CareerCategory::where(['status' => '1'])->get();
        $countries = Country::orderby('title', 'asc')->get();
        return view('admin.pages.career.edit', compact('career', 'categories', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
            'categories' => 'required',
            'country_id' => 'required'
            ]);

        $career = Career::findOrFail($id);
        $career->title = $request['title'];
        $career->job_description = $request['job_description'];
        $career->qualification = $request['qualification'];
        $career->country_id = $request['country_id'];
        $career->status = (isset($request['status']) ? $request['status'] : '0');
        $career->save();

        if(!empty($request['categories']))
            $career->categories()->sync($request['categories']);

        return redirect()->route('career.index')->with('success', 
            'Career, '. $career->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $career = Career::findOrFail($id);
        $career->delete();

        return redirect()->route('career.index')
            ->with('success',
             'Career successfully deleted');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories(Request $request) {
        $categories = CareerCategory::all();
        return view('admin.pages.career.index_category', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCategory() {
        return view('admin.pages.career.create_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCategory(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title' => 'required'
            ]);
        $data['title'] = $request['title'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $careerCategory = CareerCategory::create($data);

        //Display a successful message upon save
        return redirect()->route('career-category.index')
            ->with('success', 'Career Category,
             '. $careerCategory->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCategory($id) {
        $careerCategory = CareerCategory::findOrFail($id); //Find seo of id = $id
        return view ('admin.pages.career.show_category', compact('careerCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editCategory($id) {
        $careerCategory = CareerCategory::findOrFail($id);
        return view('admin.pages.career.edit_category', compact('careerCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCategory(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required'
        ]);

        $careerCategory = CareerCategory::findOrFail($id);
        $careerCategory->title = $request['title'];
        $careerCategory->status = (isset($request['status']) ? $request['status'] : '0');
        $careerCategory->save();

        return redirect()->route('career-category.index')->with('success', 
            'Career Category, '. $careerCategory->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyCategory($id) {
        $careerCategory = CareerCategory::findOrFail($id);
        $careerCategory->delete();

        return redirect()->route('career-category.index')
            ->with('success',
             'Career Category successfully deleted');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function images(Request $request) {
        $images = AppImage::where(['type' => 'career'])->get();
        return view('admin.pages.career.create_image', compact('images'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeImage(Request $request) { 

        //Validating title and body field
        // $this->validate($request, [
        //     'images' => 'required'
        //     ]);

        //Remove images if removed
        if(isset($request['remove_images']) && !empty($request['remove_images'])){
            $imagesDirectory = public_path('/images/career/');
            foreach ($request['remove_images'] as $removeImage) {
                if(file_exists($imagesDirectory.$removeImage)){
                    //Remove Product Image
                    unlink($imagesDirectory.$removeImage);
                    AppImage::where(['name' => $removeImage, 'type' => 'career'])->delete();
                }
            }
        }

        if(!empty($request['images'])){
            $images = [];
            foreach ($request['images'] as $imageKey => $image) {
                $imageName = $image->getClientOriginalName();
                $image->move( public_path('/images/career/'), $imageName);
                $images[$imageKey]['type'] = 'career';
                $images[$imageKey]['item_id'] = '0';
                $images[$imageKey]['title'] = $image->getClientOriginalName();
                $images[$imageKey]['name'] = $imageName;
                $images[$imageKey]['status'] = '1';
                $images[$imageKey]['created_at'] = Carbon::now()->format('Y-m-d h:i:s');
                $images[$imageKey]['updated_at'] = Carbon::now()->format('Y-m-d h:i:s');
            }
            if(!empty($images))
                AppImage::insert($images);           
        }

        //Display a successful message upon save
        return redirect()->route('career-image.index')
            ->with('success', 'Career Images,
             Updated');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lifeFibertek(Request $request) {
        $items = LifeFibertek::all();
        return view('admin.pages.career.index_life_fibertek', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createLifeFibertek() {
        return view('admin.pages.career.create_life_fibertek');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLifeFibertek(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title' => 'required'
            ]);

        $data['title'] = $request['title'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['description'] = $request['description'];

        $lifeFibertek = LifeFibertek::create($data);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/career/lifeFibertek/'), $image);
            $lifeFibertek->image = $image;
            $lifeFibertek->save();
        }

        //Display a successful message upon save
        return redirect()->route('career-life-fibertek.index')
            ->with('success', 'Life @ Fibertek,
             '. $lifeFibertek->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showLifeFibertek($id) {
        $lifeFibertek = LifeFibertek::findOrFail($id); //Find seo of id = $id
        return view ('admin.pages.career.show_life_fibertek', compact('lifeFibertek'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editLifeFibertek($id) {
        $lifeFibertek = LifeFibertek::findOrFail($id);
        return view('admin.pages.career.edit_life_fibertek', compact('lifeFibertek'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateLifeFibertek(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required'
        ]);

        $lifeFibertek = LifeFibertek::findOrFail($id);

        $lifeFibertek->title = $request['title'];
        $lifeFibertek->status = (isset($request['status']) ? $request['status'] : '0');
        $lifeFibertek->description = $request['description'];

        if($request->image){
            if($lifeFibertek->image){
                if(file_exists(public_path('/images/career/lifeFibertek/').$lifeFibertek->image))
                    unlink(public_path('/images/career/lifeFibertek/').$lifeFibertek->image);
            }
            $image = $request->image->getClientOriginalName();
            $request->image->move( public_path('/images/career/lifeFibertek/'), $image);
            $lifeFibertek->image = $image;
        }
        $lifeFibertek->save();

        return redirect()->route('career-life-fibertek.index')->with('success', 
            'Life @ Fibertek, '. $lifeFibertek->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyLifeFibertek($id) {
        $lifeFibertek = LifeFibertek::findOrFail($id);
        if($lifeFibertek->image){
            if(file_exists(public_path('/images/career/lifeFibertek/').$lifeFibertek->image))
                unlink(public_path('/images/career/lifeFibertek/').$lifeFibertek->image);
        }
        $lifeFibertek->delete();

        return redirect()->route('career-life-fibertek.index')
            ->with('success',
             'Life @ Fibertek successfully deleted');
    }
}
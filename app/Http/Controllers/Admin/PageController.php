<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Page;

use App\Helpers\Helper;

class PageController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = Page::orderby('id', 'desc')->get();
        return view('admin.pages.page.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title'=>'required'
            ]);

        $data['title'] = $request['title'];
        $data['content'] = $request['content'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['banner_alt'] = $request['banner_alt'];

        $page = Page::create($data);

        if(!empty($request['banner'])){
            $banner = $request['banner']->getClientOriginalName();
            $request['banner']->move( public_path('/images/page/'.$page->id.'/'), $banner);
            $page->banner = $banner;
            $page->save();
        }

        //Display a successful message upon save
        return redirect()->route('pages.index')
            ->with('success', 'Page,
             '. $page->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $page = Page::findOrFail($id); //Find post of id = $id
        return view ('admin.pages.page.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $page = Page::findOrFail($id);
        return view('admin.pages.page.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required'
        ]);

        $this->validate($request, [
            'title'=>'required'
            ]);

        $page = Page::findOrFail($id);
        $page->title = $request['title'];
        $page->slug = $request['slug'];
        $page->content = $request['content'];       
        $page->status = (isset($request['status']) ? $request['status'] : '0');
        $page->banner_alt = $request['banner_alt'];

        if(!empty($request['banner'])){
            if($page->banner){
                if(file_exists(public_path('/images/page/'.$page->id.'/').$page->image))
                    unlink(public_path('/images/page/'.$page->id.'/').$page->image);
            }
            $banner = $request['banner']->getClientOriginalName();
            $request['banner']->move( public_path('/images/page/'.$page->id.'/'), $banner);
            $page->banner = $banner;
        }

        if($request['extras']){
            $extrasContent = $savedContent = [];
            if($page->extras)
                $savedContent = json_decode($page->extras, true);
            foreach($request['extras']['description'] as $descKey => $descValue){ 
                $savedContent[$descKey]['description'] = $descValue;
            }
            if(isset($request['extras']['image'])){
                foreach($request['extras']['image'] as $imageKey => $imageValue){
                    $image = $imageValue->getClientOriginalName();
                    $imageValue->move( public_path('/images/page/'.$page->id.'/extra/'), $image);
                    $savedContent[$imageKey]['image'] = $image;
                }
            }
            $page->extras = json_encode($savedContent);
        }

        $page->save();

        return redirect()->route('pages.index')->with('success', 
            'Page, '. $page->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // $page = Page::findOrFail($id);
        // if($page->image){
        //     $imagesDirectory = public_path('/images/page/'.$page->id.'/');
        //     if(file_exists($imagesDirectory.$page->image))
        //         unlink($imagesDirectory.$page->image);
        //     //Remove Product Directory
        //     Helper::deleteDirectory($imagesDirectory);
        // }
        // $page->delete();

        return redirect()->route('pages.index')
            ->with('success',
             'Cannot delete page');
    }
}

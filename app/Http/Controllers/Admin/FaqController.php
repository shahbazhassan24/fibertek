<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\FAQ;

use App\Helpers\Helper;


class FaqController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index() {
        $items = FAQ::orderby('id', 'desc')->get();
        return view('admin.pages.faq.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'question'=>'required'
            ]);

        $data['question'] = $request['question'];
        $data['answer'] = $request['answer'];
        $data['sort'] = (isset($request['sort']) ? $request['sort'] : '0');
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');

        $faq = FAQ::create($data);

        //Display a successful message upon save
        return redirect()->route('faqs.index')
            ->with('success', 'FAQ,
             '. $faq->question.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $faq = FAQ::findOrFail($id); //Find post of id = $id
        return view ('admin.pages.faq.show', compact('faq'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $faq = FAQ::findOrFail($id);
        return view('admin.pages.faq.edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'question'=>'required'
        ]);

        $faq = FAQ::findOrFail($id);
        $faq->question = $request['question'];
        $faq->answer = $request['answer'];
        $faq->sort = $request['sort'];
        $faq->status = (isset($request['status']) ? $request['status'] : '0');
        $faq->save();

        return redirect()->route('faqs.index')->with('success', 
            'FAQ, '. $faq->question.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $faq = FAQ::findOrFail($id);
        $faq->delete();

        return redirect()->route('faqs.index')
            ->with('success',
             'FAQ successfully deleted');
    }
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Banner;

use App\Helpers\Helper;

class BannerController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = Banner::where('type', '!=', 'home')->orderby('id', 'desc')->get();
        return view('admin.pages.banner.index', compact('items'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function homeBanners() {
        $items = Banner::where('type', '=', 'home')->orderby('id', 'desc')->get();
        return view('admin.pages.banner.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title'=>'required',
            'image'=>'required|image'
            ]);

        $data['title'] = $request['title'];
        $data['type'] = $request['type'];
        $data['item_id'] = (isset($request['item_id']) ? $request['item_id'] : '0');
        $data['description'] = $request['description'];
        $data['sort'] = (isset($request['sort']) ? $request['sort'] : '0');
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['image_alt'] = $request['image_alt'];

        $banner = Banner::create($data);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/banner/'.$request['type'].'/'.$banner->id.'/'), $image);
            $banner->image = $image;
            $banner->save();
        }

        //Display a successful message upon save
        return redirect()->back()
            ->with('success', 'Banner,
             '. $banner->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $banner = Banner::findOrFail($id); //Find post of id = $id
        return view ('admin.pages.banner.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $banner = Banner::findOrFail($id);
        return view('admin.pages.banner.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required'
        ]);

        $this->validate($request, [
            'title'=>'required',
            'image'=>'sometimes|required|image'
            ]);

        $banner = Banner::findOrFail($id);
        $banner->title = $request['title'];
        $banner->type = $request['type'];
        $banner->item_id = (isset($request['item_id']) ? $request['item_id'] : '0');
        $banner->description = $request['description'];      
        $banner->sort = (isset($request['sort']) ? $request['sort'] : '0'); 
        $banner->status = (isset($request['status']) ? $request['status'] : '0');
        $banner->image_alt = $request['image_alt'];

        if(!empty($request['image'])){
            if($banner->image){
                if(file_exists(public_path('/images/banner/'.$request['type'].'/'.$banner->id.'/').$banner->image))
                    unlink(public_path('/images/banner/'.$request['type'].'/'.$banner->id.'/').$banner->image);
            }
            $image = $request['image']->getClientOriginalName();
            $request['banner']->move( public_path('/images/banner/'.$request['type'].'/'.$banner->id.'/'), $image);
            $banner->image = $image;
        }

        $banner->save();

        return redirect()->back()->with('success', 
            'Banner, '. $banner->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $banner = Banner::findOrFail($id);
        if($banner->image){
            $imagesDirectory = public_path('/images/banner/'.$banner->type.'/'.$banner->id.'/');
            if(file_exists($imagesDirectory.$banner->image))
                unlink($imagesDirectory.$banner->image);
            //Remove Product Directory
            Helper::deleteDirectory($imagesDirectory);
        }
        $banner->delete();

        return redirect()->route('banners.index')
            ->with('success',
             'Banner deleted Successfully!');
    }
}

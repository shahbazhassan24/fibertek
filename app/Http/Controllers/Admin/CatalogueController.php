<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Catalogue;
use App\Models\CatalogueCategory;

use App\Helpers\Helper;

class CatalogueController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index() {
        $items = Catalogue::orderby('id', 'desc')->get();
        return view('admin.pages.catalogue.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = CatalogueCategory::where(['status' => '1'])->get();
        return view('admin.pages.catalogue.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title'=>'required',
            'categories'=>'required'
            ]);

        $data['title'] = $request['title'];
        $slug = Helper::parseSaveURLString($request['title']);
        $data['slug'] = $slug;
        $data['description'] = $request['description'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['image_alt'] = $request['image_alt'];

        $catalogue = Catalogue::create($data);

        if(!empty($request['categories']))
            $catalogue->categories()->sync($request['categories']);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/catalogue/'.$catalogue->id.'/'), $image);
            $catalogue->image = $image;
            $catalogue->save();
        }

        if(!empty($request['document'])){
            $document = $request['document']->getClientOriginalName();
            $request['document']->move( public_path('/documents/catalogue/'.$catalogue->id.'/'), $document);
            $catalogue->document = $document;
            $catalogue->save();
        }

        //Display a successful message upon save
        return redirect()->route('catalogues.index')
            ->with('success', 'Catalogue,
             '. $catalogue->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $catalogue = Catalogue::findOrFail($id); //Find post of id = $id
        return view ('admin.pages.catalogue.show', compact('catalogue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $catalogue = Catalogue::with(['categories'])->findOrFail($id);
        $categories = CatalogueCategory::where(['status' => '1'])->get();
        return view('admin.pages.catalogue.edit', compact('catalogue', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required',
            'categories'=>'required'
        ]);

        $catalogue = Catalogue::findOrFail($id);
        $catalogue->title = $request['title'];
        $slug = Helper::parseSaveURLString($request['title']);
        $catalogue->slug = $slug;
        $catalogue->description = $request['description'];       
        $catalogue->status = (isset($request['status']) ? $request['status'] : '0');
        $catalogue->image_alt = $request['image_alt'];

        if(!empty($request['categories']))
            $catalogue->categories()->sync($request['categories']);

        if(!empty($request['image'])){
            if($catalogue->image){
                if(file_exists(public_path('/images/catalogue/'.$catalogue->id.'/').$catalogue->image))
                    unlink(public_path('/images/catalogue/'.$catalogue->id.'/').$catalogue->image);
            }
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/catalogue/'.$catalogue->id.'/'), $image);
            $catalogue->image = $image;
        }

        if(!empty($request['document'])){
            if($catalogue->document){
                if(file_exists(public_path('/documents/catalogue/'.$catalogue->id.'/').$catalogue->document))
                    unlink(public_path('/documents/catalogue/'.$catalogue->id.'/').$catalogue->document);
            }
            $document = $request['document']->getClientOriginalName();
            $request['document']->move( public_path('/documents/catalogue/'.$catalogue->id.'/'), $document);
            $catalogue->document = $document;
        }

        $catalogue->save();

        return redirect()->route('catalogues.index')->with('success', 
            'Catalogue, '. $catalogue->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $catalogue = Catalogue::findOrFail($id);
        if($catalogue->image){
            $imagesDirectory = public_path('/images/catalogue/'.$catalogue->id.'/');
            if(file_exists($imagesDirectory.$catalogue->image))
                unlink($imagesDirectory.$catalogue->image);
            //Remove Product Directory
            Helper::deleteDirectory($imagesDirectory);
        }
        if($catalogue->document){
            $documentDirectory = public_path('/documents/catalogue/'.$catalogue->id.'/');
            if(file_exists($documentDirectory.$catalogue->document))
                unlink($documentDirectory.$catalogue->document);
            //Remove Product Directory
            Helper::deleteDirectory($documentDirectory);
        }
        $catalogue->delete();

        return redirect()->route('catalogues.index')
            ->with('success',
             'Catalogue successfully deleted');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories(Request $request) {
        $items = CatalogueCategory::all();
        return view('admin.pages.catalogue.index_category', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCategory() {
        return view('admin.pages.catalogue.create_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCategory(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title' => 'required'
            ]);
        $data['title'] = $request['title'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $catalogueCategory = CatalogueCategory::create($data);

        //Display a successful message upon save
        return redirect()->route('catalogues-category.index')
            ->with('success', 'Catalogue Category,
             '. $catalogueCategory->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCategory($id) {
        $catalogueCategory = CatalogueCategory::findOrFail($id); //Find seo of id = $id
        return view ('admin.pages.catalogue.show_category', compact('tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editCategory($id) {
        $catalogueCategory = CatalogueCategory::findOrFail($id);
        return view('admin.pages.catalogue.edit_category', compact('catalogueCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCategory(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required'
        ]);

        $catalogueCategory = CatalogueCategory::findOrFail($id);
        $catalogueCategory->title = $request['title'];
        $catalogueCategory->status = (isset($request['status']) ? $request['status'] : '0');
        $catalogueCategory->save();

        return redirect()->route('catalogues-category.index')->with('success', 
            'Catalogue Category, '. $catalogueCategory->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyCategory($id) {
        $catalogueCategory = CatalogueCategory::findOrFail($id);
        $catalogueCategory->delete();

        return redirect()->route('catalogues-category.index')
            ->with('success',
             'Catalogue Category successfully deleted');
    }
}
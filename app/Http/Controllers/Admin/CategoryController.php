<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

use App\Models\Category;

use App\Models\Image as AppImage;
use App\Helpers\Helper;

class CategoryController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $catQuery = Category::with(['products']);
        if($request->sub){
            $catQuery->where('parent_id', $request->sub);
        }else{
            $catQuery->where('parent_id', NULL);
        }
        $categories = $catQuery->orderby('sort', 'asc')->paginate(20);
        return view('admin.pages.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $allCategories = Category::all();
        return view('admin.pages.category.create', compact('allCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title
        $this->validate($request, [
            'title'=>'required'
            ]);

        $data['title'] = $request['title'];
        $data['parent_id'] = $request['parent_id'];
        $slug = Helper::parseSaveURLString($data['title']);
        if($request['parent_id'])
            $slug = Category::find($request['parent_id'])->slug.'-'.$slug;
        $data['slug'] = $slug;
        $data['sort'] = (isset($request['sort']) ? $request['sort'] : '0');
        $data['is_featured'] = (isset($request['is_featured']) ? $request['is_featured'] : '0');
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['short_description'] = $request['short_description'];
        $data['description'] = $request['description'];
        $data['banner_description'] = $request['banner_description'];
        $data['video_link'] = $request['video_link'];

        $category = Category::create($data);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/category/'.$category->id.'/'), $image);
            $category->image = $image;
            $category->save();
        }

        if(!empty($request['demonstration_image'])){
            $demonstrationImage = $request['demonstration_image']->getClientOriginalName();
            $request['demonstration_image']->move( public_path('/images/category/'.$category->id.'/'), $demonstrationImage);
            $category->demonstration_image = $demonstrationImage;
            $category->save();
        }

        if(!empty($request['banner'])){
            $banner = $request['banner']->getClientOriginalName();
            $request['banner']->move( public_path('/images/category/'.$category->id.'/'), $banner);
            $category->banner = $banner;
            $category->save();
        }

        $categoryImage['type'] = 'category';
        $categoryImage['item_id'] = $category->id;
        $productImages['title'] = '';
        $productImages['name'] = '';
        $categoryImage['alt_text'] = json_encode([
            'image' => $request->image_alt,
            'banner' => $request->banner_alt, 
            'demonstration' => $request->demonstration_image_alt
            ]);
        $categoryImage['status'] = '1';
        $categoryImage['created_at'] = Carbon::now()->format('Y-m-d h:i:s');
        $categoryImage['updated_at'] = Carbon::now()->format('Y-m-d h:i:s');

        if(!empty($categoryImage))
            AppImage::insert($categoryImage);

        //Display a successful message upon save
        $subCat = [];
        if(isset($_GET['sub']))
            $subCat = ['sub' => $_GET['sub']];
        return redirect()->route('categories.index', $subCat)
            ->with('success', 'Category,
             '. $category->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $category = Category::findOrFail($id); //Find category of id = $id
        return view ('admin.pages.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $category = Category::findOrFail($id);
        $allCategories = Category::all();
        return view('admin.pages.category.edit', compact('category', 'allCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //Validating title
        $this->validate($request, [
            'title'=>'required|max:100'
            ]);

        $category = Category::findOrFail($id);

        $category->title = $request['title'];
        $category->parent_id = $request['parent_id'];
        $slug = Helper::parseSaveURLString($request['title']);
        if($request['parent_id'])
            $slug = Category::find($request['parent_id'])->slug.'-'.$slug;
        $category->slug = $slug;
        $category->sort = $request['sort'];
        $category->is_featured = (isset($request['is_featured']) ? $request['is_featured'] : '0');
        $category->status = (isset($request['status']) ? $request['status'] : '0');
        $category->short_description = $request['short_description'];
        $category->description = $request['description'];
        $category->banner_description = $request['banner_description'];
        $category->video_link = $request['video_link'];

        if(!empty($request['image'])){
            if($category->image){
                if(file_exists(public_path('/images/category/'.$category->id.'/').$category->image))
                    unlink(public_path('/images/category/'.$category->id.'/').$category->image);
            }
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/category/'.$category->id.'/'), $image);
            $category->image = $image;
        }

        if(!empty($request['demonstration_image'])){
            if($category->demonstration_image){
                if(file_exists(public_path('/images/category/'.$category->id.'/').$category->demonstration_image))
                    unlink(public_path('/images/category/'.$category->id.'/').$category->demonstration_image);
            }
            $demonstrationImage = $request['demonstration_image']->getClientOriginalName();
            $request['demonstration_image']->move( public_path('/images/category/'.$category->id.'/'), $demonstrationImage);
            $category->demonstration_image = $demonstrationImage;
        }

        if(!empty($request['banner'])){
            if($category->banner){
                if(file_exists(public_path('/images/category/'.$category->id.'/').$category->banner))
                    unlink(public_path('/images/category/'.$category->id.'/').$category->banner);
            }
            $banner = $request['banner']->getClientOriginalName();
            $request['banner']->move( public_path('/images/category/'.$category->id.'/'), $banner);
            $category->banner = $banner;
        }

        $category->save();

        if($category->images->isNotEmpty()){
            $categoryImage['alt_text'] = json_encode([
                'image' => $request->image_alt,
                'banner' => $request->banner_alt, 
                'demonstration' => $request->demonstration_image_alt
                ]);
            $categoryImage['updated_at'] = Carbon::now()->format('Y-m-d h:i:s');

            if(!empty($categoryImage))
                AppImage::where('id', $category->images[0]->id)->update($categoryImage); 
        }else{
            $categoryImage['type'] = 'category';
            $categoryImage['item_id'] = $category->id;
            $productImages['title'] = '';
            $productImages['name'] = '';
            $categoryImage['alt_text'] = json_encode([
                'image' => $request->image_alt,
                'banner' => $request->banner_alt, 
                'demonstration' => $request->demonstration_image_alt
                ]);
            $categoryImage['status'] = '1';
            $categoryImage['created_at'] = Carbon::now()->format('Y-m-d h:i:s');
            $categoryImage['updated_at'] = Carbon::now()->format('Y-m-d h:i:s');

            if(!empty($categoryImage))
                AppImage::insert($categoryImage); 
        }

        $subCat = [];
        if(isset($_GET['sub']))
            $subCat = ['sub' => $_GET['sub']];
        return redirect()->route('categories.index', $subCat)->with('success', 
            'Category, '. $category->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $category = Category::findOrFail($id);
        //Remove Images
        $imagesDirectory = public_path('/images/category/'.$category->id.'/');
        if($category->image){
            if(file_exists($imagesDirectory.$category->image))
                unlink($imagesDirectory.$category->image);
        }
        if($category->demonstration_image){
            if(file_exists($imagesDirectory.$category->demonstration_image))
                unlink($imagesDirectory.$category->demonstration_image);
        }
        if($category->banner){
            if(file_exists($imagesDirectory.$category->banner))
                unlink($imagesDirectory.$category->banner);
        }

        //Remove Category Directory
        if(file_exists($imagesDirectory))
            Helper::deleteDirectory($imagesDirectory);

        $category->delete();

        $subCat = [];
        if(isset($_GET['sub']))
            $subCat = ['sub' => $_GET['sub']];
        return redirect()->route('categories.index', $subCat)
            ->with('success',
             'Category successfully deleted');
    }
}
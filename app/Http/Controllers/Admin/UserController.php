<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Country;
use App\Models\Download;

use Auth;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;

class UserController extends Controller {

    public function __construct() {
        //$this->middleware(['auth', 'clearance']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) {
        //Get all users and pass it to the view
        $users = User::whereHas('roles', function($q) use ($request) {
            $q->where('name', $request->type);
        })->orderBy('created_at', 'desc')->get();
        return view('admin.pages.user.'.$request->type)->with('users', $users);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
    //Get all roles and pass it to the view
        $roles = Role::get();
        $countries = Country::orderby('title', 'asc')->get();

        return view('admin.pages.user.create', ['roles' => $roles, 'countries' => $countries]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {
    //Validate name, email and password fields
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6|confirmed'
        ]);

        $user = User::create($request->only('email', 'name', 'password', 'company_name', 'country_id')); //Retrieving only the email and password data
        if(isset($request['stay_alert'])){
            $user->send_latest_alert = $request['stay_alert'];
            $user->save();
        }
            
        $roles = $request['roles']; //Retrieving the roles field
    //Checking if a role was selected
        if (isset($roles)) {

            foreach ($roles as $role) {
            $role_r = Role::where('id', '=', $role)->firstOrFail();            
            $user->assignRole($role_r); //Assigning role to user
            }
        }        
    //Redirect to the users.index view and display message
        return redirect()->route('users.index')
            ->with('success',
             'User successfully added.');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id) {
        return redirect('users'); 
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $user = User::findOrFail($id); //Get user with specified id
        $roles = Role::get(); //Get all roles
        $countries = Country::orderby('title', 'asc')->get();

        return view('admin.pages.user.edit', compact('user', 'roles', 'countries')); //pass user and roles data to view

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id) {
        $user = User::findOrFail($id); //Get role specified by id

    //Validate name, email and password fields    
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users,email,'.$id,
            //'password'=>'required|min:6|confirmed'
        ]);

        $input = $request->only(['name', 'email', 'password', 'company_name', 'country_id']); //Retreive the name, email and password fields
        $roles = $request['roles']; //Retreive all roles
        if(isset($request['stay_alert']))
            $input['send_latest_alert'] = $request['stay_alert'];
        else
            $input['send_latest_alert'] = '0';
        
        $user->fill($input)->save();

        if (isset($roles)) {        
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles          
        }        
        else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        return redirect()->back()
            ->with('success',
             'User successfully edited.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id) {
    //Find a user with a given id and delete
        $user = User::findOrFail($id); 
        $user->delete();

        return redirect()->back()
            ->with('success',
             'User successfully deleted.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function viewDownloads(Request $request, $id) {
        //Find a user with a given id and delete
        $user = User::findOrFail($id); 
        $downloads = Download::with(['catalogue', 'manual', 'certification', 'project_reference', 'product'])->where(['user_id' => $user->id])->get();
        return view('admin.pages.user.downloads', compact('user', 'downloads'));
    }
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\HomeNewsHighlight;

use App\Helpers\Helper;

class HomeNewsHighlightController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index() {
        $items = HomeNewsHighlight::orderby('id', 'desc')->get();
        return view('admin.pages.home.news_highlight.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.home.news_highlight.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title'=>'required',
            'url'=>'required',
            'label'=>'required',
            'image'=>'required'
        ]);

        $data['title'] = $request['title'];
        $data['url'] = $request['url'];
        $data['label'] = $request['label'];
        $data['sort'] = (!empty($request['sort']) ? $request['sort'] : '0');
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['image_alt'] = $request['image_alt'];

        $HomeNewsHighlight = HomeNewsHighlight::create($data);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/home/news-highlight/'.$HomeNewsHighlight->id.'/'), $image);
            $HomeNewsHighlight->image = $image;
            $HomeNewsHighlight->save();
        }

        //Display a successful message upon save
        return redirect()->route('home-news-highlight.index')
            ->with('success', 'News & Highlight,
             '. $HomeNewsHighlight->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $HomeNewsHighlight = HomeNewsHighlight::findOrFail($id); //Find post of id = $id
        return view ('admin.pages.home.news_highlight.show', compact('HomeNewsHighlight'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $HomeNewsHighlight = HomeNewsHighlight::findOrFail($id);
        return view('admin.pages.home.news_highlight.edit', compact('HomeNewsHighlight'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required',
            'url'=>'required',
            'label'=>'required'
        ]);

        $HomeNewsHighlight = HomeNewsHighlight::findOrFail($id);
        $HomeNewsHighlight->title = $request['title'];
        $HomeNewsHighlight->url = $request['url'];
        $HomeNewsHighlight->label = $request['label'];
        $HomeNewsHighlight->sort = $request['sort'];    
        $HomeNewsHighlight->status = (isset($request['status']) ? $request['status'] : '0');
        $HomeNewsHighlight->image_alt = $request['image_alt'];

        if(!empty($request['image'])){
            if($HomeNewsHighlight->image){
                if(file_exists(public_path('/images/home/news-highlight/'.$HomeNewsHighlight->id.'/').$HomeNewsHighlight->image))
                    unlink(public_path('/images/home/news-highlight/'.$HomeNewsHighlight->id.'/').$HomeNewsHighlight->image);
            }
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/home/news-highlight/'.$HomeNewsHighlight->id.'/'), $image);
            $HomeNewsHighlight->image = $image;
        }

        $HomeNewsHighlight->save();

        return redirect()->route('home-news-highlight.index')->with('success', 
            'HomeNewsHighlight, '. $HomeNewsHighlight->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $HomeNewsHighlight = HomeNewsHighlight::findOrFail($id);
        if($HomeNewsHighlight->image){
            $imagesDirectory = public_path('/images/home/news-highlight/'.$HomeNewsHighlight->id.'/');
            if(file_exists($imagesDirectory.$HomeNewsHighlight->image))
                unlink($imagesDirectory.$HomeNewsHighlight->image);
            //Remove Product Directory
            Helper::deleteDirectory($imagesDirectory);
        }
        $HomeNewsHighlight->delete();

        return redirect()->route('home-news-highlight.index')
            ->with('success',
             'News & Highlight successfully deleted');
    }
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Seo;

class SeoController extends Controller
{
    public $leftMainActive;

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
        $this->leftMainActive = 'seo';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request) {
        $seo = Seo::all();
        return view('admin.pages.seo.index', compact('seo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.seo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'url' => 'required'
            ]);
        $data['url'] = $request['url'];
        $data['meta_title'] = $request['meta_title'];
        $data['meta_description'] = $request['meta_description'];
        $data['meta_keywords'] = $request['meta_keywords'];
        $seo = Seo::create($data);

        //Display a successful message upon save
        return redirect()->route('seo.index')
            ->with('success', 'Seo,
             '. $seo->url.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $seo = Seo::findOrFail($id); //Find seo of id = $id
        return view ('admin.pages.seo.show', compact('seo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $seo = Seo::findOrFail($id);
        return view('admin.pages.seo.edit', compact('seo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'url'=>'required'
        ]);

        $seo = Seo::findOrFail($id);
        $seo->url = $request['url'];
        $seo->meta_title = $request['meta_title'];
        $seo->meta_description = $request['meta_description'];
        $seo->meta_keywords = $request['meta_keywords'];
        $seo->save();

        return redirect()->route('seo.index')->with('success', 
            'Seo, '. $seo->url.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $seo = Seo::findOrFail($id);
        $seo->delete();

        return redirect()->route('seo.index')
            ->with('success',
             'Seo successfully deleted');
    }
}
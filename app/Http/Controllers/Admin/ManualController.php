<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Manual;
use App\Models\ManualCategory;

use App\Helpers\Helper;

class ManualController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index() {
        $items = Manual::orderby('id', 'desc')->get();
        return view('admin.pages.manual.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = ManualCategory::where(['status' => '1'])->get();
        return view('admin.pages.manual.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title'=>'required',
            'categories'=>'required'
            ]);

        $data['title'] = $request['title'];
        $slug = Helper::parseSaveURLString($request['title']);
        $data['slug'] = $slug;
        $data['description'] = $request['description'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['image_alt'] = $request['image_alt'];

        $manual = Manual::create($data);

        if(!empty($request['categories']))
            $manual->categories()->sync($request['categories']);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/manual/'.$manual->id.'/'), $image);
            $manual->image = $image;
            $manual->save();
        }

        if(!empty($request['document'])){
            $document = $request['document']->getClientOriginalName();
            $request['document']->move( public_path('/documents/manual/'.$manual->id.'/'), $document);
            $manual->document = $document;
            $manual->save();
        }

        //Display a successful message upon save
        return redirect()->route('manuals.index')
            ->with('success', 'Catalogue,
             '. $manual->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $manual = Manual::findOrFail($id); //Find post of id = $id
        return view ('admin.pages.manual.show', compact('manual'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $manual = Manual::with(['categories'])->findOrFail($id);
        $categories = ManualCategory::where(['status' => '1'])->get();
        return view('admin.pages.manual.edit', compact('manual', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required',
            'categories'=>'required'
        ]);

        $manual = Manual::findOrFail($id);
        $manual->title = $request['title'];
        $slug = Helper::parseSaveURLString($request['title']);
        $manual->slug = $slug;
        $manual->description = $request['description'];       
        $manual->status = (isset($request['status']) ? $request['status'] : '0');
        $manual->image_alt = $request['image_alt'];

        if(!empty($request['categories']))
            $manual->categories()->sync($request['categories']);

        if(!empty($request['image'])){
            if($manual->image){
                if(file_exists(public_path('/images/manual/'.$manual->id.'/').$manual->image))
                    unlink(public_path('/images/manual/'.$manual->id.'/').$manual->image);
            }
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/manual/'.$manual->id.'/'), $image);
            $manual->image = $image;
        }

        if(!empty($request['document'])){
            if($manual->document){
                if(file_exists(public_path('/documents/manual/'.$manual->id.'/').$manual->document))
                    unlink(public_path('/documents/manual/'.$manual->id.'/').$manual->document);
            }
            $document = $request['document']->getClientOriginalName();
            $request['document']->move( public_path('/documents/manual/'.$manual->id.'/'), $document);
            $manual->document = $document;
        }

        $manual->save();

        return redirect()->route('manuals.index')->with('success', 
            'Catalogue, '. $manual->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $manual = Manual::findOrFail($id);
        if($manual->image){
            $imagesDirectory = public_path('/images/manual/'.$manual->id.'/');
            if(file_exists($imagesDirectory.$manual->image))
                unlink($imagesDirectory.$manual->image);
            //Remove Product Directory
            Helper::deleteDirectory($imagesDirectory);
        }
        if($manual->document){
            $documentDirectory = public_path('/documents/manual/'.$manual->id.'/');
            if(file_exists($documentDirectory.$manual->document))
                unlink($documentDirectory.$manual->document);
            //Remove Product Directory
            Helper::deleteDirectory($documentDirectory);
        }
        $manual->delete();

        return redirect()->route('manuals.index')
            ->with('success',
             'Catalogue successfully deleted');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories(Request $request) {
        $items = ManualCategory::all();
        return view('admin.pages.manual.index_category', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCategory() {
        return view('admin.pages.manual.create_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCategory(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title' => 'required'
            ]);
        $data['title'] = $request['title'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');

        $manualCategory = ManualCategory::create($data);

        //Display a successful message upon save
        return redirect()->route('manuals-category.index')
            ->with('success', 'Catalogue Category,
             '. $manualCategory->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCategory($id) {
        $manualCategory = ManualCategory::findOrFail($id); //Find seo of id = $id
        return view ('admin.pages.manual.show_category', compact('tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editCategory($id) {
        $manualCategory = ManualCategory::findOrFail($id);
        return view('admin.pages.manual.edit_category', compact('manualCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCategory(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required'
        ]);

        $manualCategory = ManualCategory::findOrFail($id);
        $manualCategory->title = $request['title'];
        $manualCategory->status = (isset($request['status']) ? $request['status'] : '0');
        $manualCategory->save();

        return redirect()->route('manuals-category.index')->with('success', 
            'Catalogue Category, '. $manualCategory->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyCategory($id) {
        $manualCategory = ManualCategory::findOrFail($id);
        $manualCategory->delete();

        return redirect()->route('manuals-category.index')
            ->with('success',
             'Catalogue Category successfully deleted');
    }
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Quote;
use Excel;

class QuoteController extends Controller
{
    public $leftMainActive;

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
        $this->leftMainActive = 'quote';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request) {
        $quotes = Quote::orderBy('updated_at', 'desc')->get();
        return view('admin.pages.quote.index', compact('quotes'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $quote = Quote::findOrFail($id);
        return view ('admin.pages.quote.show', compact('quote'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $quote = Quote::findOrFail($id);
        return view('admin.pages.quote.edit', compact('quote'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        $quote = Quote::findOrFail($id);
        $quote->name = $request['name'];
        $quote->email = $request['email'];
        $quote->company = $request['company'];
        $quote->phone = $request['phone'];
        $quote->message = $request['message'];
        $quote->status = (isset($request['status']) ? $request['status'] : '0');
        $quote->save();

        return redirect()->route('quotes.index')->with('success', 
            'Quote, '. $quote->name.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $seo = Quote::findOrFail($id);
        $seo->delete();

        return redirect()->route('quotes.index')
            ->with('success',
             'Quote successfully deleted');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadExcel(Request $request, $type)
    {
        $allQuotes = Quote::select('name','company','email','phone','products','created_at','message')->orderBy('updated_at', 'desc')->get()->toArray();
        if(!empty($allQuotes)){
            $quotes = [];
            foreach($allQuotes as $allQuoteKey => $allQuote){
                foreach($allQuote as $quoteKey => $quote){
                    if($quoteKey == 'products'){
                        $quoteRequestedProducts = [];
                        if($quote){
                            $requestQuoteProducts = json_decode($quote, true);
                            foreach($requestQuoteProducts as $quoteProduct)
                                $quoteRequestedProducts[] = $quoteProduct['title']. ' - ('.$quoteProduct['quantity'].')';
                        }
                        $quotes[$allQuoteKey]['products'] = implode(','.PHP_EOL, $quoteRequestedProducts);
                    }else{
                        $quotes[$allQuoteKey][$quoteKey] = $quote;
                    }
                }
            }
            return Excel::create('quotes', function($excel) use ($quotes) {
                $excel->sheet('all quotes', function($sheet) use ($quotes)
                {
                    $sheet->fromArray($quotes);
                });
            })->download($type);
        }
        return redirect()->route('quotes.index')
            ->with('success',
             'No Quote to retrive.');
    }
}
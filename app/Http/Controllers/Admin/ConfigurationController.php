<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Models\Configuration;

use App\Helpers\Helper;

class ConfigurationController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit() {
        $data['configurations'] = Helper::getConfigurations();
        $data['pageTitle'] = 'Site Configurations';
        return view('admin.pages.configuration.edit', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function footerLinksEdit() {
        $data['configurations'] = Helper::getConfigurations();
        $data['pageTitle'] = 'Site Footer';
        return view('admin.pages.configuration.footer-links', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $configurations = $request->except(['_method','_token']);
        $prevConfigurations = Helper::getConfigurations();
        //dd($prevConfigurations);

        if(!isset($configurations['web_maintenance'])){
            $configurations['web_maintenance'] = NULL;
        }
        if(!empty($configurations['smtp_password'])){
            $configurations['smtp_password'] = Crypt::encryptString($configurations['smtp_password']);
        }else{
            $configurations['smtp_password'] = $prevConfigurations['smtp_password'];
        }
        if(!empty($configurations['web_logo'])){
            if($prevConfigurations['web_logo']){
                if(file_exists(public_path('/images/').$prevConfigurations['web_logo']))
                    unlink(public_path('/images/').$prevConfigurations['web_logo']);
            }
            $image = time().'-web-logo.'.$configurations['web_logo']->getClientOriginalExtension();
            $configurations['web_logo']->move( public_path('/images/'), $image);
            $configurations['web_logo'] = $image;
        }
        if(!empty($configurations['web_ico'])){
            if($prevConfigurations['web_ico']){
                if(file_exists(public_path('/images/').$prevConfigurations['web_ico']))
                    unlink(public_path('/images/').$prevConfigurations['web_ico']);
            }
            $image = time().'-web-ico.'.$configurations['web_ico']->getClientOriginalExtension();
            $configurations['web_ico']->move( public_path('/images/'), $image);
            $configurations['web_ico'] = $image;
        }
        if(!empty($configurations['sitemap_file'])){
            if($prevConfigurations['sitemap_file']){
                if(file_exists(public_path('/').$prevConfigurations['sitemap_file']))
                    unlink(public_path('/').$prevConfigurations['sitemap_file']);
            }
            $sitemap = $configurations['sitemap_file']->getClientOriginalName();
            $configurations['sitemap_file']->move( public_path('/'), $sitemap);
            $configurations['sitemap_file'] = $sitemap;
        }

        foreach ($configurations as $key => $value) {
            Configuration::updateOrCreate(['key_name' => $key], ['key_name' => $key, 'value' => $value]);
        }

        return redirect()->back()->with('success', 
            'settings updated');
    }
}
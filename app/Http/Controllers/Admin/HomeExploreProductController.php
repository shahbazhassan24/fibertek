<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\HomeExploreProduct;

use App\Helpers\Helper;

class HomeExploreProductController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index() {
        $items = HomeExploreProduct::orderby('sort', 'asc')->get();
        return view('admin.pages.home.explore_product.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.home.explore_product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title'=>'required',
            'url'=>'required',
            'image'=>'required'
            ]);

        $data['title'] = $request['title'];
        $data['url'] = $request['url'];
        $data['sort'] = (!empty($request['sort']) ? $request['sort'] : '0');
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['image_alt'] = $request['image_alt'];

        $homeExploreProduct = HomeExploreProduct::create($data);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/home/explore-product/'.$homeExploreProduct->id.'/'), $image);
            $homeExploreProduct->image = $image;
            $homeExploreProduct->save();
        }

        //Display a successful message upon save
        return redirect()->route('home-exlpore-product.index')
            ->with('success', 'Exlpore Product,
             '. $homeExploreProduct->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $homeExploreProduct = HomeExploreProduct::findOrFail($id); //Find post of id = $id
        return view ('admin.pages.home.explore_product.show', compact('homeExploreProduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $homeExploreProduct = HomeExploreProduct::findOrFail($id);
        return view('admin.pages.home.explore_product.edit', compact('homeExploreProduct'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required',
            'url'=>'required'
        ]);

        $homeExploreProduct = HomeExploreProduct::findOrFail($id);
        $homeExploreProduct->title = $request['title'];
        $homeExploreProduct->url = $request['url'];
        $homeExploreProduct->sort = $request['sort'];       
        $homeExploreProduct->status = (isset($request['status']) ? $request['status'] : '0');
        $homeExploreProduct->image_alt = $request['image_alt'];

        if(!empty($request['image'])){
            if($homeExploreProduct->image){
                if(file_exists(public_path('/images/home/explore-product/'.$homeExploreProduct->id.'/').$homeExploreProduct->image))
                    unlink(public_path('/images/home/explore-product/'.$homeExploreProduct->id.'/').$homeExploreProduct->image);
            }
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/home/explore-product/'.$homeExploreProduct->id.'/'), $image);
            $homeExploreProduct->image = $image;
        }

        $homeExploreProduct->save();

        return redirect()->route('home-exlpore-product.index')->with('success', 
            'Exlpore Product, '. $homeExploreProduct->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $homeExploreProduct = HomeExploreProduct::findOrFail($id);
        if($homeExploreProduct->image){
            $imagesDirectory = public_path('/images/home/explore-product/'.$homeExploreProduct->id.'/');
            if(file_exists($imagesDirectory.$homeExploreProduct->image))
                unlink($imagesDirectory.$homeExploreProduct->image);
            //Remove Product Directory
            Helper::deleteDirectory($imagesDirectory);
        }
        $homeExploreProduct->delete();

        return redirect()->route('home-exlpore-product.index')
            ->with('success',
             'Exlpore Product successfully deleted');
    }
}
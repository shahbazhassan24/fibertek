<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Subscriber;
use Excel;

class SubscriberController extends Controller
{
    public $leftMainActive;

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
        $this->leftMainActive = 'subscriber';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request) {
        $subscribers = Subscriber::orderBy('updated_at', 'desc')->get();
        return view('admin.pages.subscriber.index', compact('subscribers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.subscriber.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:subscribers,email'
            ]);
        $data['name'] = $request['name'];
        $data['email'] = $request['email'];
        $data['company'] = $request['company'];
        $data['how'] = $request['how'];
        $data['how_source'] = $request['how_source'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $subscriber = Subscriber::create($data);

        //Display a successful message upon save
        return redirect()->route('subscriber.index')
            ->with('success', 'Subscriber,
             '. $subscriber->name.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $subscriber = Subscriber::findOrFail($id);
        return view ('admin.pages.subscriber.show', compact('subscriber'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $subscriber = Subscriber::findOrFail($id);
        return view('admin.pages.subscriber.edit', compact('subscriber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        $subscriber = Subscriber::findOrFail($id);
        $subscriber->name = $request['name'];
        $subscriber->email = $request['email'];
        $subscriber->company = $request['company'];
        $subscriber->how = $request['how'];
        $subscriber->how_source = $request['how_source'];
        $subscriber->status = $request['status'];
        $subscriber->save();

        return redirect()->route('subscriber.index')->with('success', 
            'Subscriber, '. $subscriber->name.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $seo = Subscriber::findOrFail($id);
        $seo->delete();

        return redirect()->route('subscriber.index')
            ->with('success',
             'Subscriber successfully deleted');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadExcel(Request $request, $type)
    {
        $allSubscribers = Subscriber::with('country')->select('name','company','email','country_id','created_at')->orderBy('updated_at', 'desc')->get()->toArray();
        if(!empty($allSubscribers)){
            $subscribers = [];
            foreach($allSubscribers as $allSubKey => $allSubscriber){
                foreach($allSubscriber as $subKey => $subscriber){
                    if($subKey == 'country_id' || $subKey == 'country'){
                        if($subKey == 'country'){
                            continue;
                        }
                        else{
                            $subscribers[$allSubKey]['country'] = $allSubscriber['country']['title'];
                        }
                    }else{
                        $subscribers[$allSubKey][$subKey] = $subscriber;
                    }
                }
            }
            return Excel::create('subscribers', function($excel) use ($subscribers) {
                $excel->sheet('all subscribers', function($sheet) use ($subscribers)
                {
                    $sheet->fromArray($subscribers);
                });
            })->download($type);
        }
        return redirect()->route('subscriber.index')
            ->with('success',
             'No Subscriber to retrive.');
    }
}
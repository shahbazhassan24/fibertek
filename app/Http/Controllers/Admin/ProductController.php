<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

use App\Models\Product;
use App\Models\Category;
use App\Models\Image as AppImage;
use App\Models\Document as AppDocument;

use File;
use Image;
use Excel;
use Storage;

use App\Helpers\Helper;

class ProductController extends Controller
{
    protected $activeMainTab;

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
        $this->activeMainTab = 'product';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request) {
        $activeTab = ['main' => $this->activeMainTab, 'sub' => 'products'];
        $breadcrumbs = ['0' => ['title' => 'Products']];
        $productQuery = Product::with(['categories']);
        if($request->featured){
            $productQuery->where('is_featured', '1');
            $activeTab = ['main' => $this->activeMainTab, 'sub' => 'featured'];
            $breadcrumbs = ['0' => ['title' => 'Featured Products']];
        }
        if($request->sub){
            $productQuery->whereHas('categories', function($q) use ($request) {
                $q->where('category_id', $request->sub);
            });
            $category = Category::find($request->sub);
            $categoryUrl = route('categories.index');
            if($category->parent_id)
                $categoryUrl = route('categories.index', ['sub' => $request->sub]);
            $breadcrumbs = [
                '0' => ['title' => $category->title, 'url' => $categoryUrl],
                '1' => ['title' => 'Products']];
        }
        $formParameters = [];
        if($request->featured){
            $formParameters['featured'] = $request->featured;
        }
        if($request->sub){
            $formParameters['sub'] = $request->sub;
        }
        $products = $productQuery->orderby('sort', 'asc')->get();
        return view('admin.pages.product.index', compact('products', 'formParameters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        $categories = Category::where('parent_id', NULL)->orderby('sort', 'asc')->get();
        $relatedProducts = Product::all();
        $formUrl = '';
        $formParameters = [];
        if($request->featured){
            $formParameters['featured'] = $request->featured;
        }
        if($request->sub){
            $formParameters['sub'] = $request->sub;
        }
        if(!empty($formParameters))
            $formUrl .= '?'.http_build_query($formParameters);

        return view('admin.pages.product.create', compact('categories', 'relatedProducts', 'formUrl'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title' => 'required',
            'categories' => 'required'
            ]);
        $data['title'] = $request['title'];
        $slug = Helper::parseSaveURLString($data['title']);
        $data['slug'] = $slug;
        $data['short_description'] = $request['short_description'];
        $data['description'] = $request['description'];
        $data['sku'] = $request['sku'];
        // $data['price'] = $request['price'];
        // $data['netweight'] = $request['netweight'];
        $data['sort'] = (!empty($request['sort']) ? $request['sort'] : '0');
        $data['is_featured'] = (isset($request['is_featured']) ? $request['is_featured'] : '0');
        $data['featured_description'] = $request['featured_description'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');

        $product = Product::create($data);

        if(!empty($request['categories']))
            $product->categories()->sync($request['categories']);

        if(!empty($request['images']['file'])){
            $productImages = [];
            $productNameSlug = $data['slug'];
            $watermarkOpacity = '100';

            $destinationPath = public_path('/images/product/'.$product->id.'/');  
            if(!File::isDirectory($destinationPath)){
                File::makeDirectory($destinationPath, 0755, true, true);
            }
            
            foreach ($request['images']['file'] as $imageKey => $image) {
                $uploadingImage = Image::make($image);
                $imageName = "img-".$productNameSlug."-".$imageKey. "." . strtolower($image->getClientOriginalExtension());
                $watermark = Image::make(public_path('/frontend/images/watermark-fibertek.png'));

                //#1
                //$watermarkSize = $uploadingImage->width() - 20; //size of the image minus 20 margins
                //#2
                //$watermarkSize = $uploadingImage->width() / 2; //half of the image size
                //#3
                $resizePercentage = '30'; //30% less then an actual image (play with this value)
                $watermarkSize = round($uploadingImage->width() * ((100 - $resizePercentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image
                // resize watermark width keep height auto
                $watermark->resize($watermarkSize, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $watermark->opacity($watermarkOpacity);
                //insert resized watermark to image center aligned
                //save new image
                
                Image::make($image->getRealPath())->insert($watermark, 'center')->save($destinationPath.$imageName);

                //Make Images Data to Insert
                $productImages[$imageKey]['type'] = 'product';
                $productImages[$imageKey]['item_id'] = $product->id;
                $productImages[$imageKey]['title'] = $image->getClientOriginalName();
                $productImages[$imageKey]['name'] = $imageName;
                $productImages[$imageKey]['alt_text'] = $request['images']['alt'][$imageKey];
                $productImages[$imageKey]['sort'] = $request['images']['index'][$imageKey];
                $productImages[$imageKey]['status'] = '1';
                $productImages[$imageKey]['created_at'] = Carbon::now()->format('Y-m-d h:i:s');
                $productImages[$imageKey]['updated_at'] = Carbon::now()->format('Y-m-d h:i:s');
            }
            if(!empty($productImages))
                AppImage::insert($productImages);           
        }

        if(!empty($request['pdf_download'])){
                $docName = $request['pdf_download']->getClientOriginalName();
                $request['pdf_download']->move( public_path('/documents/product/'.$product->id.'/'), $docName);
                $productDoc['type'] = 'product';
                $productDoc['item_id'] = $product->id;
                $productDoc['title'] = $request['pdf_download']->getClientOriginalName();
                $productDoc['name'] = $docName;
                $productDoc['status'] = '1';
            if(!empty($productDoc))
                AppDocument::create($productDoc);           
        }

        $redirectParameters = [];
        if($request->featured){
            $redirectParameters['featured'] = $request->featured;
        }
        if($request->sub){
            $redirectParameters['sub'] = $request->sub;
        }
        return redirect()->route('products.index', $redirectParameters)
            ->with('flash_message', 'Product,
             '. $product->title.' created');
    }

    public function applyWaterMarkImages(){
        $directories = [];
        $iterator = new \DirectoryIterator(public_path('images/product/'));
        foreach ($iterator as $fileinfo) {
            if (!$fileinfo->isDot()) {
                $directories[] = $fileinfo->getFilename();
            }
        }
        sort($directories);
        if(!empty($directories)){
            $watermark = Image::make(public_path('/frontend/images/watermark-fibertek.png'));
            $watermarkOpacity = '100';
            foreach($directories as $directory){
                $files=['jpg', 'jpeg', 'png'];
                $images = preg_grep('/\.(jpg|jpeg|png|gif)(?:[\?\#].*)?$/i', $files);
                $imagesDirectory = public_path('images/product/'.$directory.'/');
                if ($handle = opendir($imagesDirectory)) {
                    while (false !== ($entry = readdir($handle))) {
                        $files[] = $entry;
                    }
                    $foundImages = preg_grep('/\.(jpg|jpeg|png|gif)(?:[\?\#].*)?$/i', $files);
                    foreach($foundImages as $imageValue)
                    {
                        $image = Image::make($imagesDirectory.$imageValue);
                        //dd($image);
                        #1
                        //$watermarkSize = $image->width() - 20; //size of the image minus 20 margins
                        //#2
                        //$watermarkSize = $image->width() / 2; //half of the image size
                        //#3
                        $resizePercentage = '30'; //30% less then an actual image (play with this value)
                        $watermarkSize = round($image->width() * ((100 - $resizePercentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image
                        // resize watermark width keep height auto
                        $watermark->resize($watermarkSize, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
        
                        $watermark->opacity($watermarkOpacity);
                        //insert resized watermark to image center aligned
                        //save new image
                        $image->insert($watermark, 'center')->save($imagesDirectory.$imageValue);
                    }
                    closedir($handle);
                }
            }
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $product = Product::findOrFail($id); //Find product of id = $id
        return view ('admin.pages.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id) {
        $product = Product::with(['categories', 'images', 'documents', 'linkedProducts'])->findOrFail($id);
        $categories = Category::where('parent_id', NULL)->orderby('sort', 'asc')->get();
        $relatedProducts = Product::where('id', '!=', $id)->get();
        $formUrl = $product->id;
        $formParameters = [];
        if($request->featured){
            $formParameters['featured'] = $request->featured;
        }
        if($request->sub){
            $formParameters['sub'] = $request->sub;
        }
        if(!empty($formParameters))
            $formUrl .= '?'.http_build_query($formParameters);

        return view('admin.pages.product.edit', compact('product', 'categories', 'relatedProducts', 'formUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required',
            'categories' => 'required'
        ]);

        $product = Product::with(['images', 'documents'])->findOrFail($id);

        $product->title = $request['title'];
        $product->slug = Helper::parseSaveURLString($request['title']);
        $product->short_description = $request['short_description'];
        $product->description = $request['description'];
        $product->sku = $request['sku'];
        $product->price = $request['price'];
        $product->netweight = $request['netweight'];
        $product->sort = $request['sort'];
        $product->is_featured = $request['is_featured'];
        $product->featured_description = $request['featured_description'];
        $product->status = $request['status'];

        $product->save();

        if(!empty($request['categories']))
            $product->categories()->sync($request['categories']);

        $product->relatedProducts()->sync($request['related_products']);

        //Images Destination Directory
        $imagesDirectory = public_path('/images/product/'.$product->id.'/');

        //Remove images if removed
        if(isset($request['remove_images']) && !empty($request['remove_images'])){
            // foreach ($request['remove_images'] as $removeImage) {
            //     if(file_exists($imagesDirectory.$removeImage)){
            //         //Remove Product Image
            //         unlink($imagesDirectory.$removeImage);
            //     }
            //     AppImage::where(['name' => $removeImage, 'item_id' => $product->id, 'type' => 'product'])->delete();
            // }
            foreach ($request['remove_images'] as $removeImage) {
                $imageToRemove = AppImage::where(['id' => $removeImage, 'type' => 'product'])->first();
                if(file_exists($imagesDirectory.$imageToRemove->name)){
                    //Remove Product Image
                    unlink($imagesDirectory.$imageToRemove->name);
                }
                $imageToRemove->delete();
            }
        }

        if(!empty($request['images']['file'])){
            $productImages = [];
            $productNameSlug = $product->slug;  
            $watermarkOpacity = '100';

            $destinationPath = public_path('/images/product/'.$product->id.'/');  
            if(!File::isDirectory($destinationPath)){
                File::makeDirectory($destinationPath, 0755, true, true);
            }
            
            foreach ($request['images']['file'] as $imageKey => $image) {
                $uploadingImage = Image::make($image);
                $imageName = "img-".$productNameSlug."-".$imageKey. "." . strtolower($image->getClientOriginalExtension());

                $watermark = Image::make(public_path('/frontend/images/watermark-fibertek.png'));
                #1
                //$watermarkSize = $uploadingImage->width() - 20; //size of the image minus 20 margins
                //#2
                //$watermarkSize = $uploadingImage->width() / 2; //half of the image size
                //#3
                $resizePercentage = '30'; //30% less then an actual image (play with this value)
                $watermarkSize = round($uploadingImage->width() * ((100 - $resizePercentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image
                // resize watermark width keep height auto
                $watermark->resize($watermarkSize, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $watermark->opacity($watermarkOpacity);
                //insert resized watermark to image center aligned
                //save new image
                Image::make($image->getRealPath())->insert($watermark, 'center')->save($imagesDirectory.$imageName);
                
                $productImages[$imageKey]['type'] = 'product';
                $productImages[$imageKey]['item_id'] = $product->id;
                $productImages[$imageKey]['title'] = $image->getClientOriginalName();
                $productImages[$imageKey]['name'] = $imageName;
                $productImages[$imageKey]['alt_text'] = $request['images']['alt'][$imageKey];
                $productImages[$imageKey]['sort'] = $request['images']['index'][$imageKey];
                $productImages[$imageKey]['status'] = '1';
                $productImages[$imageKey]['created_at'] = Carbon::now()->format('Y-m-d h:i:s');
                $productImages[$imageKey]['updated_at'] = Carbon::now()->format('Y-m-d h:i:s');
            }

            if(!empty($productImages))
                AppImage::insert($productImages);           
        }

        if(!empty($request['images']['ids'])){
            foreach ($request['images']['ids'] as $imageKey => $imageID) {
                AppImage::where('id', $imageID)->update(['alt_text' => $request['images']['alt'][$imageKey], 'sort' => $request['images']['index'][$imageKey]]);
            }
        }

        if(!empty($request['pdf_download'])){
            $docName = $request['pdf_download']->getClientOriginalName();
            $request['pdf_download']->move( public_path('/documents/product/'.$product->id.'/'), $docName);
            if(isset($product->documents) && ($product->documents->isNotEmpty())){
                if(file_exists(public_path('/documents/product/'.$product->id.'/').$product->documents[0]['name']))
                unlink(public_path('/documents/product/'.$product->id.'/').$product->documents[0]['name']);
                $productDoc = AppDocument::where(['type' => 'product', 'item_id' => $product->id])->first();
                $productDoc->title = $request['pdf_download']->getClientOriginalName();
                $productDoc->name = $docName;
                $productDoc->save();
            }else{
                $productDoc['type'] = 'product';
                $productDoc['item_id'] = $product->id;
                $productDoc['title'] = $request['pdf_download']->getClientOriginalName();
                $productDoc['name'] = $docName;
                $productDoc['status'] = '1';
                if(!empty($productDoc))
                    AppDocument::create($productDoc); 
            }
        }

        $redirectParameters = [];
        if($request->featured){
            $redirectParameters['featured'] = $request->featured;
        }
        if($request->sub){
            $redirectParameters['sub'] = $request->sub;
        }
        return redirect()->route('products.index', $redirectParameters)
        ->with('success', 'Product, '. $product->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        $product = Product::with(['categories', 'images', 'documents'])->findOrFail($id);
        //Remove Images
        if(isset($product->images) && ($product->images->isNotEmpty())){
            $imagesDirectory = public_path('/images/product/'.$product->id.'/');
            foreach ($product->images as $imageKey => $image) {
                if(file_exists($imagesDirectory.$image->name))
                    unlink($imagesDirectory.$image->name);
            }
            //Remove Product Directory
            Helper::deleteDirectory($imagesDirectory);
            AppImage::where(['item_id' => $product->id, 'type' => 'product'])->delete();
        }
        //Remove Documents
        if(isset($product->documents) && ($product->documents->isNotEmpty())){
            $documentDirectory = public_path('/documents/product/'.$product->id.'/');
            if(file_exists($documentDirectory.$product->documents[0]['name']))
                unlink($documentDirectory.$product->documents[0]['name']);
            //Remove Document Directory
            Helper::deleteDirectory($documentDirectory);
            AppDocument::where(['id' => $product->documents[0]['id']])->delete();
        }

        //Detach Categories
        $productCategories = $product->categories->pluck('id')->toArray();
        $product->categories()->detach($productCategories);

        $product->delete();

        $redirectParameters = [];
        if($request->featured){
            $redirectParameters['featured'] = $request->featured;
        }
        if($request->sub){
            $redirectParameters['sub'] = $request->sub;
        }
        return redirect()->route('products.index', $redirectParameters)
            ->with('success',
             'Product successfully deleted');
    }

    public function importProducts(Request $request) 
    {
        $request->validate([
            'import_products' => 'required'
        ]);
        $path = $request->file('import_products')->getRealPath();
        $excelData = Excel::load($path)->get();
        if($excelData->count()){
            $syncItem = false;
            foreach ($excelData as $excelKey => $excelValue) {
                if($excelValue->title && $excelValue->slug){
                    $product = Product::where(['title' => $excelValue->title, 'slug' => $excelValue->slug])->first();
                    if(!$product) {
                        //Create New Product
                        $productDetail = [
                            'title' => $excelValue->title,
                            'slug' => $excelValue->slug,
                            'short_description' => $excelValue->short_description,
                            'description' => $excelValue->description,
                            'sku' => $excelValue->sku,
                            'sort' => $excelValue->index,
                            'status' => $excelValue->status,
                            'is_featured' => $excelValue->featured,
                            'featured_description' => $excelValue->featured_description
                        ];
                        $product = product::create($productDetail);
                    }else{
                        //Update Existing Product
                        $product->short_description = $excelValue->short_description;
                        $product->description = $excelValue->description;
                        $product->sku = $excelValue->sku;
                        $product->sort = $excelValue->index;
                        $product->status = $excelValue->status;
                        $product->is_featured = $excelValue->featured;
                        $product->featured_description = $excelValue->featured_description;
                        $product->save();
                    }
                    //Update Product Features now
                    $syncItem = true;
                }

                if($syncItem && $excelValue->images){
                    $imagesDirectory = public_path('/images/product/'.$product->id.'/');
                    if (!file_exists($imagesDirectory)) {
                        mkdir($imagesDirectory, 755, true);
                    }
                    $importImages = explode(',', $excelValue->images);
                    $productImages = [];
                    $productNameSlug = $product->slug;  
                    $watermarkOpacity = '100';

                    foreach ($importImages as $imageKey => $imageValue) {
                        $image = Image::make(public_path('/import/product/images/'.$imageValue));
                        $imageName = "img-".$productNameSlug."-".$imageKey. "." . strtolower($image->extension);
                        //dd($image);
                        $watermark = Image::make(public_path('/frontend/images/watermark-fibertek.png'));
                        #1
                        //$watermarkSize = $image->width() - 20; //size of the image minus 20 margins
                        //#2
                        //$watermarkSize = $image->width() / 2; //half of the image size
                        //#3
                        $resizePercentage = '30'; //30% less then an actual image (play with this value)
                        $watermarkSize = round($image->width() * ((100 - $resizePercentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image
                        // resize watermark width keep height auto
                        $watermark->resize($watermarkSize, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                        $watermark->opacity($watermarkOpacity);
                        //insert resized watermark to image center aligned
                        //save new image
                        $image->insert($watermark, 'center')->save($imagesDirectory.$imageName);
                        
                        $productImages[$imageKey]['type'] = 'product';
                        $productImages[$imageKey]['item_id'] = $product->id;
                        $productImages[$imageKey]['title'] = $image->basename;
                        $productImages[$imageKey]['name'] = $imageName;
                        $productImages[$imageKey]['status'] = '1';
                        $productImages[$imageKey]['created_at'] = Carbon::now()->format('Y-m-d h:i:s');
                        $productImages[$imageKey]['updated_at'] = Carbon::now()->format('Y-m-d h:i:s');
                    }

                    if(!empty($productImages))
                        AppImage::insert($productImages);
                }
                if($syncItem && $excelValue->categories){
                    $categories = Category::select('id')->whereIn('title', explode(',', $excelValue->categories))->get()->pluck(['id'])->toArray();
                    $product->categories()->sync($categories);
                }
                if($syncItem && $excelValue->related_products){
                    $relatedProducts = Product::select('id')->whereIn('title', explode(',', $excelValue->related_products))->get()->pluck(['id'])->toArray();
                    $product->relatedProducts()->sync($relatedProducts);
                }

                if($syncItem && $excelValue->document){
                    $documentDirectory = public_path('/documents/product/'.$product->id.'/');
                    if (!file_exists($documentDirectory)) {
                        mkdir($documentDirectory, 755, true);
                    }
                    //Copy Document File
                    File::copy(
                        public_path('/import/product/documents/'.$excelValue->document),
                        $documentDirectory.$excelValue->document
                    );
                    $docName = $excelValue->document;
                    if(isset($product->documents) && ($product->documents->isNotEmpty())){
                        if(file_exists($documentDirectory.$product->documents[0]['name']))
                            unlink($documentDirectory.$product->documents[0]['name']);
                        $productDoc = AppDocument::where(['type' => 'product', 'item_id' => $product->id])->first();
                        $productDoc->title = $excelValue->document;
                        $productDoc->name = $docName;
                        $productDoc->save();
                    }else{
                        $productDoc['type'] = 'product';
                        $productDoc['item_id'] = $product->id;
                        $productDoc['title'] = $excelValue->document;
                        $productDoc['name'] = $docName;
                        $productDoc['status'] = '1';
                        if(!empty($productDoc))
                            AppDocument::create($productDoc); 
                    }
                }
            }
        }
        $redirectParameters = [];
        if($request->featured){
            $redirectParameters['featured'] = $request->featured;
        }
        if($request->sub){
            $redirectParameters['sub'] = $request->sub;
        }
        return redirect()->route('products.index', $redirectParameters)
            ->with('success',
             'Products successfully imported');
    }
}
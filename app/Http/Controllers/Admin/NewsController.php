<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\News;
use App\Models\NewsCategory;
use App\Models\Country;

use App\Helpers\Helper;

class NewsController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index() {
        $items = News::orderby('id', 'desc')->get();
        return view('admin.pages.news.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = NewsCategory::where(['status' => '1'])->get();
        $countries = Country::orderby('title', 'asc')->get();
        return view('admin.pages.news.create', compact('categories', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title'=>'required',
            'categories'=>'required'
            ]);

        $data['title'] = $request['title'];
        $slug = Helper::parseSaveURLString($request['title']);
        $data['slug'] = $slug;
        $data['description'] = $request['description'];
        $data['country_id'] = $request['country_id'];
        $data['start_date'] = $request['start_date'];
        $data['end_date'] = $request['end_date'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['image_alt'] = $request['image_alt'];

        $news = News::create($data);

        if(!empty($request['categories']))
            $news->categories()->sync($request['categories']);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/news/'.$news->id.'/'), $image);
            $news->image = $image;
            $news->save();
        }

        //Display a successful message upon save
        return redirect()->route('news.index')
            ->with('success', 'News,
             '. $news->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $news = News::findOrFail($id); //Find post of id = $id
        return view ('admin.pages.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $news = News::with(['categories'])->findOrFail($id);
        $categories = NewsCategory::where(['status' => '1'])->get();
        $countries = Country::orderby('title', 'asc')->get();
        return view('admin.pages.news.edit', compact('news', 'categories', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required',
            'categories'=>'required'
        ]);

        $news = News::findOrFail($id);
        $news->title = $request['title'];
        $slug = Helper::parseSaveURLString($request['title']);
        $news->slug = $slug;
        $news->description = $request['description'];
        $news->country_id = $request['country_id'];
        $news->start_date = $request['start_date'];
        $news->end_date = $request['end_date'];      
        $news->status = (isset($request['status']) ? $request['status'] : '0');
        $news->image_alt = $request['image_alt'];

        if(!empty($request['categories']))
            $news->categories()->sync($request['categories']);

        if(!empty($request['image'])){
            if($news->image){
                if(file_exists(public_path('/images/news/'.$news->id.'/').$news->image))
                    unlink(public_path('/images/news/'.$news->id.'/').$news->image);
            }
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/news/'.$news->id.'/'), $image);
            $news->image = $image;
        }

        $news->save();

        return redirect()->route('news.index')->with('success', 
            'News, '. $news->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $news = News::findOrFail($id);
        if($news->image){
            $imagesDirectory = public_path('/images/news/'.$news->id.'/');
            if(file_exists($imagesDirectory.$news->image))
                unlink($imagesDirectory.$news->image);
            //Remove Product Directory
            Helper::deleteDirectory($imagesDirectory);
        }
        if($news->document){
            $documentDirectory = public_path('/documents/news/'.$news->id.'/');
            if(file_exists($documentDirectory.$news->document))
                unlink($documentDirectory.$news->document);
            //Remove Product Directory
            Helper::deleteDirectory($documentDirectory);
        }
        $news->delete();

        return redirect()->route('news.index')
            ->with('success',
             'News successfully deleted');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories(Request $request) {
        $items = NewsCategory::all();
        return view('admin.pages.news.index_category', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCategory() {
        return view('admin.pages.news.create_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCategory(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title' => 'required'
            ]);
        $data['title'] = $request['title'];
        $slug = Helper::parseSaveURLString($request['title']);
        $data['slug'] = $slug;
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $newsCategory = NewsCategory::create($data);

        //Display a successful message upon save
        return redirect()->route('news-category.index')
            ->with('success', 'News Category,
             '. $newsCategory->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCategory($id) {
        $newsCategory = NewsCategory::findOrFail($id); //Find seo of id = $id
        return view ('admin.pages.news.show_category', compact('newsCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editCategory($id) {
        $newsCategory = NewsCategory::findOrFail($id);
        return view('admin.pages.news.edit_category', compact('newsCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCategory(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required'
        ]);

        $newsCategory = NewsCategory::findOrFail($id);
        $newsCategory->title = $request['title'];
        $newsCategory->status = (isset($request['status']) ? $request['status'] : '0');
        $newsCategory->save();

        return redirect()->route('news-category.index')->with('success', 
            'News Category, '. $newsCategory->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyCategory($id) {
        $newsCategory = NewsCategory::findOrFail($id);
        $newsCategory->delete();

        return redirect()->route('news-category.index')
            ->with('success',
             'News Category successfully deleted');
    }
}

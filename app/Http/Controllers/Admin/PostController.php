<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Post;
use App\Models\Tag;

use App\Helpers\Helper;

class PostController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = Post::orderby('id', 'desc')->get();
        return view('admin.pages.post.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $tags = Tag::where(['status' => '1'])->get();
        return view('admin.pages.post.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title'=>'required'
            ]);

        $data['title'] = $request['title'];
        $data['description'] = $request['description'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['image_alt'] = $request['image_alt'];

        $post = Post::create($data);

        if(!empty($request['tags']))
            $post->tags()->sync($request['tags']);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/post/'.$post->id.'/'), $image);
            $post->image = $image;
            $post->save();
        }

        //Display a successful message upon save
        return redirect()->route('posts.index')
            ->with('success', 'Post,
             '. $post->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $post = Post::findOrFail($id); //Find post of id = $id
        return view ('admin.pages.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $post = Post::with(['tags'])->findOrFail($id);
        $tags = Tag::where(['status' => '1'])->get();
        return view('admin.pages.post.edit', compact('post', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required'
        ]);

        $this->validate($request, [
            'title'=>'required'
            ]);

        $post = Post::findOrFail($id);
        $post->title = $request['title'];
        $post->slug = $request['slug'];
        $post->description = $request['description'];       
        $post->status = (isset($request['status']) ? $request['status'] : '0');
        $post->image_alt = $request['image_alt'];

        if(!empty($request['tags']))
            $post->tags()->sync($request['tags']);

        if(!empty($request['image'])){
            if($post->image){
                if(file_exists(public_path('/images/post/'.$post->id.'/').$post->image))
                    unlink(public_path('/images/post/'.$post->id.'/').$post->image);
            }
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/post/'.$post->id.'/'), $image);
            $post->image = $image;
        }

        $post->save();

        return redirect()->route('posts.index')->with('success', 
            'Post, '. $post->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $post = Post::findOrFail($id);
        if($post->image){
            $imagesDirectory = public_path('/images/post/'.$post->id.'/');
            if(file_exists($imagesDirectory.$post->image))
                unlink($imagesDirectory.$post->image);
            //Remove Product Directory
            Helper::deleteDirectory($imagesDirectory);
        }
        $post->delete();

        return redirect()->route('posts.index')
            ->with('success',
             'Post successfully deleted');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tags(Request $request) {
        $items = Tag::all();
        return view('admin.pages.post.index_tag', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createTag() {
        return view('admin.pages.post.create_tag');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeTag(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title' => 'required'
            ]);
        $data['title'] = $request['title'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $tag = Tag::create($data);

        //Display a successful message upon save
        return redirect()->route('post-tag.index')
            ->with('success', 'Post Tag,
             '. $tag->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showTag($id) {
        $tag = Tag::findOrFail($id); //Find seo of id = $id
        return view ('admin.pages.post.show_tag', compact('tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editTag($id) {
        $tag = Tag::findOrFail($id);
        return view('admin.pages.post.edit_tag', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateTag(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required'
        ]);

        $tag = Tag::findOrFail($id);
        $tag->title = $request['title'];
        $tag->slug = $request['slug'];
        $tag->status = (isset($request['status']) ? $request['status'] : '0');
        $tag->save();

        return redirect()->route('post-tag.index')->with('success', 
            'Post Tag, '. $tag->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyTag($id) {
        $tag = Tag::findOrFail($id);
        $tag->delete();

        return redirect()->route('post-tag.index')
            ->with('success',
             'Post Tag successfully deleted');
    }
}
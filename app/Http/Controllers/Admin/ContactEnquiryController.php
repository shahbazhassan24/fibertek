<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ContactEnquiry;
use Excel;

class ContactEnquiryController extends Controller
{
    public $leftMainActive;

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
        $this->leftMainActive = 'enquiries';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request) {
        $enquiries = ContactEnquiry::orderBy('updated_at', 'desc')->get();
        return view('admin.pages.enquiries.index', compact('enquiries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.enquiries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
            ]);
        $data['name'] = $request['name'];
        $data['email'] = $request['email'];
        $data['company'] = $request['company'];
        $data['phone'] = $request['phone'];
        $data['enquiry'] = $request['enquiry'];
        $data['hear'] = $request['hear'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $contactEnquiry = ContactEnquiry::create($data);

        //Display a successful message upon save
        return redirect()->route('contact-enquiries.index')
            ->with('success', 'Contact Enquiry,
             '. $contactEnquiry->name.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $contactEnquiry = ContactEnquiry::findOrFail($id);
        return view ('admin.pages.enquiries.show', compact('contactEnquiry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $contactEnquiry = ContactEnquiry::findOrFail($id);
        return view('admin.pages.enquiries.edit', compact('contactEnquiry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        $contactEnquiry = ContactEnquiry::findOrFail($id);
        $contactEnquiry->name = $request['name'];
        $contactEnquiry->email = $request['email'];
        $contactEnquiry->company = $request['company'];
        $contactEnquiry->phone = $request['phone'];
        $contactEnquiry->hear = $request['hear'];
        $contactEnquiry->enquiry = $request['enquiry'];
        $contactEnquiry->status = $request['status'];
        $contactEnquiry->save();

        return redirect()->route('contact-enquiries.index')->with('success', 
            'Contact Enquiry, '. $contactEnquiry->name.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $contactEnquiry = ContactEnquiry::findOrFail($id);
        $contactEnquiry->delete();

        return redirect()->route('contact-enquiries.index')
            ->with('success',
             'Contact Enquiry successfully deleted');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadExcel(Request $request, $type)
    {
        $allEnquiries = ContactEnquiry::select('name','company','email','phone','hear','enquiry','created_at')->orderBy('updated_at', 'desc')->get()->toArray();
        if(!empty($allEnquiries)){
            $enquiries = [];
            foreach($allEnquiries as $allEnquiryKey => $allEnquiry){
                $enquiries[$allEnquiryKey] = $allEnquiry;
            }
            return Excel::create('enquiries', function($excel) use ($enquiries) {
                $excel->sheet('all enquiries', function($sheet) use ($enquiries)
                {
                    $sheet->fromArray($enquiries);
                });
            })->download($type);
        }
        return redirect()->route('contact-enquiries.index')
            ->with('success',
             'No Contact Enquiry to retrive.');
    }
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Certification;

use App\Helpers\Helper;


class CertificationController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index() {
        $items = Certification::orderby('id', 'desc')->get();
        return view('admin.pages.certification.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.certification.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title and body field
        $this->validate($request, [
            'title'=>'required'
            ]);

        $data['title'] = $request['title'];
        $slug = Helper::parseSaveURLString($request['title']);
        $data['slug'] = $slug;
        $data['description'] = $request['description'];
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['image_alt'] = $request['image_alt'];

        $certification = Certification::create($data);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/certification/'.$certification->id.'/'), $image);
            $certification->image = $image;
            $certification->save();
        }

        if(!empty($request['document'])){
            $document = $request['document']->getClientOriginalName();
            $request['document']->move( public_path('/documents/certification/'.$certification->id.'/'), $document);
            $certification->document = $document;
            $certification->save();
        }

        //Display a successful message upon save
        return redirect()->route('certifications.index')
            ->with('success', 'Catalogue,
             '. $certification->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $certification = Certification::findOrFail($id); //Find post of id = $id
        return view ('admin.pages.certification.show', compact('certification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $certification = Certification::findOrFail($id);
        return view('admin.pages.certification.edit', compact('certification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title'=>'required'
        ]);

        $certification = Certification::findOrFail($id);
        $certification->title = $request['title'];
        $slug = Helper::parseSaveURLString($request['title']);
        $certification->slug = $slug;
        $certification->description = $request['description'];       
        $certification->status = (isset($request['status']) ? $request['status'] : '0');
        $certification->image_alt = $request['image_alt'];

        if(!empty($request['image'])){
            if($certification->image){
                if(file_exists(public_path('/images/certification/'.$certification->id.'/').$certification->image))
                    unlink(public_path('/images/certification/'.$certification->id.'/').$certification->image);
            }
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/certification/'.$certification->id.'/'), $image);
            $certification->image = $image;
        }

        if(!empty($request['document'])){
            if($certification->document){
                if(file_exists(public_path('/documents/certification/'.$certification->id.'/').$certification->document))
                    unlink(public_path('/documents/certification/'.$certification->id.'/').$certification->document);
            }
            $document = $request['document']->getClientOriginalName();
            $request['document']->move( public_path('/documents/certification/'.$certification->id.'/'), $document);
            $certification->document = $document;
        }

        $certification->save();

        return redirect()->route('certifications.index')->with('success', 
            'Catalogue, '. $certification->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $certification = Certification::findOrFail($id);
        if($certification->image){
            $imagesDirectory = public_path('/images/certification/'.$certification->id.'/');
            if(file_exists($imagesDirectory.$certification->image))
                unlink($imagesDirectory.$certification->image);
            //Remove Product Directory
            Helper::deleteDirectory($imagesDirectory);
        }
        if($certification->document){
            $documentDirectory = public_path('/documents/certification/'.$certification->id.'/');
            if(file_exists($documentDirectory.$certification->document))
                unlink($documentDirectory.$certification->document);
            //Remove Product Directory
            Helper::deleteDirectory($documentDirectory);
        }
        $certification->delete();

        return redirect()->route('certifications.index')
            ->with('success',
             'Catalogue successfully deleted');
    }
}
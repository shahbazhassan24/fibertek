<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Page;
use App\Models\Post;
use App\Models\Tag;
use App\Models\Career;
use App\Models\CareerCategory;
use App\Models\LifeFibertek;
use App\Models\ProjectReference;
use App\Models\Catalogue;
use App\Models\CatalogueCategory;
use App\Models\Certification;
use App\Models\Manual;
use App\Models\ManualCategory;
use App\Models\FAQ;
use App\Models\News;
use App\Models\NewsCategory;
use App\Models\Quote;
use App\Models\Subscriber;
use App\Models\Seo;

use App\Models\Product;
use App\Models\Category;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data['pageTitle'] = 'Dashboard';
        $data['pages'] = Page::count();
        $data['posts'] = Post::count();
        $data['tags'] = Tag::count();
        $data['jobs'] = Career::count();
        $data['projectReferences'] = ProjectReference::count();
        $data['catalogues'] = Catalogue::count();
        $data['certifications'] = Certification::count();
        $data['manuals'] = Manual::count();
        $data['news'] = News::count();
        $data['products'] = Product::count();
        $data['categories'] = Category::count();
        $data['quotes'] = Quote::count();
        $data['subscribers'] = Subscriber::count();
        $data['faqs'] = FAQ::count();
        $data['seo'] = Seo::count();

        //dd($data);
        //return redirect(route('admin-dashboard'))->with(Auth::logout());
        return view('admin.pages.dashboard.index', $data);
    }
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Menu;

use App\Helpers\Helper;

class MenuController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $menuQuery = Menu::whereIn('status', ['0', '1']);
        if($request->sub){
            $menuQuery->where('parent_id', $request->sub);
        }else{
            $menuQuery->where('parent_id', NULL);
        }
        $menus = $menuQuery->orderby('sort', 'asc')->paginate(20);
        return view('admin.pages.menu.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $allMenus = Menu::all();
        return view('admin.pages.menu.create', compact('allMenus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        //Validating title
        $this->validate($request, [
            'title'=>'required'
            ]);

        $data['title'] = $request['title'];
        $data['parent_id'] = $request['parent_id'];
        $data['url'] = $request['url'];
        $data['new_tab'] = (isset($request['new_tab']) ? $request['new_tab'] : '0');
        $data['short_description'] = $request['short_description'];
        $data['sort'] = (isset($request['sort']) ? $request['sort'] : '0');
        $data['status'] = (isset($request['status']) ? $request['status'] : '0');
        $data['image_alt'] = $request['image_alt'];

        $menu = Menu::create($data);

        if(!empty($request['image'])){
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/menu/'.$menu->id.'/'), $image);
            $menu->image = $image;
            $menu->save();
        }

        //Display a successful message upon save
        $subCat = [];
        if(isset($_GET['sub']))
            $subCat = ['sub' => $_GET['sub']];
        return redirect()->route('menus.index', $subCat)
            ->with('success', 'Menu,
             '. $menu->title.' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $menu = Menu::findOrFail($id); //Find menu of id = $id
        return view ('admin.pages.menu.show', compact('.menu.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $menu = Menu::findOrFail($id);
        $allMenus = Menu::all();
        return view('admin.pages.menu.edit', compact('menu', 'allMenus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //Validating title
        $this->validate($request, [
            'title'=>'required|max:100'
            ]);

        $menu = Menu::findOrFail($id);

        $menu->title = $request['title'];
        $menu->parent_id = $request['parent_id'];
        $menu->url = $request['url'];
        $menu->short_description = $request['short_description'];
        $menu->new_tab = (isset($request['new_tab']) ? $request['new_tab'] : '0');
        $menu->sort = $request['sort'];
        $menu->status = (isset($request['status']) ? $request['status'] : '0');
        $menu->image_alt = $request['image_alt'];

        if(!empty($request['image'])){
            if($menu->image){
                if(file_exists(public_path('/images/menu/'.$menu->id.'/').$menu->image))
                    unlink(public_path('/images/menu/'.$menu->id.'/').$menu->image);
            }
            $image = $request['image']->getClientOriginalName();
            $request['image']->move( public_path('/images/menu/'.$menu->id.'/'), $image);
            $menu->image = $image;
        }

        $menu->save();

        $subCat = [];
        if(isset($_GET['sub']))
            $subCat = ['sub' => $_GET['sub']];
        return redirect()->route('menus.index', $subCat)->with('success', 
            'Menu, '. $menu->title.' updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $menu = Menu::findOrFail($id);
        //Remove Images
        $imagesDirectory = public_path('/images/menu/'.$menu->id.'/');
        if($menu->image){
            if(file_exists($imagesDirectory.$menu->image))
                unlink($imagesDirectory.$menu->image);
        }

        //Remove Menu Directory
        if(file_exists($imagesDirectory))
        Helper::deleteDirectory($imagesDirectory);

        $menu->delete();

        $subCat = [];
        if(isset($_GET['sub']))
            $subCat = ['sub' => $_GET['sub']];
        return redirect()->route('menus.index', $subCat)
            ->with('success',
             'Menu successfully deleted');
    }
}
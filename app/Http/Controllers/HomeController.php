<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Banner as AppBanner;
use App\Models\HomeExploreProduct;
use App\Models\HomeNewsHighlight;

//Helper
use App\Helpers\Helper;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$homeCategories = Helper::getProductCategories(['status' => '1', 'parent_id' => NULL])->chunk(4, true);
        $featuredProducts = Product::where(['status' => '1', 'is_featured' => '1'])->take(5)->orderBy('sort', 'asc')->get();
        $homeBanners = AppBanner::where(['status' => '1', 'type' => 'home'])->orderBy('sort', 'asc')->get();
        $homeCategories = HomeExploreProduct::where(['status' => '1'])->orderBy('sort', 'asc')->get()->chunk(4, true);
        $homeNewsHighlights = HomeNewsHighlight::where(['status' => '1'])->orderBy('sort', 'asc')->take(2)->get();

        return view('frontend.pages.home.index', compact('homeBanners', 'homeCategories', 'featuredProducts', 'homeNewsHighlights'));
    }
}
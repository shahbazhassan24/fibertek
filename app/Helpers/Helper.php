<?php
namespace App\Helpers;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Storage;

use App\Models\Role;
use App\Models\Category;
use App\Models\Page;
use App\Models\Configuration;
use App\Models\Seo;
use App\Models\Country;
use App\Models\Menu;

class Helper
{
    public static function getRoles()
    {
        return Role::orderBy('name')->get();
    }

    public static function getPages( $whereArray = ['status' => '1'])
    {
        $pages = Page::select(['id', 'title', 'slug', 'status'])->where($whereArray)->orderBy('id')->get()->toArray();
        return $pages;
    }

    public static function getParentMenu( $whereArray = ['status' => '1', 'parent_id' => NULL])
    {
        $menus = Menu::where($whereArray)->where('id', '!=', '1')->orderBy('sort', 'asc')->get();
        return $menus;
    }

    public static function getMenuItem( $menuItem = '1', $key = 'title' )
    {
        $menuItem = Menu::select($key)->where('id', '=', $menuItem)->first();
        return $menuItem;
    }

    public static function getProductCategories( $whereArray = ['status' => '1'])
    {
        $categories = Category::where($whereArray)->orderBy('sort', 'asc')->get();
        return $categories;
    }

    public static function getPageSlugs( $whereArray = ['status' => '1'])
    {
        $allPages = self::getPages();
        $pages = [];
        foreach($allPages as $page){
            $pages[$page['id']]['title'] = $page['title'];
            $pages[$page['id']]['slug'] = $page['slug'];
            $pages[$page['id']]['status'] = $page['status'];
        }
        return $pages;
    }

    public static function getConfigurations()
    {
        $allConfigurations = Configuration::select('key_name', 'value')->get()->toArray();
        $configurations = [];
        foreach( $allConfigurations as $configuration ){
            $configurations[$configuration['key_name']] = $configuration['value'];
        }
        return $configurations;         
    }

    public static function getConfigurationByKey($key)
    {
        if(!$key)
            return false;

        return Configuration::where(['key_name' => $key])->select('value')->first()->value;        
    }

    public static function getSeoInformation($url)
    {
        if(!$url)
            return false;
        return Seo::where(['url' => $url])->first();        
    }

    public static function getCountries()
    {
        return Country::all();        
    }

    public static function getCountryByID($country)
    {
        return Country::find($country);        
    }

    public static function parseSaveURLString($sString)
    {
        $pattern = array("'é'", "'è'", "'ë'", "'ê'", "'É'", "'È'", "'Ë'", "'Ê'", "'á'", "'à'", "'ä'", "'â'", "'å'", "'Á'", "'À'", "'Ä'", "'Â'", "'Å'", "'ó'", "'ò'", "'ö'", "'ô'", "'Ó'", "'Ò'", "'Ö'", "'Ô'", "'í'", "'ì'", "'ï'", "'î'", "'Í'", "'Ì'", "'Ï'", "'Î'", "'ú'", "'ù'", "'ü'", "'û'", "'Ú'", "'Ù'", "'Ü'", "'Û'", "'ý'", "'ÿ'", "'Ý'", "'ø'", "'Ø'", "'œ'", "'Œ'", "'Æ'", "'ç'", "'Ç'");
        $replace = array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E', 'a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A', 'A', 'o', 'o', 'o', 'o', 'O', 'O', 'O', 'O', 'i', 'i', 'i', 'I', 'I', 'I', 'I', 'I', 'u', 'u', 'u', 'u', 'U', 'U', 'U', 'U', 'y', 'y', 'Y', 'o', 'O', 'a', 'A', 'A', 'c', 'C');
        $sString = preg_replace($pattern, $replace, $sString);
        $sTemp = strtolower($sString);
        $sTemp = preg_replace('/\W+/',' ',$sTemp);
        $sTemp = preg_replace('/\s+/','-',trim($sTemp));
        
        $sTemp = urlencode($sTemp);
        
        return $sTemp;
    }

    public static function uniqueArray( $array ){
        return array_map("unserialize", array_unique(array_map("serialize", $array)));
    }

    public static function getDateDifference( $date ){
        return Carbon::createFromTimeStamp(strtotime($date))->timezone(\Config::get('constants.TIME_ZONE'))->diffForHumans();
    }

    public static function get_local_time(){
        $ip = self::getUserIpAddr();
        $url = 'http://ip-api.com/json/'.$ip;
        $tz = file_get_contents($url);
        $tz = json_decode($tz,true);
        return $tz;
    }

    public static function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function getMimeType($filename) {
        $idx = explode( '.', $filename );
        $count_explode = count($idx);
        $idx = strtolower($idx[$count_explode-1]);

        $mimet = array( 
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',


            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        if (isset( $mimet[$idx] )) {
         return $mimet[$idx];
        } else {
         return 'application/octet-stream';
        }
    }

    public static function array_search_inner ($array, $attr, $val, $strict = FALSE) {
        // Error is input array is not an array
        if (!is_array($array)) return FALSE;
        // Loop the array
        foreach ($array as $key => $inner) {
          // Error if inner item is not an array (you may want to remove this line)
          if (!is_array($inner)) return FALSE;
          // Skip entries where search key is not present
          if (!isset($inner[$attr])) continue;
          if ($strict) {
            // Strict typing
            if ($inner[$attr] === $val) return $key;
          } else {
            // Loose typing
            if ($inner[$attr] == $val) return $key;
          }
        }
        // We didn't find it
        return NULL;
      }

    public static function getYoutubeEmbedUrl($url)
    {
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }

        if (preg_match($shortUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        return 'https://www.youtube.com/embed/' . $youtube_id ;
    }

    public static function getSocialShareLink($url, $platform = 'facebook')
    {
        $shareLink = '';
        if($platform == 'facebook'){
            $shareLink = 'http://www.facebook.com/sharer.php?u='.$url;
        }elseif($platform == 'twittter'){
            $shareLink = 'https://twitter.com/share?url='.$url;
        }elseif($platform == 'linkedin'){
            $shareLink = 'http://www.linkedin.com/shareArticle?mini=true&url='.$url;
        }elseif($platform == 'google'){
            $shareLink = 'https://plus.google.com/share?url='.$url;
        }elseif($platform == 'wechat'){
            $shareLink = 'https://www.addtoany.com/add_to/wechat?linkurl='.$url;
        }elseif($platform == 'kakao'){
            $shareLink = 'https://www.addtoany.com/add_to/kakao?linkurl='.$url;
        }elseif($platform == 'lineit'){
            $shareLink = 'https://lineit.line.me/share/ui?url='.$url;
        }elseif($platform == 'whatsapp'){
            $shareLink = 'https://api.whatsapp.com/send?phone=&text='.$url.'&source=&data=';
        }
        return $shareLink;
    }

    public static function getImageAlt($title = '', $custom = false)
    {
        if(!$title)
            return '';
            
        if(strrpos($title, "."))
            $title = substr($title, 0 , (strrpos($title, ".")));

        if($custom){
            $titleParts = explode('-', $title);
            unset($titleParts[0]);
            $title = implode(' ', $titleParts);
        }
        $title = str_replace(['-', '_'], ' ', $title);
            
        return $title;
    }

    public static function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    public static function deleteDirectory( $path ) {
        File::cleanDirectory( $path );
        Storage::deleteDirectory( $path );
        if(file_exists($path))
            rmdir( $path );
   }
}
?>
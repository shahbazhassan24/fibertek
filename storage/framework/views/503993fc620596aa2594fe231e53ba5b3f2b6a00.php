<?php $__env->startSection('content'); ?>
<div class="events_area">
    <div class="events_holder">
        <header class="sec_header add">
            <?php echo $category->description; ?>

        </header>
        <div id="content" class="fluid main-category">
            <div class="tab-content">
                <div id="tab1" class="tab active">
                    <?php if($category->subCategories): ?>
                        <?php $__currentLoopData = $category->subCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($subCategory->status): ?>
                    <div class="event">
                        <div class="event_holder">
                            <a href="<?php echo e(route('category', $subCategory->slug)); ?>" class="detail_btn">View Products</a>
                            <?php if($subCategory->image): ?>
                            <div class="img_holder">
                                <a href="<?php echo e(route('category', $subCategory->slug)); ?>">
                                    <img src="<?php echo e(asset('/images/category/'.$subCategory->id.'/'.$subCategory->image)); ?>" alt="<?php echo e((isset($subCategory->images[0])) ? json_decode($subCategory->images[0]->alt_text)->image : ''); ?>" class="img-responsive max_width">
                                </a>
                            </div>
                            <?php endif; ?>
                            <div class="txt add <?php echo e((!$subCategory->image) ? 'no-image' : ''); ?>">
                                <a href="<?php echo e(route('category', $subCategory->slug)); ?>">
                                    <h2><?php echo e($subCategory->title); ?></h2>
                                </a>
                                <p><?php echo $subCategory->short_description; ?></p>
                            </div>
                        </div>
                    </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/pages/category.blade.php ENDPATH**/ ?>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>
	<?php if(isset($seoInformation) && $seoInformation): ?>
		<?php echo e($seoInformation->meta_title); ?>

	<?php else: ?>
		<?php if(isset($metaTitle)): ?>
			<?php echo e(Helper::getConfigurationByKey('meta_name_keywords') | Helper::getConfigurationByKey('web_title')); ?>

		<?php else: ?>
			<?php echo e('FiberTek | '.Helper::getConfigurationByKey('web_title')); ?>

		<?php endif; ?>
	<?php endif; ?>
</title>

<meta name="allow-search" content="Yes">
<meta name="ROBOTS" content = "All">
<?php if(isset($seoInformation) && $seoInformation): ?>
<meta name="keywords" content="<?php echo e($seoInformation->meta_keywords); ?>" />
<meta name="description" content="<?php echo e($seoInformation->meta_description); ?>" />
<?php else: ?>
<meta name="keywords" content="<?php echo e(Helper::getConfigurationByKey('meta_name_keywords')); ?>" />
<meta name="description" content="<?php echo e(Helper::getConfigurationByKey('meta_name_description')); ?>" />
<?php endif; ?>
<!-- Favicon icon -->
<link rel="icon" href="<?php echo e(asset('/images/').'/'.Helper::getConfigurationByKey('web_ico')); ?>" type="image/x-icon">
<!-- CSRF Token -->
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<base href="<?php echo e(url('/').'/'); ?>" />

<link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/addtocalendar.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/font-awesome.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/slick.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/slick-theme.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/all.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('frontend/css/responsive.css')); ?>">
<?php echo Helper::getConfigurationByKey('custom_script'); ?>

<meta name="google-translate-customization" content="e4f8ddcdeb86d-900e4f5239d48169-gbc9772411070dec6-1c"></meta>
<?php echo ReCaptcha::htmlScriptTagJsApi(); ?><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/includes/head.blade.php ENDPATH**/ ?>
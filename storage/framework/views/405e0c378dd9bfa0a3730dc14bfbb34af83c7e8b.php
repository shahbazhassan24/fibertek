<?php $__env->startSection('content'); ?>
<div class="product_upper">
    <div class="container">
        <?php if($category->video_link): ?>
        <div class="vid_holder">
            <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="400" height="230" type="text/html" src="<?php echo Helper::getYoutubeEmbedUrl($category->video_link); ?>?autoplay=0&start=0&end=0" allowfullscreen></iframe>
        </div>
        <?php elseif($category->demonstration_image): ?>
        <div class="vid_holder">
            <!-- <a href="#"> -->
                <img src="<?php echo e(asset('/images/category/'.$category->id.'/'.$category->demonstration_image)); ?>" alt="<?php echo e((isset($subCategory->images[0])) ? json_decode($subCategory->images[0]->alt_text)->demonstration : ''); ?>" class="img-responsive">
            <!-- </a> -->
        </div>
        <?php endif; ?>
        <div class="text <?php echo e((!$category->video_link) ? 'no-video' : ''); ?> <?php echo e((!$category->demonstration_image) ? 'no-image' : ''); ?>">
            <h2><?php echo e($category->title); ?></h2>
            <?php echo $category->description; ?>

        </div>
    </div>
</div>
<?php if($category->subCategories->isNotEmpty()): ?>
<div class="events_area">
    <div class="events_holder">
        <div id="content" class="fluid main-category">
            <div class="tab-content">
                <div id="tab1" class="tab active">
                    <?php $__currentLoopData = $category->subCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($subCategory->status): ?>
                    <div class="event">
                        <div class="event_holder">
                            <a href="<?php echo e(route('category', $subCategory->slug)); ?>" class="detail_btn">View Products</a>
                            <?php if($subCategory->image): ?>
                            <div class="img_holder">
                                <a href="<?php echo e(route('category', $subCategory->slug)); ?>">
                                    <img src="<?php echo e(asset('/images/category/'.$subCategory->id.'/'.$subCategory->image)); ?>" alt="<?php echo e((isset($subCategory->images[0])) ? json_decode($subCategory->images[0]->alt_text)->image : ''); ?>" class="img-responsive max_width">
                                </a>
                            </div>
                            <?php endif; ?>
                            <div class="txt add <?php echo e((!$subCategory->image) ? 'no-image' : ''); ?>">
                                <a href="<?php echo e(route('category', $subCategory->slug)); ?>">
                                    <h2><?php echo e($subCategory->title); ?></h2>
                                </a>
                                <p><?php echo $subCategory->short_description; ?></p>
                            </div>
                        </div>
                    </div>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<?php if($category->products->isNotEmpty()): ?>
<div class="rack_products sub-category">
    <div class="container">
        <div class="rac_head">
            <select name="sort" id="product-sort">
                <option value="">Sort By</option>
                <option value="name-asc">Sort by Name A to Z</option>
                <option value="name-desc">Sort by Name Z to A</option>
                <option value="added-asc">Oldest to Newest</option>
                <option value="added-desc">Newest to Oldest</option>
            </select>
            <strong>Products</strong>
        </div>
        <ul class="products_list list_none">
            <?php $__currentLoopData = $category->products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($product->status): ?>
            <li data-time="<?php echo e(strtotime($product->created_at)); ?>">
                <?php if($product->images->isNotEmpty()): ?>
                    <a href="<?php echo e(route('product', $product->slug)); ?>">
                        <img src="<?php echo e(asset('/images/product/'.$product->id.'/'.$product->images[0]->name)); ?>" alt="<?php echo e(($product->images[0]->alt_text) ? $product->images[0]->alt_text : Helper::getImageAlt($product->images[0]->name)); ?>">
                    </a>
                <?php endif; ?>
                <strong><a href="<?php echo e(route('product', $product->slug)); ?>"><?php echo e($product->title); ?></a></strong>
            </li>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
</div>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/pages/category-sub.blade.php ENDPATH**/ ?>
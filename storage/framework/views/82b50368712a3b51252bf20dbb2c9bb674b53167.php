<script type="text/javascript">
    var appJSUrl = '<?php echo e(route('home')); ?>';
    var appAssetsUrl = '<?php echo e(asset('/')); ?>';
</script>
<script type="text/javascript" src="<?php echo e(asset('frontend/js/atc.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('frontend/js/jquery-3.2.1.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('frontend/js/slider.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('frontend/js/slick.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('frontend/js/jquery.validate.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('frontend/js/custom.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('frontend/js/masonary.js')); ?>"></script><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/includes/scripts.blade.php ENDPATH**/ ?>
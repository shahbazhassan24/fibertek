<div class="banner" style="background:#000 url(<?php echo e((isset($banner) && $banner) ? $banner : ''); ?>) no-repeat;">
<?php if((isset($banner) && $banner) && (isset($banner_alt) && $banner_alt)): ?>
    <img src="<?php echo e(str_replace('/images/', '/images/background/', $banner)); ?>" alt="<?php echo e($banner_alt); ?>" class="hidden" />
<?php endif; ?>
    <div class="d_table">
        <div class="v_middle">
            <div class="container">
                <div class="banner_caption <?php echo e(isset($category_banner) ? 'light' : ''); ?> ">
                    <?php if(isset($title) && $title): ?>
                    <h1 class="<?php echo e(!isset($category_banner) ? 'text-center' : ''); ?>"><?php echo e($title); ?></h1>
                    <?php endif; ?>
                    <?php if(isset($banner_description) && $banner_description): ?>
                    <p><?php echo $banner_description; ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/includes/banner.blade.php ENDPATH**/ ?>
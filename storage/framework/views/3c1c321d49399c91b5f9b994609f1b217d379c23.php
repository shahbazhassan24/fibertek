<header id="header">
    <div class="top_header">
        <div class="container">
            <div class="left">
                <p><?php echo $configuration['tagline']; ?></p>
                <a href="<?php echo $configuration['online_shop_link']; ?>" class="btn_cart mobile_view" target="_blank"><img src="<?php echo e(asset('frontend/images/icon_5.png')); ?>" alt="FiberTek Online Shop"> Our Online Shop</a>
            </div>
            <div class="right">
                <span>
                    <img src="<?php echo e(asset('frontend/images/icon_1.png')); ?>" alt="FiberTek Contact Number">
                    <a href="tel:<?php echo str_replace(' ', '', $configuration['contact_phone']); ?>" target="_blank"><?php echo $configuration['contact_phone']; ?></a>
                </span>
                <span class="ewrap">
                    <img src="<?php echo e(asset('frontend/images/icon_2.png')); ?>" alt="FiberTek Email Address">
                    <a href="mailto:<?php echo $configuration['contact_email']; ?>" target="_blank"><?php echo $configuration['contact_email']; ?></a>
                </span>
                <span class="langwrap">
                    <div id="google_translate_element"></div>
                    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                </span>
                <span class="desktop_view">
                    <img src="<?php echo e(asset('frontend/images/icon_4.png')); ?>" alt="FiberTek Sign In">
                    <?php if(!Auth::check()): ?>
                    <a href="<?php echo e(route('user.login')); ?>">Sign in</a>
                    <?php else: ?>
                    <a href="<?php echo e(route('user-dashboard')); ?>">My Account</a>
                    |
                    <a href="<?php echo e(route('get.logout')); ?>">Logout</a>
                    <?php endif; ?>
                </span>
                <span class="cart_wrap">
                    <?php ($requestQuoteProducts = request()->session()->get('request_quote_products')); ?>
                    <a id="cart" href="#" class="btncart"><img src="<?php echo e(asset('frontend/images/cart-enq-icon.png')); ?>" alt="FiberTek Online Shop"><span class="cart-count"><?php echo e(($requestQuoteProducts) ? count($requestQuoteProducts) : '0'); ?></span></a>
                    <div class="shopping-cart">
                        <ul class="shopping-cart-items">
                            <?php if($requestQuoteProducts): ?>
                                <?php $__currentLoopData = $requestQuoteProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $quoteProduct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li>
                                    <div class="cart-img">
                                        <a href="<?php echo e(route('product', $quoteProduct['slug'])); ?>">
                                            <img src="<?php echo e(asset('/images/product/'.$quoteProduct['id'].'/'.$quoteProduct['image'])); ?>" alt="<?php echo e(($quoteProduct['image_alt']) ? $quoteProduct['image_alt'] : Helper::getImageAlt($quoteProduct['image'])); ?>">
                                        </a>
                                    </div>
                                    <div class="cart-title">
                                        <span class="item-name"><a href="<?php echo e(route('product', $quoteProduct['slug'])); ?>"><?php echo e($quoteProduct['title']); ?></a></span>
                                    </div>
                                    <div class="cart-remove">
                                        <button type="button" class="item-delete remove-request-a-quote" data-product-id="<?php echo e($quoteProduct['id']); ?>">&#10005;</button>
                                    </div>
                                </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </ul>
                        <div class="cart_btn_wrap">
                            <?php if($requestQuoteProducts): ?>
                            <a href="./page/enquiry-cart" class="cart-btn">Send Enquiry</a>
                            <?php else: ?>
                            <a href="#" class="cart-btn btn-no-item-request-a-quote">No Request Quote</a>
                            <?php endif; ?>
                        </div>
                    </div> <!--end shopping-cart -->
                </span>
            </div>
        </div>
    </div>
    <div class="nav_holder">
        <div class="container">
            <a href="<?php echo e(route('home')); ?>" class="logo"><img src="<?php echo e(asset('/images/').'/'.$configuration['web_logo']); ?>" alt="<?php echo e($configuration['web_logo_alt']); ?>"></a>
            <a href="#" class="nav_opener"><i class="fa fa-bars" aria-hidden="true"></i><span>Menu</span></a>
            <nav id="nav">
                <ul class="list_none">
                    <li>
                        <?php ($productMenuItem = Helper::getMenuItem('1', 'title')); ?>
                        <a href="#"><?php echo e($productMenuItem->title); ?></a>
                        <div class="four_cols fluid">
                            <div class="holder">
                                <div class="left_side tabs">
                                    <?php if($mainCategories->isNotEmpty()): ?>
                                    <ul class="list_none">
                                        <?php $__currentLoopData = $mainCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mainKey =>  $mainCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($mainCategory->status): ?>
                                        <li class="<?php echo e((!$mainKey) ? 'active' : ''); ?>"><a href="<?php echo e(route('category', $mainCategory->slug)); ?>" data-tab-link="#<?php echo e($mainCategory->slug); ?>"><?php echo e($mainCategory->title); ?></a></li>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                    <?php endif; ?>
                                </div>
                                <div class="menu_holder">
                                    <a href="#" class="back_tray"><i class="fa fa-angle-left" aria-hidden="true"></i> Back</a>
                                    <div class="tab-content">
                                        <?php if($mainCategories->isNotEmpty()): ?>
                                        <?php $__currentLoopData = $mainCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mainKey => $mainCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($mainCategory->status): ?>
                                        <div id="<?php echo e($mainCategory->slug); ?>" class="tab <?php echo e((!$mainKey) ? 'active' : ''); ?>">
                                            <div class="four_cols three">
                                                <div class="holder">
                                                    <?php if(count($mainCategory->subCategories)): ?>
                                                    <?php $__currentLoopData = $mainCategory->subCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subKey => $subCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($subCategory->status): ?>
                                                    <div class="col">
                                                        <div class="image-holder">
                                                            <div class="col-icon">
                                                                <img src="<?php echo e(asset('/images/category/'.$subCategory->id.'/'.$subCategory->image)); ?>" alt="<?php echo e((isset($subCategory->images[0])) ? json_decode($subCategory->images[0]->alt_text)->image : ''); ?>" class="img">
                                                            </div>
                                                        </div>
                                                        <div class="col-information">
                                                            <div class="txt">
                                                                <strong><a href="<?php echo e(route('category', $subCategory->slug)); ?>"><?php echo e($subCategory->title); ?></a></strong>
                                                                <?php if(count($subCategory->subCategories)): ?>
                                                                <?php $__currentLoopData = $subCategory->subCategories->take(5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subSubKey => $subSubCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($subSubCategory->status): ?>
                                                                <span><a href="<?php echo e(route('category', $subSubCategory->slug)); ?>"><?php echo e($subSubCategory->title); ?></a></span>
                                                                <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if(count($subCategory->subCategories) > 5): ?>
                                                                <!-- <span class="view-more-categories"><a href="<?php echo e(route('category', $subCategory->slug)); ?>">View More >></a></span> -->
                                                                <?php endif; ?>
                                                                <?php elseif(count($subCategory->products)): ?>
                                                                <?php $__currentLoopData = $subCategory->products->take(5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productKey => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($product->status): ?>
                                                                <span><a href="<?php echo e(route('product', $product->slug)); ?>"><?php echo e($product->title); ?></a></span>
                                                                <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if(count($subCategory->products) > 5): ?>
                                                                <!-- <span class="view-more-categories"><a href="<?php echo e(route('category', $subCategory->slug)); ?>">View More >></a></span> -->
                                                                <?php endif; ?>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php ($parentMenuItems = Helper::getParentMenu()); ?>
                    <?php if($parentMenuItems->isNotEmpty()): ?>
                    <?php $__currentLoopData = $parentMenuItems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parentMenuItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                        <a href="<?php echo e($parentMenuItem->url); ?>" <?php if($parentMenuItem->new_tab): ?> target="_blank" <?php endif; ?>><?php echo e($parentMenuItem->title); ?></a>
                        <?php if($parentMenuItem->subMenus->isNotEmpty()): ?>
                        <div class="four_cols three vertical-menu-item">
                            <div class="holder">
                                <?php $__currentLoopData = $parentMenuItem->subMenus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subMenuItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(($subMenuItem['status'] == '1')): ?>
                                <div class="col">
                                    <a href="<?php echo e($subMenuItem->url); ?>" class="ico" <?php if($subMenuItem->new_tab): ?> target="_blank" <?php endif; ?>>
                                       <div class="col-icon" style="background-image: url(<?php echo e(asset('/images/menu/'.$subMenuItem->id.'/'.$subMenuItem->image)); ?>)"></div>
                                        <?php if($subMenuItem->image_alt): ?>
                                            <img src="<?php echo e(str_replace('/images/', '/images/background/', asset('/images/menu/'.$subMenuItem->id.'/'.$subMenuItem->image))); ?>" alt="<?php echo e($subMenuItem->image_alt); ?>" class="hidden" />
                                        <?php endif; ?> 
                                    </a>
                                    <div class="col-information">
                                        <strong><a href="<?php echo e($subMenuItem->url); ?>" <?php if($subMenuItem->new_tab): ?> target="_blank" <?php endif; ?>><?php echo e($subMenuItem->title); ?></a></strong>
                                        <p><?php echo $subMenuItem->short_description; ?></p>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </ul>
            </nav>
            <div class="align_right">
                <form class="search_form" action="<?php echo e(route('search')); ?>">
                    <input type="search" placeholder="Search on FiberTek..." value="<?php if(isset($_REQUEST['search'])): ?><?php echo e($_REQUEST['search']); ?><?php endif; ?>" name="search">
                    <input type="submit" value="Search">
                </form>
                <span class="mobile_view imgwrap">
                    <div class="userlogin-mobile" role="navigation">
                        <ul>
                            <li><img src="<?php echo e(asset('frontend/images/icon_4.png')); ?>" alt="FiberTek Sign In">
                                <ul class="dropdown">
                                    <?php if(!Auth::check()): ?>
                                    <li><a href="<?php echo e(route('user.login')); ?>">Sign in</a></li>
                                    <?php else: ?>
                                    <li><a href="<?php echo e(route('user-dashboard')); ?>"><?php echo e(Auth::user()->name); ?></a></li>					
                                    <li><a href="<?php echo e(route('get.logout')); ?>">Logout</a></li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </span>
                <a href="<?php echo $configuration['online_shop_link']; ?>" class="btn_cart desktop_view" target="_blank"><img src="<?php echo e(asset('frontend/images/icon_5.png')); ?>" alt="FiberTek Online Shop"> Our Online Shop</a>
            </div>
        </div>
    </div>
</header><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/includes/header.blade.php ENDPATH**/ ?>
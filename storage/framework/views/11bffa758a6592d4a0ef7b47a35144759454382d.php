<?php $__env->startSection('content'); ?>
<div class="container blog">
    <div class="text">
        <h1><?php echo e($news->title); ?></h1>
        <span><?php echo e(strtoupper($news->created_at->format('jS M Y'))); ?></span>
        <?php if($news->image): ?>
        <div class="featured-image">
            <img src="<?php echo e(asset('/images/news/'.$news->id.'/'.$news->image)); ?>" alt="<?php echo e($news->image_alt); ?>">
        </div>
        <?php endif; ?>
        <?php echo $news->description; ?>

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/pages/news-details.blade.php ENDPATH**/ ?>
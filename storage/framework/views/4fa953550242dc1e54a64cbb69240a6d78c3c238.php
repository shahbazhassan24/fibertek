<footer id="footer">
    <div class="container cols">
        <?php if(Helper::getConfigurationByKey('footer_links_1')): ?>
        <div class="col">
            <?php echo Helper::getConfigurationByKey('footer_links_1'); ?>

        </div>
        <?php endif; ?>
        <?php if(Helper::getConfigurationByKey('footer_links_2')): ?>
        <div class="col">
            <?php echo Helper::getConfigurationByKey('footer_links_2'); ?>

        </div>
        <?php endif; ?>
        <?php if(Helper::getConfigurationByKey('footer_links_3')): ?>
        <div class="col">
            <?php echo Helper::getConfigurationByKey('footer_links_3'); ?>

        </div>
        <?php endif; ?>
        <?php if(Helper::getConfigurationByKey('footer_links_4')): ?>
        <div class="col">
            <?php echo Helper::getConfigurationByKey('footer_links_4'); ?>

        </div>
        <?php endif; ?>
    </div>
    <div class="rights">
        <?php echo $configuration['copyright']; ?>

    </div>
</footer>
<a href="#" id="back-to-top-scroll"><span></span></a><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/includes/footer.blade.php ENDPATH**/ ?>
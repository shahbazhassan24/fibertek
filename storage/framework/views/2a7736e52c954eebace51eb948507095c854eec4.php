<div class="container">
    <?php if ($errors->any()) { ?>
    <div class="alert alert-danger">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <?php foreach ($errors->all() as $error) { ?>
            <?=$error ?><br/>
        <?php } ?>
    </div>
    <?php
    }
    if (Session::has('error_message')) { ?>
    <div class="alert alert-danger">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <?php echo Session::get('error_message') ?>
    </div>
    <?php } 
    if (Session::has('success_message')) { ?>
    <div class="alert alert-success">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <?php echo Session::get('success_message') ?>
    </div>
    <?php } ?>
</div><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/includes/error_messages.blade.php ENDPATH**/ ?>
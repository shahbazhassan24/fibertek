<div class="newsletter_area">
    <div class="col">
        <div class="holder">
            <div class="d_table">
                <div class="v_middle">
                    <h2>Follow Us</h2>
                    <p>Stay up to date with the latest FiberTek news. Follow and connect with us on social media.</p>
                    <ul class="list_none socials">
                        <!-- class="active" -->
                        <?php if(isset($configuration['facebook_link']) && $configuration['facebook_link']): ?>
                        <li><a href="<?php echo $configuration['facebook_link']; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <?php endif; ?>
                        <?php if(isset($configuration['twitter_link']) && $configuration['twitter_link']): ?>
                        <li><a href="<?php echo $configuration['twitter_link']; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <?php endif; ?>
                        <?php if(isset($configuration['youtube_link']) && $configuration['youtube_link']): ?>
                        <li><a href="<?php echo $configuration['youtube_link']; ?>" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        <?php endif; ?>
                        <?php if(isset($configuration['instagram_link']) && $configuration['instagram_link']): ?>
                        <li><a href="<?php echo $configuration['instagram_link']; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <?php endif; ?>
                        <?php if(isset($configuration['linkedin_link']) && $configuration['linkedin_link']): ?>
                        <li><a href="<?php echo $configuration['linkedin_link']; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <form class="mail_form" id="join-list-form">
            <strong class="title"><i class="fa fa-envelope" aria-hidden="true"></i> JOIN OUR MAILING LIST</strong>
            <div class="fields">
                <input type="text" placeholder="Company Name" name="company">
                <input type="text" placeholder="Name *" name="name" required>
                <input type="email" placeholder="Email Address *" name="email" required>
                <select name="country_id" class="form-control" required>
                    <option value="">Country</option>
                    <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($country->id); ?>" <?php echo e(old('country_id') ==  $country->id ? 'selected' : ''); ?>><?php echo e($country->title); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <div class="submit-btn">
                    <input type="submit" value="Subscribe" class="btn-ajax-submit">
                    <div class="ajax-loader hide"></div>
                </div>
                <div class="success-message">Request has been submitted!</div>
            </div>
        </form>
    </div>
</div><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/includes/follow_newsletter.blade.php ENDPATH**/ ?>
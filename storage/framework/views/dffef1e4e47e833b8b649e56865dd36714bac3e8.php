<?php $__env->startSection('content'); ?>
<?php if(isset($homeBanners) && $homeBanners->isNotEmpty()): ?>
<div class="gallery home-banner">
    <div class="mask">
        <div class="slideset">
        <?php $__currentLoopData = $homeBanners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $homeBanner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="slide">
                <div class="banner home" style="background:#000 url(<?php echo e(asset('/images/banner/'.$homeBanner->type.'/'.$homeBanner->id.'/'.$homeBanner->image)); ?>) no-repeat;">
                <?php if($homeBanner->image_alt): ?>
                    <img src="<?php echo e(str_replace('/images/', '/images/background/', asset('/images/banner/'.$homeBanner->type.'/'.$homeBanner->id.'/'.$homeBanner->image))); ?>" alt="<?php echo e($homeBanner->image_alt); ?>" class="hidden" />
                <?php endif; ?>    
                    <div class="d_table">
                        <div class="v_middle">
                            <div class="container">
                                <?php if($homeBanner->description): ?>
                                <div class="banner_caption">
                                    <?php echo $homeBanner->description; ?>

                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <?php if($homeBanners->count() > '1'): ?>
        <a href="#" class="btn-prev"><img src="<?php echo e(asset('frontend/images/slider_btn_left.png')); ?>" alt="<"></a>
        <a href="#" class="btn-next"><img src="<?php echo e(asset('frontend/images/slider_btn.png')); ?>" alt=">"></a>
        <div class="pagination"></div>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>
<div class="products">
    <div class="container explore">
        <h2 class="sec_heading">Explore our Products</h2>
        <div class="gallery add">
            <div class="mask">
                <?php if(!Helper::isMobile()): ?>
                <div class="slideset">
                    <?php if($homeCategories->isNotEmpty()): ?>
                        <?php $__currentLoopData = $homeCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loopCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="slide">
                        <?php $__currentLoopData = $loopCategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $homeCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="slide_holder">
                            <a href="<?php echo e($homeCategory->url); ?>" class="img_holder">
                                <div class="inner_img">
                                    <img src="<?php echo e(asset('/images/home/explore-product/'.$homeCategory->id.'/'.$homeCategory->image)); ?>" alt="<?php echo e($homeCategory->image_alt); ?>">
                                </div>
                                <span><?php echo e($homeCategory->title); ?></span>
                            </a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
                <?php if(Helper::isMobile()): ?>
                <div class="slideset">
                    <?php if($homeCategories->isNotEmpty()): ?>
                        <?php $__currentLoopData = $homeCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loopCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $__currentLoopData = $loopCategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $homeCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="slide">
                        <div class="slide_holder">
                            <a href="<?php echo e($homeCategory->url); ?>" class="img_holder">
                                <div class="inner_img">
                                    <img src="<?php echo e(asset('/images/home/explore-product/'.$homeCategory->id.'/'.$homeCategory->image)); ?>" alt="<?php echo e($homeCategory->image_alt); ?>">
                                </div>
                                <span><?php echo e($homeCategory->title); ?></span>
                            </a>
                        </div>
                    </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
                <a href="#" class="btn-prev"><img src="<?php echo e(asset('frontend/images/slider_btn_left.png')); ?>" alt="<"></a>
                <a href="#" class="btn-next"><img src="<?php echo e(asset('frontend/images/slider_btn.png')); ?>" alt=">"></a>
                <!-- <div class="pagination"></div> -->
            </div>
        </div>
    </div>
    <?php if($featuredProducts->isNotEmpty()): ?>
    <div class="featured_products">
        <div class="container">
            <h2 class="sec_heading">Featured Products</h2>
            <div class="gallery">
                <div class="mask">
                    <div class="slideset">
                        <?php $__currentLoopData = $featuredProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $featuredProduct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="slide">
                            <div class="featured_product">
                                <?php if($featuredProduct->images->isNotEmpty()): ?>
                                <div class="img-holder-wrapper">
                                    <img src="<?php echo e(asset('/images/product/'.$featuredProduct->id.'/'.$featuredProduct->images[0]->name)); ?>" alt="<?php echo e(($featuredProduct->images[0]->alt_text) ? $featuredProduct->images[0]->alt_text : Helper::getImageAlt($featuredProduct->images[0]->name)); ?>">
                                </div>
                                <?php endif; ?>
                                <div class="slide_txt">
                                    <strong><?php echo e($featuredProduct->title); ?> 
                                        
                                    </strong>
                                    <p><?php echo $featuredProduct->featured_description; ?> </p>
                                    <a href="<?php echo e(route('product', $featuredProduct->slug)); ?>" class="details_btn">VIEW DETAILS</a>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <a href="#" class="btn-prev"><img src="<?php echo e(asset('frontend/images/slider_btn_left.png')); ?>" alt="<"></a>
                    <a href="#" class="btn-next"><img src="<?php echo e(asset('frontend/images/slider_btn.png')); ?>" alt=">"></a>
                    <div class="pagination"></div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>
<?php if($homeNewsHighlights->isNotEmpty()): ?>
<div class="hightlights container">
    <h2 class="sec_heading">News & Highlights</h2>
    <ul class="list_none">
        <?php $__currentLoopData = $homeNewsHighlights; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $homeNewsHighlight): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <li style="background:url(<?php echo e(asset('/images/home/news-highlight/'.$homeNewsHighlight->id.'/'.$homeNewsHighlight->image)); ?>) no-repeat; background-size:cover;">
        <?php if($homeNewsHighlight->image_alt): ?>
            <img src="<?php echo e(str_replace('/images/', '/images/background/', asset('/images/home/news-highlight/'.$homeNewsHighlight->id.'/'.$homeNewsHighlight->image))); ?>" alt="<?php echo e($homeNewsHighlight->image_alt); ?>" class="hidden" />
        <?php endif; ?>    
            <div class="txt">
                <span><?php echo e($homeNewsHighlight->label); ?> </span>
                <a href="<?php echo e($homeNewsHighlight->url); ?>">
                    <strong class="title"><?php echo e($homeNewsHighlight->title); ?></strong>
                </a>
            </div>
        </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
</div>
<?php endif; ?>
<?php echo $__env->make('frontend.includes.follow_newsletter', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/pages/home/index.blade.php ENDPATH**/ ?>
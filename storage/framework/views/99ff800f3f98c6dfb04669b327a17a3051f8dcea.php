<?php $__env->startSection('content'); ?>
<div class="product_galery">
    <div class="container">
        <?php if($product->images->isNotEmpty()): ?>
        <div class="align_left">
            <div class="sh-product-gallery">
                <div class="sh-p-gallery-main watermarked-image-disable">
                    <?php $__currentLoopData = $product->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productImage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="main_image_holder"><img src="<?php echo e(asset('/images/product/'.$product->id.'/'.$productImage->name)); ?>" alt="<?php echo e(($productImage->alt_text) ? $productImage->alt_text : Helper::getImageAlt($productImage->title)); ?>" class="img-responsive img_pop_opener"></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <span class="msg img_pop_opener">Click to open expanded view</span>
                <div class="sh-p-gallery-thumb">
                    <?php $__currentLoopData = $product->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productImage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="thumb_holder"><img src="<?php echo e(asset('/images/product/'.$product->id.'/'.$productImage->name)); ?>" alt="<?php echo e(($productImage->alt_text) ? $productImage->alt_text : Helper::getImageAlt($productImage->title)); ?>"></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="align_right">
            <h2><?php echo e($product->title); ?></h2>
            <?php echo $product->short_description; ?>

            <div class="b_part">
                <div class="product-social-wrapper">
                    <div class="product-social-lable">
                        Share :
                    </div>
                    <div class="product-social-icons">
                        <ul class="social_icons list_none">
                            <li><a href="<?php echo e(Helper::getSocialShareLink(route('product', $product->slug ))); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="<?php echo e(Helper::getSocialShareLink(route('product', $product->slug ), 'twitter')); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="<?php echo e(Helper::getSocialShareLink(route('product', $product->slug ), 'linkedin')); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="<?php echo e(Helper::getSocialShareLink(route('product', $product->slug ), 'whatsapp')); ?>" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                            <li><a href="mailto:?Subject=<?php echo e($product->title); ?>&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 <?php echo e(route('product', $product->slug )); ?>" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="left">
                    <?php if(isset($product->documents[0])): ?>
                        <?php if(!Auth::check()): ?>
                        <a href="#" class="pdf_btn open-modal" data-modal="required-login">
                        <?php else: ?>
                        <a href="<?php echo e(asset('/documents/product/'.$product->id.'/'.$product->documents[0]->name)); ?>" class="pdf_btn add-to-downloads" data-type="product" data-document-id="<?php echo e($product->id); ?>" target="_blank">
                        <?php endif; ?>
                            <img src="<?php echo e(asset('frontend/images/pdf_icon.png')); ?>" alt="Download Datasheet"> Datasheet
                        </a>
                    <?php endif; ?>
                        <?php ($requestQuoteProducts = request()->session()->get('request_quote_products')); ?>
                        <?php ($requestAQuoteText = 'Request a Quote'); ?>
                        <?php ($requestAQuoteClass = 'add-request-a-quote'); ?>
                        <?php if((!empty($requestQuoteProducts) && isset($requestQuoteProducts[$product->id]))): ?>
                        <?php ($requestAQuoteText = 'Added for Quote'); ?>
                        <?php ($requestAQuoteClass = 'remove-request-a-quote'); ?>
                        <?php endif; ?>
                        <a href="#" class="active <?php echo e($requestAQuoteClass); ?>" data-product-id="<?php echo e($product->id); ?>"><img src="<?php echo e(asset('frontend/images/pencil_icon.png')); ?>" alt="<?php echo e($requestAQuoteText); ?>"><span><?php echo e($requestAQuoteText); ?></span></a>
                </div>
                <div class="alert alert-success request-a-quote-message" role="alert">
                    <?php echo e($product->title); ?> has been <span class="request-a-quote-action">added to</span> your enquiry cart.
                </div>
            </div>
        </div>
    </div>
    <div class="container product_tabs-holder">
        <ul class="list_none tabs product_tabs">
            <li class="active"><a href="#description">Description</a></li>
            <?php if($product->linkedProducts->isNotEmpty()): ?>
            <li><a href="#related">Related Products</a></li>
            <?php endif; ?>
            <!-- <li><a href="#downloads">Downloads</a></li> -->
        </ul>
        <div class="tab-content">
            <div id="description" class="tab active">
                <?php echo $product->description; ?>

            </div>
            <?php if($product->linkedProducts->isNotEmpty()): ?>
            <div id="related" class="tab">
                <div id="content" class="fluid">
                    <?php $__currentLoopData = $product->linkedProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $relatedProduct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="event p_cat_details">
                        <span class="detail_btn_holder"><a href="<?php echo e(route('product', $relatedProduct->product->slug)); ?>" class="detail_btn">View Product</a></span>
                        <div class="event_holder">
                            <?php if(isset($relatedProduct->product->images[0])): ?>
                            <div class="img_holder">
                                <img src="<?php echo e(asset('/images/product/'.$relatedProduct->product->id.'/'.$relatedProduct->product->images[0]->name)); ?>" alt="<?php echo e(($relatedProduct->product->images[0]->alt_text) ? $relatedProduct->product->images[0]->alt_text : Helper::getImageAlt($relatedProduct->product->images[0]->title)); ?>" class="img-responsive max_width">
                            </div>
                            <?php endif; ?>
                            <div class="txt add">
                                <h2><?php echo e($relatedProduct->product->title); ?></h2>
                                <p><?php echo $relatedProduct->product->short_description; ?>  </p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <!-- <div class="loading_div text-center">
                    <a href="#"><img src="<?php echo e(asset('frontend/images/load_btn.jpg')); ?>" alt="Load more"></a>
                </div> -->
            </div>
            <?php endif; ?>
            <div id="downloads" class="tab">
                <div id="content" class="fluid">

                </div>
                <!-- <div class="loading_div text-center">
                    <a href="#"><img src="<?php echo e(asset('frontend/images/load_btn.jpg')); ?>" alt="Load more"></a>
                </div> -->
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/pages/product-details.blade.php ENDPATH**/ ?>
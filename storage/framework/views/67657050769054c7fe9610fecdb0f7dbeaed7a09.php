<div class="popup" id="img_popup">
    <div class="d_table">
        <div class="v_middle">
            <div class="popup_holder">
                <a href="#" class="pop_closer"><i class="fa fa-times" aria-hidden="true"></i></a>
                <img src="" alt="#" class="full_image">
            </div>
        </div>
    </div>
</div>
<!-- The Modal -->
<div id="required-login" class="popup">
  <!-- Modal content -->
  	<div class="d_table">
        <div class="v_middle">
            <div class="popup_holder">
            	<div class="modal-content">
				    <h2 class="popup-title">Login Required</h2>
				    <div class="popup-content">Only existing members can access this content. <br/>Please log-in or create a new account.</div>
				    <div class="login-popup-btn btn-login">
				    	<a href="<?php echo e(route('user.login')); ?>" class="btn_aclogin">Use an existing account</a>
				    </div>
				    <div class="login-popup-btn btn-register">
				    	<a href="<?php echo e(route('register')); ?>" class="blue-btn">Create a new account</a>
				    </div>
				    <div class="login-popup-btn btn-close">
				    	<a href="#" class="close-modal" data-dismis="required-login">Close</a>
				    </div>
				</div>
            </div>
        </div>
    </div>
</div><?php /**PATH /home/fibertekap/public_html/resources/views/frontend/includes/popups.blade.php ENDPATH**/ ?>